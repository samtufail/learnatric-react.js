module.exports = {
  reactStrictMode: false,
  images: {
    domains: [
      'learnatric.s3.amazonaws.com',
      'learnatric-problem-illustrations.s3.amazonaws.com',
      'res.cloudinary.com',
      'learnatric-website-images.s3.amazonaws.com',
    ],
  },
  async headers() {
    return [
      {
        source: '/:all*(svg|jpg|png|webp)',
        locale: false,
        headers: [
          {
            key: 'Cache-Control',
            value: 'public, max-age=9999999999, must-revalidate',
          },
        ],
      },
    ]
  },
  // eslint: {
  //   ignoreDuringBuilds: true,
  // },
}
// const withPlugins = require('next-compose-plugins')
// const withImages = require('next-images')

// const nextConfig = {
//   reactStrictMode: false,
//   images: {
//     domains: ['learnatric.s3.amazonaws.com']
//   }
// }

// module.exports = withPlugins([[withImages]], nextConfig)

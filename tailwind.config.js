module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundColor: {
        secondary: '#495f74'
      }
    },
  },
  variants: {
    extend: {
    },
  },
  plugins: [

  ],
}
import Icon from '@ant-design/icons'

const DownSVG = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="26"
      height="24"
      viewBox="0 0 26 24"
    >
      <g
        id="Group_2616"
        data-name="Group 2616"
        transform="translate(-47.593 -11.592)"
      >
        <path
          id="Polygon_2"
          data-name="Polygon 2"
          d="M5.822.626a1,1,0,0,1,1.357,0l3.942,3.639A1,1,0,0,1,10.442,6H2.558a1,1,0,0,1-.678-1.735Z"
          transform="translate(66.593 26.592) rotate(180)"
        />
        <rect
          id="Rectangle_355"
          data-name="Rectangle 355"
          width="26"
          height="24"
          transform="translate(47.593 11.592)"
          fill="none"
        />
      </g>
    </svg>
  )
}

export const Down = () => {
  return <Icon component={DownSVG} />
}

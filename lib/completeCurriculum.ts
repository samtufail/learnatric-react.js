export const kinderCurriculum = [
  {
    heading: 'Counting and Cardinality:',
    courseChapters: [
      'Compare two numbers between 1 and 10 presented as written numerals',
      'Count forward beginning from a given number other than one',
      `Count to 100 by ones, fives, and tens`,
      `Count to answer "how many?" questions about as many as 20 things arranged in a line,
       a rectangular array, or a circle, or as many as 10 things in a scattered configuration;
       given a number from 1-20, count out that many objects`,
      `Identify whether the number of objects in one group is greater than, less than, or equal
       to the number of objects in another group`,
      `Understand the relationship between numbers and quantities`,
    ],
  },
  {
    heading: 'Geometry:',
    courseChapters: [
      'Analyze and compare two- and three-dimensional shapes',
      'Join shapes to make other shapes',
      'Correctly identify shapes',
      'Identify areas above, below, beside, in front of, behind, and next to an object',
    ],
  },
  {
    heading: 'Measurement & Data:',
    courseChapters: [
      'Classify objects into given categories',
      'Describe measurable attributes of objects, such as length or weight',
      'Compare and interpret measurements',
      'Directly compare two objects with a measurable attribute in common',
      'Create a number line',
    ],
  },
  {
    heading: 'Operations & Algebraic Thinking:',
    courseChapters: [
      'Add up to 5',
      'Subtract up to 5',
      'Create addition equation to make 10 using 1-9',
      'Create subtraction equation to make 10 using 1-9',
      'Find number to make 10 when given starting number',
      'Represent addition & subtraction using drawing/objects',
      'Identify odd and even numbers',
      'Count by 2s',
    ],
  },
]

export const firstGradeCurriculum = [
  {
    heading: 'Numbers & Operations in Base Ten:',
    courseChapters: [
      'Subtraction/Addition relationship',
      'Add and subtract within 100',
      'Place values: base 1 and base 10',
      'Subtract by 10s up to 100',
      'Add three digit numbers',
    ],
  },
  {
    heading: 'Operations & Algebraic Thinking:',
    courseChapters: [
      'Commutative property: Addition',
      `Determine the unknown whole number in an addition or subtraction
     equation relating three whole numbers. For example,
     determine the unknown number that makes the equation true in each
      of the equations 8 + ? = 11, 5 = _ - 3, 6 + 6 = _.`,
      'One step word problems',
    ],
  },
  {
    heading: 'Geometry:',
    courseChapters: [
      'Describe and distinguish shape attributes',
      'Area: relate to add and multiply',
      'Partition rectangle: rows and columns',
      'Describe shares of a whole as one-fourth, two halves, three thirds, four fourths',
      'Partition circles and rectangles: 2,3,4, equal shares',
    ],
  },
]

export const secondGradeCurriculum = [
  {
    heading: 'Numbers & Operations:',
    courseChapters: [
      'Add and subtract within 1000; skip-count by 5s, 10s, and 100s.',
      'Subtract three-digit numbers',
      'Place value: base 100 and base 1,000',
    ],
  },
  {
    heading: 'Operations & Algebraic Thinking:',
    courseChapters: [
      'Two-step word problems requiring addition and subtraction',
      'Add objects up to 5x5 rectangular arrays',
      'Basic concepts of multiplication and division',
      'Multiplication/Division relationship',
    ],
  },
  {
    heading: 'Geometry:',
    courseChapters: [
      `Partition a rectangle into rows and columns of
     same-size squares and count total number of them`,
      'Describe shares of a whole as one-fourth, two halves, three thirds, four fourths',
      'Partition circles and rectangles into 2,3,4, equal shares',
    ],
  },
  {
    heading: 'Measurement and Data:',
    courseChapters: [
      'Estimate lengths using units of inches, feet, centimeters, and meters',
      `Measure the length of an object by selecting and using appropriate tools such as rulers,
     yardsticks, meter sticks, and measuring tapes`,
      `Tell and write time from analog and digital clocks to the nearest five minutes, using
     a.m. and p.m`,
      'Identify money denominations',
    ],
  },
]

export const thirdGradeCurriculum = [
  {
    heading: 'Numbers & Operations:',
    courseChapters: [
      'Compare two fractions',
      'Recognize and generate simple equivalent fractions',
      'Understand a fraction as a number on the number line',
      'Addition and subtraction of fractions',
      'Strategies for addition and subtraction within 1,000',
      'Multiply one-digit whole numbers by multiples of 10',
      'Round to 10',
      'Round to 100',
    ],
  },
  {
    heading: 'Operations & Algebraic Thinking:',
    courseChapters: [
      'Apply properties of operations as strategies to multiply and divide.',
      `Fluently multiply and divide within 100. By the end of Grade 3, know
      from memory all products of two one-digit numbers`,
      'Two-step word problems with multiplication and division',
    ],
    examples: [
      `Commutative property of multiplication: If 6 × 4 = 24 is known,
         then 4 × 6 = 24 is also known
        Associative Property of multiplication:
         3 × 5 × 2 can be found by 3 × 5 = 15, then 15 × 2 = 30, or by 5 × 2 = 10,
          then 3 × 10 = 30`,
      `Distributive property: Knowing that 8 × 5 = 40 and 8 × 2 = 16,
        one can find 8 × 7 as 8 × (5 + 2) = (8 × 5) + (8 × 2) = 40 + 16 = 56'`,
    ],
  },
  {
    heading: 'Measurement & Data:',
    courseChapters: [
      `Use the four operations to solve word problems involving distances,
      intervals of time, liquid volumes, masses of objects, and
      money, including problems involving simple fractions or decimals, and
       problems that require expressing measurements`,
      'Solve problems using bar and picture graphs',
      'Add and subtract lengths and weights',
      'Measure areas by counting unit squares',
      'Measure perimeter',
    ],
  },
]

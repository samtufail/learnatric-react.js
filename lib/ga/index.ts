// log the pageview with their URL
export const pageview = (url: URL) => {
  window.gtag('config', process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS, {
    page_path: url,
  })
}

type GTagEvent = {
  action: string
  event_category: string
  event_label: string
  value: number
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const gaEvent = (
  { 
    action, 
    event_category,
    event_label,
    value
  }: GTagEvent) => {
    // console.log('event: ', action)
  window.gtag('event', action, {
    event_category: event_category,
    event_label: event_label,
    value: value
  })
}

export const blogPost = [
  {
    id: 'learnatric-concept',
    long: 'yes',
    postImage: 'https://res.cloudinary.com/learnatric/image/upload/v1635169938/Blog/BlogPosts/hans-reniers-lQGJCMY5qcM-unsplash_ebm0um.jpg',
    title: 'The Learnatric Concept',
    author: 'David Shpigler',
    text: `Today, parents are faced with the dilemma of how to choose from among so many 
    undifferentiated educational approaches that feature static content that fails to 
    adjust to the needs of the student.`,
    content: [
      {
        id: 'problem-education',
        heading: "The Problem with Education",
        paragraphs: [
          {
            id: 'problem-1',
            text: `Today, parents are faced with the dilemma of how to choose from among so many 
            undifferentiated educational approaches that feature static content that fails to 
            adjust to the needs of the student.  By treating all students in exactly the same 
            way, we unnecessarily limit the ability of students to learn.  In essence, this is the 
            problem associated with the concept of “teaching to the middle” – we establish an 
            approach for a “typical” student and hope that this approach will help as many 
            students as possible.`
          },
          {
            id: 'problem-2',
            text: `If we think about it, we realize that other industries differentiate by the needs of the
            user.  A doctor doesn’t prescribe medicine for a typical patient, but rather for your 
            individual medical needs.  A financial planner doesn’t develop a financial plan for a 
            typical client, but instead provides one that fits the unique needs at hand.  In a field 
            as important as education, why should we settle for less?`
          }
        ]
      },
      { 
        id: 'learnatric-concept',
        heading: 'The Learnatric Concept',
        paragraphs:[
          {
            id: 'concept-1', 
            text: `Learnatric is designed to leverage an innovative educational approach that is 
            combined with state-of-the-art technology to deliver value to students through 
            continuously adaptive and optimized learning processes.  The student’s progress is 
            monitored by the platform which utilizes machine learning building to a true 
            Artificial Intelligence (AI) system to transparently and dynamically adjust the 
            learning process to optimize the student’s learning experience.`
          },
          { 
            id: 'concept-2', 
            text: `Each student goes through lessons that are optimized based on a student’s needs 
            at that precise point in time.  A lesson involves three elements:`,
            list: [
              `I Do – instruction on a new skill that the Learnatric platform has identified as 
              key to the student’s optimal learning pathway`,
              `We Do – a guided activity that allows the student to test their ability to apply 
              the new skill that was just taught`,
              `You Do – a set of problems that the student can perform independently that 
              reinforces the lesson`
            ]
          },
          { 
            id: 'concept-3', 
            text: `The overarching approach is aimed to reinforce learning in the optimal way by 
            helping each student to achieve his/her individual breakthrough as quickly as 
            possible.  This approach allows Learnatric to focus on the learner in a way unlike 
            any existing EdTech platform.`
          }
        ]
      },
      { 
        id: 'science-learnatric',
        heading: 'The Science Behind Learnatric', 
        paragraphs: [
          { 
            id: 'science-1', 
            text: `The overarching educational approach employed by Learnatric is based on the 
            concept of Threshold Concepts and Troublesome Knowledge.  This concept provides
            the foundation that seeks to help each student achieve his/her individual 
            breakthrough as quickly as possible.  This approach allows us to focus on the 
            learner in a way unlike any existing EdTech platform.`,
            quote: `A threshold concept can be considered as akin to a portal, opening up a 
            new and previously inaccessible way of thinking about something. It 
            represents a transformed way of understanding, or interpreting, or 
            viewing something without which the learner cannot progress. As a 
            consequence of comprehending a threshold concept there may thus be a 
            transformed internal view of subject matter, subject landscape, or even 
            worldview. This transformation may be sudden or it may be protracted 
            over a considerable period of time, with the transition to understanding 
            proving troublesome. Such a transformed view or landscape may 
            represent how people ‘think’ in a particular discipline, or how they 
            perceive, apprehend, or experience particular phenomena within that 
            discipline (or more generally) (Meyer & Land, 2003)`
          },
          { 
            id: 'science-2',
            text: `Threshold Concepts are commonly seen as the areas of a subject at which students 
            have to put in concerted effort to push through conceptually.  In all subject domains
            and disciplines there are points which lead us into “previously inaccessible ways of 
            thinking”.  If a concept is a way of organizing and making sense of what is known in 
            a particular field, a Threshold Concept organizes the knowledge and experience, 
            making an epiphany or ‘eureka moment’ possible.`
          },
          { 
            id: 'science-3',
            text: `Using Threshold Concepts as the guideposts within Learnatric, we distinguish our 
            system from most online educational programs today as Learnatric does more than 
            just use linear assessments to create branching pathways within a system of static, 
            sequential lessons.  Learnatric uses a complex tracking algorithm that guides the 
            student through a unique sequence of activities and introduce Threshold Concept 
            (TC) lessons as a student demonstrates the skills and knowledge needed to 
            advance.  These lessons are modified to meet the learning needs of the student.`
          }
        ]
      },
      { 
        id: 'educational-tags', 
        heading: 'Educational Tags',
        paragraphs: [
          { 
            id: 'tags-1', 
            text: `To apply the educational approach, Learnatric uses a new approach called 
            “educational tags”.  Functionally, these tags are embedded in the exploration 
            activities of the platform and serve as the primary data input for the system.  These
            tags provide an evaluation score within content domains, learning styles, and 
            attitude skills in a way similar to chess ELO ratings in each field of learning.  The 
            learning analytics feed into the Learnatric learning engine, which continually 
            updates each student’s profile and provides information to adjust content delivery, 
            keeping each student on their optimal learning pathway.`
          },
          { 
            id: 'tags-2',
            text: `The results of the dynamic scoring of educational tags are then integrated into the 
            machine learning system to optimize student learning.  Machine learning is the 
            process for programming systems to take in large amounts of data, analyze it, and 
            make increasingly better decisions based on the outcomes produced.  Today, 
            machine learning is used to track the data of actual drivers in Google Maps.  In the 
            same way, we are now able to take the collective experience of effective teaching 
            to create the content and then allow the Learnatric learning engine to learn when to
            present content and the right sequence of modifications to optimize student 
            outcomes.  The more data points that are tracked, the more accurately the system 
            will be able to adapt to students using the system.  This is the same way that a 
            professional teacher takes years to form best practices in the classroom based on 
            the experience of teaching.  By providing data reporting to the classroom teacher 
            and parent, we have created a loop that helps identify and remediate or challenge 
            more effectively.`
          }
        ]
      },
      { 
        id: 'go-from-here',
        heading: 'Where Do We Go From Here',
        paragraphs: [
          {
            id: 'go-from-here-1',
            text: `Learnatric is now developing a new set of system characteristics that will allow for 
            an enhanced experience for students and parents.  Some of the upcoming 
            developments that will be arriving on the Learnatric platform in the coming weeks 
            include:`,
            list: [
              'Adding new account controls for parents',
              'Developing an initial assessment to better establish a student’s starting point',
              'Creating a gamified pathway for young students to track their progress'
            ]
          }
        ]
      }
    ]
  },
  {
    id:'learnatric-secret-sauce',
    postImage: 'https://res.cloudinary.com/learnatric/image/upload/v1634314368/Blog/BlogPosts/kristina-flour-BcjdbyKWquw-unsplash_mfjzom.jpg',
    title: 'The Secret Sauce Behind Learnatric',
    author: 'David Shpigler',
    text: `Learnatric is being designed to leverage an innovative pedagogical approach combined with state of the art technology to deliver value to students through continuously adaptive and optimized learning processes. The student’s progress is monitored by the platform which will utilize machine learning building to a true Artificial Intelligence (AI) system to transparently and dynamically adjust the learning process to optimize the student's learning experience. The overarching pedagogy is based on the concept of Threshold Concepts and Troublesome Knowledge, where we seek to help each student achieve his/her individual breakthrough as quickly as possible. This approach allows us to focus on the learner in a way unlike any existing EdTech platform.
    So, what makes a Threshold Concept different from, say, a 'key concept'? Fundamentally, Threshold Concepts are commonly seen as the areas of a subject at which students have to put in concerted effort to push through conceptually. Further, more advanced ideas depend on the understanding of certain important fundamentals. In all subject domains and disciplines there are points which lead us into “previously inaccessible ways of thinking”. If a concept is a way of organizing and making sense of what is known in a particular field, a Threshold Concept organizes the knowledge and experience, making an epiphany or ‘eureka moment' possible.
    Using Threshold Concepts as the guideposts within Learnatric, we distinguish our system from most online educational programs today as Learnatric does more than just use linear assessments to create branching pathways within a system of static, sequential lessons. Learnatric uses a complex tracking algorithm that guides the student through a unique sequence of activities and introduce Threshold Concept (TC) lessons as a student demonstrates the skills and knowledge needed to advance. These TC lessons are modified to meet the learning needs and predilections of the student.
    Functionally, the learning attribute tags that are embedded in the gamified exploration activities of the platform serve as the primary data input for the designed algorithm. These tags provide an evaluation score within content domains, learning styles, and attitude skills in a way similar to chess ELO ratings in each sub-domain area. The learning analytics feed into the Learnatric learning engine, which continually updates each student's profile and provides information to adjust content delivery, keeping each student on their optimal learning pathway.
    What do we mean by “machine learning” in the design of the Learnatric Learning Engine? In essence, machine learning is the process for programming systems to take in massive amounts of data, analyze it, and make increasingly better decisions based on the outcomes produced. In the same way that machine learning has led to breakthroughs in autonomous cars by tracking the data of actual drivers in Google Maps, we are now able to take the collective experience of effective teachers to create the content and then allow the Learnatric learning engine to learn when to present content and the right sequence of modifications to optimize student outcomes. The more data points that are tracked, the more accurately the system will be able to adapt to students using the system. This is the same way that a professional teacher takes years to form best practices in the classroom based on the experience of teaching. By providing data reporting to the classroom teacher and parent, we have created a loop that helps identify and remediate or challenge more effectively.`,
    qoute: `A threshold concept can be considered as akin to a portal, opening up a new and previously inaccessible way of thinking about something. It represents a transformed way of understanding, or interpreting, or viewing something without which the learner cannot progress. As a consequence of comprehending a threshold concept there may thus be a transformed internal view of subject matter, subject landscape, or even worldview. This transformation may be sudden or it may be protracted over a considerable period of time, with the transition to understanding proving troublesome. Such a transformed view or landscape may represent how people ‘think’ in a particular discipline, or how they perceive, apprehend, or experience particular phenomena within that discipline (or more generally)” (Meyer & Land, 2003).`
  },
  {
    id: 'data-science-lesson-for-kids',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247534/Blog/BlogPosts/data-sceince-lesson-for-kids_zk8voj.jpg',
    title: 'Data Science Lesson for Kids',
    author: 'Anthony DiCecco',
    text: `Earlier this year Data Science Connect put on the first ever Data Science Conference for K-12
    students and teachers. We were thrilled to have our Chief Education Officer Holly Windram
    host a session on how educators are data scientists too! It was a great day with a lot of
    fantastic speakers and with it being done virtually, all the sessions were recorded and are
    now posted on YouTube for your viewing pleasure. There are 32 sessions in all, some geared
    toward educators and parents like Holly’s, and some for students ranging from grades K-12.
    So take a productive break and check it out!`,
    videoUrl: 'https://www.youtube.com/watch?v=fCVyiVf8Utw',
  },
  {
    id: 'adapting-adaptive-technology',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247502/Blog/BlogPosts/adapting-adaptive-technology_wwd3ys.jpg',
    title: 'Adapting Adaptive Technology',
    author: 'David Shpigler',
    text: `A wide variety of educational reforms and programs have been championed in a pursuit to
    improve education in the United States.  Some recent ideas include enforcing standards,
    opening charter schools, providing vouchers for private education, improving teacher pay;
    two recent federal programs include No Child Left Behind and Race to the Top.  Despite these
    efforts, by almost any standard the quality of education in this country is lagging, with U.S.
    schools still languishing in the middle of international rankings, behind the schools of such
    countries as Estonia and Slovenia (Mehta, 2013).
    Learnatric believes there exists significant potential in utilizing adaptive learning principles
    into the educational sphere to a far greater degree.  In fact, studies have shown that
    tremendous impact can be created in this way (Adkins, 2009):
    “Cognitive and intelligent tutors are meta-cognition technologies that simulate the behavior
    of a human mentor and provide personalized responses, remediation, and interventions in
    real time based on the knowledge, behavior, and cognitive abilities of a particular user.
    The products are based on artificial intelligence and generate a cognitive model of the
    student based on the interaction with the student.  The model is then used to provide
    individualized instruction to the student.
    In a seminal study known as the “Two Sigma Problem”, Bloom found that, on average, tutored
    students scored better than 98% of classroom students.  This means that the achievement of
    individually-tutored students may exceed that of classroom students by as much as two
    standard deviations (a two sigma shift).  This knowledge-transfer improvement is roughly
    equivalent to raising the performance of 50  percentile students to that of 98  percentile
    students.  New cognitive tutor products are capable of exceeding the two-sigma deviation.”
    As Learnatric embarks on deploying its dynamic learning platform, we believe we can
    significantly impact education in this country in two ways.  First, we have the potential to
    deliver a more responsive educational approach to students – one that promises to support
    greater educational achievement in large numbers of students.  Second, we also have the
    opportunity to reshape theories in pedagogy and learning science.  Through our use of
    machine learning, we will have a “big data” component to the system that can monitor
    learning trends among large numbers of students.  By analyzing the data, we have the
    potential to validate or challenge existing theories about optimal educational approaches.`,
  },
  {
    id: 'machine-learning-that-serves-the-students-not-the-curricula',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247501/Blog/BlogPosts/machine-learning-that-serves-the-students_vkuw3x.jpg',
    title: 'Machine Learning That Serves The Students, Not The Curricula',
    author: 'David Shpigler',
    text: `Technology innovation impacts nearly every industry and enables customers to get a
    personalized experience.  However, for some reason education still lags, with children still
    largely being taught in the same manner as 100 years ago.  Select implementations of
    technology in education like smart boards don’t really solve the core problem. (Student-Centered Learning Environments: Foundations, Assumptions, and Design,
    Theoretical Foundations of Learning Environments (pp.3-25) Edition: 2ndChapter; Publisher:
    Taylor & Francis; Editors: David Jonassen, Susan Land ).  True dynamic adaptive learning
    empowers the student by enabling him/her to be taught according to his/her needs.  Existing
    solutions feature adaptivity limited to student assessment and simple branching protocols
    rather than using the power of machine learning and cognitive analytics to make education
    customized for each and every student.
    Learnatric proposes to blend innovative pedagogy with true machine learning to deliver value
    to students by utilizing cognitive analytics that interfaces with the learning science built into
    the back end of our system – an approach that will result in an online learning system that
    offers true adaptive learning that responds to the student’s need on a fully dynamic basis.
    The pedagogy is based on the concept of Threshold Concepts and Troublesome Knowledge
    (Threshold Concepts and Troublesome Knowledge: Epistemological Considerations and a
    Conceptual Framework for Teaching and Learning, Jan Meyer and Ray Land, 2005.).  Using
    this blend, we now have the ability to identify the specific source of difficulty and opportunity
    for each child in a truly dynamic way.`,
    qoute: `Instead, what we need is an educational approach that is student-
    centered, rather than curriculum-centered`,
  },
  {
    id: 'machine-learning-for-kids',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247525/Blog/BlogPosts/machine-learning-for-kids_hfwtez.jpg',
    title: 'Machine Learning...for Kids?',
    author: 'Anthony DiCecco',
    text: `
      Over the last decade, Machine Learning and AI have become common terms and tools that
      can be applied to most problems. So many things we interact with have some form of a
      machine learning component to aid in the process. Fraud detection helps keeps your bank
      accounts safer Route optimization gets you to your destination as fast as possible while
      avoiding accidents. Speech recognition and translation have made it easier to interact with
      each other and technology. And the application that we probably experience most is the
      recommendation algorithms that serve up new music, TV shows, news articles, ads, and
      search results based on what our preferences are.
      Now I’d argue that most of these applications have helped us be more productive and make
      better decisions although I know there’s a lot of grey area. It’s natural to be uneasy with a
      black box that is using our data to make decisions without confirming it has our best
      intentions in mind. The top two things I believe help us all overcome this hesitation are
      transparency and results. Any insight you’re able to get about how the decisions are made
      can make it feel more human. And even if that doesn’t happen, ease of use and seamless
      integration can go a long way into adoption.
      We want to take the best that machine learning has to offer to create something that’s
      undeniably for good with no hidden agenda. With all the outside help we get, one of them
      should definitely be to help strengthen student’s skills.
    `,
  },
  {
    id: 'procrustean-beds-and-tutors',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247535/Blog/BlogPosts/procrustean-beds-and-tutors_opxdlm.jpg',
    title: 'Procrustean Beds and Tutors',
    author: 'Ben Taylor',
    text: `There’s a Greek myth that describes a son of Poseidon named Procrustes. He had an iron bed
    with the imprint of what he considered to be the “proper man.” Rather grimly, Procrustes
    would capture wavering travelers and force them to conform to that imprint. If they were too
    short, he would stretch them until they filled it out. If too large—well, you get the picture.
    Though that metal bed foreshadowed something like medieval torture devices, the
    Procrustean bed is present today in abstract terms, and we know this. For instance, parents
    insist that their children choose their own extracurricular activities because it’d be wrong to
    impose a parent’s favorite sport on an unwilling child. We fear that society’s constraints will
    stamp out the individual creativity and sense of wonder in our children. As a teacher who was
    instructed to “teach to the middle,” the myth of Procrustes weighed heavily on me. Though
    we had a rich curriculum, it was linear and applied the same criteria to students of vastly
    different ways of acquiring knowledge.
    Another example, the German word meaning “education” also means “formation” as a potter
    does clay (German: Bildung). Of course, the metaphor unwittingly assumes that students are
    akin to clay, i.e., an undifferentiated mass. It’s passive and formed to the desires of the
    teacher who’s, again, teaching to the middle.
    Many wonderful educators have found ways to avoid such a dismal (and grisly, in the case of
    Procrustes) set of approaches. But it’s not exactly been done on the eLearning front. We still
    find warm beds of iron that may or may not fit the form of our child. To avoid educational
    torture, so to speak, we at Learnatric are harnessing the power of machine learning, micro-
    assessors, and more to educate students according to their individual needs. Our analogy is
    not a wicked son of Poseidon or a potter forming undifferentiated clay. We are translating the
    sharp eyes and mind of a masterful tutor into technology so that children focus on learning
    exactly how and what they need in order to succeed.
    `,
    qoute: `Of course, the metaphor unwittingly assumes that students are akin to
    clay, i.e., an undifferentiated mass.`,
  },
  {
    id: 'finding-your-kids-new-normal',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247504/Blog/BlogPosts/finding-your-kids-new-normal_eayk8n.jpg',
    title: 'Finding Your Kid’s New Normal',
    author: 'Anthony DiCecco',
    text: `From my experience, and I hope I’m not alone in this, becoming a teacher was a very scary
    goal. The best teachers make it look effortless and have a wealth of knowledge in not only
    their subject but in all the soft skills of controlling a classroom, inspiring their students, and
    doing what it takes to help everyone succeed. When looking at lists of “characteristics of
    highly effective teachers,” a lot of common themes come up like organization and structure,
    positivity and support, being motivational and engaging; and this is all put on top of the most
    common fear people experience of public speaking. As the last year has pushed at least
    some of the teaching role to most parents, even more have experienced the struggles. So
    with all these challenges and skills that take time to build up, what can be done to help?
    The most general and easiest answer is, “a lot,” but that doesn’t help much. In solving any
    problem, taking stock of what’s going on is usually a good first step. With so much to keep
    track of, it’s important to be very structured with what is captured and to get it out of your
    own head. These are some of the core concepts we’re taking at Learnatric to build a learning
    platform to help. There are many struggles that will exist for teachers that just take time and
    experience to build up, but having better tools for teachers and caretakers should be a no-
    brainer as students figure out their new normal. `,
    qoute: `In solving any problem, taking stock of what’s going on is usually a good
    first step.`,
  },
  {
    id: 'safe-or-beneficial',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247529/Blog/BlogPosts/safe-or-beneficial_reiph5.jpg',
    title: 'Safe or Beneficial',
    author: 'Ben Taylor',
    text: `I remember overhearing a gentleman in a waiting room venting that someone he knew was
    meditating, which was obviously (to him) an evil, eastern practice. After half-yelling about
    that, he went on to talk about the news of the day for about thirty minutes. I couldn’t help but
    smile to myself at the irony: he himself was “meditating” (quite intensely) on political talking
    points until a tired nurse called his name.
    While setting aside time for meditation has become an increasingly popular practice in the
    West, anytime we reflect, discuss, or complain about value-laden topics, we are meditating
    on (and affirming) those values. And what a wide-ranging set of values there are, especially
    when armed with a phone or tablet! With a practically infinite series of web-locations online,
    people must be careful and intention, else they’ll fall prey to whatever worldviews are lurking
    about.
    But learning to control your technology instead of vice versa is difficult, especially for children
    and teenagers who are naturally impulsive. Having safe spaces for children has become
    popular, and that’s a good thing. Still, spending two hours playing a safe game isn’t exactly
    time well spent. In fact, something positively bad, idleness, may be lurking there.
    Wouldn’t it be better if we could move from focusing on something safe to actually instilling
    positive values, like fostering the natural love of learning? Learning and seeing their progress
    in learning can be insanely rewarding (and keep them engrossed in an app for some quiet
    time). Learnatric would like to be a space where kids aren’t just safe, but are prompted to
    refine and celebrate their love of learning.`,
    qoute: `Having safe spaces for children has become popular, and that’s a good
    thing. Still, spending two hours playing a safe game isn’t exactly time well
    spent.`,
  },
  {
    id: 'is-online-learning-for-me',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247517/Blog/BlogPosts/online-learning_p9g7az.jpg',
    title: 'Is Online Learning for Me?',
    author: 'Holly Windram',
    text: `Making a decision about what online learning platform to choose for your child is important.
    Prior to COVID forcing schools to go virtual, there were many products available.  One
    beneficial outcome of school closures is that options for online learning have expanded.
    Caregivers still have questions about quality, safety, and whether their investment has
    resulted in their child’s learning.   Here are a few items for caregivers to consider when
    choosing an online learning platform:
    A free trial. Any reputable online learning platform should allow a free trial for you and your
    child to explore and see what it has to offer. A free trial should allow your child enough time to
    have several opportunities for using it. A free trial is critical to determining the next item,
    which is....
    Kids like it.  Does your child seem actively interested and willing to use the platform?  And
    not just once or twice over 1-2 days.  How about 1-2 weeks into the free trial? Ask them –
    what makes this fun? What makes this not fun? What would make online learning the most
    fun for you?
    Tracking progress.  Look for a feature that provides you and your child with regular
    feedback on progress within the learning tool, for example, lessons completed, and, more
    importantly, in the learning of skills and knowledge.
    Safety.  Is the platform password protected? How many ads appear and what kind of content
    do they include?
    Evidence.  Testimonials and stories are important;  but, actual research conducted by third
    parties on the impact of the product on student learning should be transparent and easily
    accessible on the website.
    Overall – does it work for you and your child?  Consider if this product fits with your
    routines, interests, and goals as a family.  Does it require a lot of adult guidance to use or can
    kids be pretty independent, for example?  The only way you are going to see if online learning
    makes a difference for your child is by using it.   You have a lot of options so be sure to find
    the right one that works best for you and your child!
    `,
  },
  {
    id: 'we-do',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247529/Blog/BlogPosts/we-do_kexsfz.jpg',
    title: 'We do',
    author: 'Ben Taylor',
    text: `Teaching is more than just teaching. It’s about a third of it. If it stopped with teaching, or
    direct instruction, kids would essentially be information receptacles. Still, I knew as a teacher
    that I couldn’t just hand them a worksheet and say, “Now that I’ve taught you this, please go
    ahead and apply it.” Teachers need a bridge between providing direct instruction and allowing
    time for independent work. That bridge is the middle third of a good lesson. It’s sometimes
    called an activity, or the second part of what teachers call “I do, you do, we do.”
    When I taught as a school teacher, I instructed students on how to solve something and then
    brought out fresh questions and said, “Now let’s solve these together.” My job was to
    anticipate and catch every way that a student could incorrectly attempt to solve the problem.
    This took a great deal of attentiveness. If I let one mistake in, my best opportunity to set
    students aright would be gone. It was also messy. I cold called on students more than any
    other part of the lesson, and they all knew it. When someone was zoned out, it hurt the
    classroom culture. They sometimes couldn’t remember or, worse, they confidently mis-
    remembered. Still, the alternative would be to throw them into the deep end and hope for the
    best.
    This has admittedly been the most difficult part of the tutor to translate into technology. The
    direct instruction and problem set portions have their challenges but are straightforward. The
    “We do” part, perhaps the most important because it meets a kid where s/he is, has been
    subjected to the most discussion and scrutiny during the development of Learnatric lesson
    templates. The fruit has been to make something novel in the EdTech space. A virtual bridge
    between instruction and independent application. Our web app will prompt the student to
    respond to a problem step-by-step instead of treating them as information receptacles, and
    their progression through the activity or “We do” section depends on the answers they input.
    Aside from curating the most appropriate lesson for each student’s individual needs, the
    activity portion technologizes the ideal tutor, and we believe it does so to the great benefit of
    kids.
    `,
  },
  {
    id: 'lesson-re-learned',
    postImage:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632247503/Blog/BlogPosts/lesson-re-learned_kkuila.jpg',
    title: 'Lesson (re)Learned',
    author: 'Ben Taylor',
    text: `There is a teacher shortage in the US, and it’s worse than we thought (of course). According
    to the U.S. Department of Education, 40+ states in the US report some significant degree of
    shortage of mathematics teachers in particular, leaving
    some to question whether the profession is worthwhile.
    Compounding all things, COVID-19 has also disrupted teachers and students, especially
    those that are unilaterally hurt by having learning styles that require being in the physical
    presence of teachers and students. Many students learn less without engaging in a face-to-
    face manner with tutors who can personalize the experience.
    What’s more, consumer and workplace preferences may very well value face-to-face
    interactions less. Such preferences, while fine per se, could leave those who have brought
    value to the workplace primarily with soft skills at a disadvantage. Less face-to-face time
    means less opportunities for the charismatic to woo potential clients. If the need for soft skills
    is lessened, will the need for “hard skills” such as math rise? Perhaps. But even without such
    a dynamic, the need for those from a STEM background is rising just by the leaps and bounds
    big tech is making.
    The need for maths mastery is increasing while the service of public and private education is
    struggling to deliver. Parents, then, are spurred to consider other means of educating their
    children.
    Learnatric is one private option. Unlike its competitors, it does not teach in a linear fashion:
    e.g., after lesson 12, the student must learn lesson 13. A major problem with teaching linearly
    is that a student who only learned most of lessons 4, 7, and 10 (let’s say), is ill-equipped to
    succeed for much of what she’ll face in upcoming lessons.
    Instead, Learnatric assesses each and every answer a student submits by rescaling hundreds
    of micro-assessors. This way, instead of seeing that Oliver scored 85% on a unit test (and so
    is ready to move along), Learnatric can identify and curate content so that Oliver can learn
    and relearn the 15% he’s still struggling with. If a real tutor could identify the 15% Oliver
    struggles with, we feel that technology has come far enough that a web app should be able to
    do the same.
    While nothing can replace family and tutors when it comes to education, Learnatric is the
    best next option for zeroing in on what lessons and concepts have dropped through the
    cracks for each child.
    `,
    qoute: `The need for maths mastery is increasing while the service of public and
    private education is struggling to deliver.`,
  },
]

import { Layout, Header } from 'components'
import { ReactElement } from 'react'
import { DescriptionSection, TeamSection } from 'sections'
import styles from 'styles/about-us.module.scss'

const AboutUs = (): ReactElement => {
  const { aboutUs, aboutUsWrapper } = styles
  return (
    <Layout
      title="About Us - Learnatric"
      description="The inspiration for Learnatric originated from David Shpigler, a father of seven children. David worked with each of his children on math and reading, trying to prepare them for school as best he could. With seven children, he developed a methodology that seemed to work. He found, however, that his approach didn’t work quite as well with his youngest daughter. For some reason, the methodology didn’t seem to “click” with her as it did with the others. After trying to understand why the approach worked for his other children and not for her, he realized that he was trying to force fit her into a pre-existing educational approach rather than finding the one that best fit her needs. As he conducted research into this issue, David began to realize that there any many students that suffer in the same way. Students benefit when they receive personalized education. The concept of dynamic education was born."
    >
      {/* <Header
        image="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244723/AboutUs/Header/about-us_lvntvx.jpg"
        heading="About Us"
        description="About Learnatric and The Team"
        curriculum={false}
      /> */}
      <img
        src="https://learnatric-website-images.s3.amazonaws.com/12.16.2021/Meet+the+Team.png"
        alt=""
        width="100%"
        height="70%"
      />
      <div className={aboutUs}>
        <div className={aboutUsWrapper}>
          <DescriptionSection />
          <TeamSection />
        </div>
      </div>
    </Layout>
  )
}

export default AboutUs

import { ParentDashboardLayout } from 'components'
import { WithAuth } from 'hoc'
import { ReactElement } from 'react'
import { LearningAssessment } from 'sections'
import { learningAssessment } from 'styles/parent-dashboard/learning-assessment.module.scss'

const props = { title: 'Parent Dashboard - Learnatric' }

const LessonAssesment = (): ReactElement => {
  return (
    <ParentDashboardLayout {...props}>
      <div className={learningAssessment}>
        <LearningAssessment />
      </div>
    </ParentDashboardLayout>
  )
}

export default WithAuth(LessonAssesment)

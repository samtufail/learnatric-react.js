import { Button, Row, Col, Typography, Card, Avatar } from 'antd'
import {
  ParentDashboardLayout,
  Layout,
  Paragraph,
  CacheBuster,
} from 'components'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { WithAuth } from 'hoc'
import { logoutUser, setPage } from 'store/actions'
import { RoboClass } from '../../sections'

const HelpSupport = () => {
  const dispatch = useDispatch()
  useEffect(() => {
    // dispatch(setPage('Account Settings'))
  }, [])

  const Mailto = ({ email, subject, body, children }) => {
    return (
      <a
        href={`mailto:${email}?subject=${
          encodeURIComponent(subject) || ''
        }&body=${encodeURIComponent(body) || ''}`}
      >
        {children}
      </a>
    )
  }
  return (
    // <CacheBuster>
    <ParentDashboardLayout>
      <RoboClass
        heading="Welcome Guide for Parents"
        url="https://vimeo.com/645289712"
      />
      <RoboClass
        heading="Welcome Guide for Children"
        url="https://vimeo.com/638761876"
      />
      <Card
        hoverable
        title={<Typography.Title level={4}>Customer Support</Typography.Title>}
        style={{ marginTop: '10px' }}
      >
        <Typography.Title level={5}>
          Email:&nbsp;
          <Mailto email="ben@learnatric.com" subject="Customer Support" body="">
            ben@learnatric.com
          </Mailto>
        </Typography.Title>
        <Typography.Title level={5}>Phone: (845) 596-8158</Typography.Title>
      </Card>
    </ParentDashboardLayout>
    // </CacheBuster>
  )
}

export default WithAuth(HelpSupport)

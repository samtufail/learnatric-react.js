import { Button, Row, Col, Typography, Card, Avatar } from 'antd'
import {
  ParentDashboardLayout,
  Layout,
  Paragraph,
  Stripe,
  CardInput,
  CustomInput,
  CacheBuster,
} from 'components'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import moment from 'moment'
import LoadingOverlay from 'react-loading-overlay'
import { WithAuth } from 'hoc'
import { setPage } from 'store/actions'
import axiosInstance from 'api/axios'
import { AUTH_ACTION_TYPES } from '../../store/actionTypes'
import styles from 'styles/account-settings/billing.module.scss'

// import { BillingForm } from 'sections/AccountSettings/Billing/BillingForm.section'
// import { AdjustChildren } from 'sections/AccountSettings/Billing/AdjustChildren.section'
import {
  BillingForm,
  AdjustChildren,
  CancelSub,
} from 'sections/AccountSettings'

const Billing = (): ReactElement => {
  const { billingCardsContainer, billingCards } = styles
  const { account, billingDetails } = useSelector(
    ({ parent }: AppState) => parent
  )
  const { auth } = useSelector((root: AppState) => root)
  const dispatch = useDispatch()
  const [billingInfo, setBillingInfo] = useState<any>({})
  const [interval, setInterval] = useState<string>('')
  const [billingAmt, setBillingAmt] = useState<string>('')
  const [trailEnd, setTrialEnd] = useState<string>('')

  const calcBillingAmt = (freq) => {
    const { childs } = account
    if (freq === 'month') {
      if (childs > 1) {
        return `$${24 * (childs - 1) + 29}.00`
      } else {
        return '$29.00'
      }
    } else if (childs > 1) {
      return `$${240 * (childs - 1) + 290}.00`
    } else {
      return '$290.00'
    }
  }

  useEffect(() => {
    if (billingDetails !== null) {
      if (billingDetails.actStatus === 'Free Three Month Sign up') {
        setTrialEnd(billingDetails.trail_ends)
      } else {
        setBillingInfo(billingDetails)
        setInterval(
          billingDetails.plan.interval.charAt(0).toUpperCase() +
            billingDetails.plan.interval.slice(1)
        )
        setBillingAmt(calcBillingAmt(billingDetails.plan.interval))
      }
    }
  }, [billingDetails])

  return (
    // <CacheBuster>
    <LoadingOverlay
      active={auth.loading}
      spinner
      text="Updating Billing Info..."
    >
      <Stripe>
        {billingDetails !== null && (
          <ParentDashboardLayout>
            {billingDetails.actStatus !== 'Free Three Month Sign up' ? (
              <div className={billingCardsContainer}>
                <div className={billingCards}>
                  <Card
                    hoverable
                    title={
                      <Typography.Title level={4}>
                        Billing Details
                      </Typography.Title>
                    }
                    style={{ marginBottom: '10px' }}
                  >
                    <Typography.Title
                      level={5}
                    >{`Account status: ${billingInfo.actStatus}`}</Typography.Title>
                    <Typography.Title level={5}>{`Member since: ${moment
                      .unix(billingInfo.created)
                      .format('MM/DD/YYYY')}`}</Typography.Title>
                    <Typography.Title
                      level={5}
                    >{`Plan Type: ${interval}ly`}</Typography.Title>
                    <Typography.Title
                      level={5}
                    >{`Billing Amount: ${billingAmt}`}</Typography.Title>
                    {billingInfo.cancel_at_period_end ? (
                      <Typography.Title level={5}>{`Subscription Ends: ${moment
                        .unix(billingInfo.cancel_at)
                        .format('MM/DD/YYYY')}`}</Typography.Title>
                    ) : (
                      <Typography.Title level={5}>{`Next Billing Date: ${moment
                        .unix(billingInfo.current_period_end)
                        .format('MM/DD/YYYY')}`}</Typography.Title>
                    )}
                  </Card>
                  <Card
                    hoverable
                    title={
                      <Typography.Title level={4}>
                        Add or Remove Children
                      </Typography.Title>
                    }
                    style={{ marginBottom: '10px' }}
                  >
                    <AdjustChildren />
                  </Card>
                  <Card
                    hoverable
                    title={
                      <Typography.Title level={4}>
                        Change Payment Info
                      </Typography.Title>
                    }
                    style={{ marginBottom: '10px' }}
                  >
                    <BillingForm />
                  </Card>
                  <Card
                    hoverable
                    title={
                      <Typography.Title level={4}>
                        {billingDetails.cancel_at_period_end
                          ? 'Resume Subscription'
                          : 'Cancel Subscription'}
                      </Typography.Title>
                    }
                  >
                    <CancelSub />
                  </Card>
                </div>
              </div>
            ) : (
              <Card
                title={
                  <Typography.Title level={4}>Billing Details</Typography.Title>
                }
                style={{ marginBottom: '10px' }}
              >
                <Typography.Title
                  level={5}
                >{`Account status: ${billingDetails.actStatus}`}</Typography.Title>
                <Typography.Title
                  level={5}
                >{`Trial ends: ${billingDetails.trail_ends}`}</Typography.Title>
              </Card>
            )}
          </ParentDashboardLayout>
        )}
      </Stripe>
    </LoadingOverlay>
    // </CacheBuster>
  )
}

export default WithAuth(Billing)

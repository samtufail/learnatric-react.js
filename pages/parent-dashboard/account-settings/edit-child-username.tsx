import { LeftOutlined } from '@ant-design/icons'
import { Button, Card, Input, Typography, Modal } from 'antd'
import { ParentDashboardLayout } from 'components/Layout/ParentDashboardLayout.component'
import { WithAuth } from 'hoc'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import _ from 'lodash'
import styles from 'styles/account-settings/editChild.module.scss'
import { CustomInput } from 'components/atoms/FormFields/CustomInput.atom'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { updateChildUsername } from 'store/actions'

const validationSchema = Yup.object().shape({
  username: Yup.string().required('Username is required!'),
})

const EditChildUsername = (): ReactElement => {
  const {
    editChildContainer,
    editChildCardTitle,
    childAvatar,
    editChildtext,
    editChildButton,
  } = styles

  const [child, setChild] = useState<{ [key: string]: any } | undefined>()

  const children = useSelector(
    ({ parent }: AppState) => parent?.childPerformance
  )

  const router = useRouter()
  const { id } = router.query
  useEffect(() => {
    const child = _.find(children, (child) => {
      return child.childId === Number(id)
    })
    setChild(child)
  }, [id])

  const dispatch = useDispatch()
  const onSubmit = (values) => {
    const finalValues = {
      id: child?.childId,
      username: values.username,
    }
    dispatch(updateChildUsername(finalValues))
  }

  const formik = useFormik({
    initialValues: {
      username: '',
    },
    validationSchema,
    onSubmit,
  })

  return (
    <ParentDashboardLayout>
      <div className={editChildContainer}>
        <Card
          hoverable
          title={
            <Link href="/parent-dashboard/account-settings">
              <a>
                <Typography.Title level={4} className={editChildCardTitle}>
                  <LeftOutlined /> Edit Child Information
                </Typography.Title>
              </a>
            </Link>
          }
        >
          <form onSubmit={formik.handleSubmit}>
            <div className={editChildtext}>
              <Typography.Title level={5}>
                Child Name:{' '}
                <span style={{ fontSize: '16px', fontWeight: 400 }}>
                  {child?.firstName}
                </span>
              </Typography.Title>
              <Typography.Title level={5}>Edit Child Username</Typography.Title>
              <CustomInput
                formik={formik}
                name="username"
                type="text"
                placeholder="Enter new username here"
              />
            </div>
            <Button
              type="primary"
              htmlType="submit"
              className={editChildButton}
            >
              Update
            </Button>
          </form>
        </Card>
      </div>
    </ParentDashboardLayout>
  )
}

export default WithAuth(EditChildUsername)

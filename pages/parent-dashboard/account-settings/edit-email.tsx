import { Button, Typography, Card, Input, Modal } from 'antd'
import { ParentDashboardLayout } from 'components'
import { ReactElement } from 'react'
import { WithAuth } from 'hoc'
import styles from 'styles/account-settings/editEmail.module.scss'
import { useFormik } from 'formik'
import { LeftOutlined } from '@ant-design/icons'
import Link from 'next/link'
import { CustomInput } from 'components/atoms/FormFields/CustomInput.atom'
import { useSelector, useDispatch } from 'react-redux'
import * as Yup from 'yup'
import { updateEmail } from 'store/actions'

const validationSchema = Yup.object().shape({
  email: Yup.string().required('Email is required!').email('Invalid Email'),
})

const Email = (): ReactElement => {
  const { editEmailContainer, editEmailTitle, editEmailButton } = styles
  const { email, id } = useSelector(({ parent }: AppState) => parent.account)
  const dispatch = useDispatch()
  const onSubmit = (values) => {
    const finalValues = {
      id,
      email: values.email?.toLowerCase(),
    }

    dispatch(updateEmail(finalValues))
  }

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema,
    onSubmit,
  })
  return (
    <ParentDashboardLayout>
      <div className={editEmailContainer}>
        <form onSubmit={formik.handleSubmit}>
          <Card
            hoverable
            title={
              <Link href="/parent-dashboard/account-settings">
                <a>
                  <Typography.Title level={4} className={editEmailTitle}>
                    <LeftOutlined /> Edit Email
                  </Typography.Title>
                </a>
              </Link>
            }
          >
            <div>
              <Typography.Title level={5}>
                Email:{' '}
                <span style={{ fontSize: '16px', fontWeight: 400 }}>
                  {email}
                </span>
              </Typography.Title>
              <Typography.Title level={5}>Edit Email</Typography.Title>
              <CustomInput
                name="email"
                formik={formik}
                type="text"
                placeholder="Enter new email address here."
              />
            </div>
            <Button
              type="primary"
              htmlType="submit"
              className={editEmailButton}
            >
              update
            </Button>
          </Card>
        </form>
      </div>
    </ParentDashboardLayout>
  )
}

export default WithAuth(Email)

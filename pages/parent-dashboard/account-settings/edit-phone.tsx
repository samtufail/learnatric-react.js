import { Button, Typography, Card, Input, Modal } from 'antd'
import { ParentDashboardLayout } from 'components'
import { ReactElement } from 'react'
import { WithAuth } from 'hoc'
import styles from 'styles/account-settings/editPhone.module.scss'
import { LeftOutlined } from '@ant-design/icons'
import Link from 'next/link'
import * as Yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'
import { CustomInputPhone } from 'components/atoms/FormFields/CustomInputPhone.atom'
import { updatePhone } from 'store/actions'

const validationSchema = Yup.object().shape({
  phoneNumber: Yup.string().required('Phone Number is required!'),
})

const PhoneNumber = (): ReactElement => {
  const { editPhoneContainer, editPhoneTitle, editPhoneButton } = styles
  const dispatch = useDispatch()
  const { phoneNumber, id } = useSelector(
    ({ parent }: AppState) => parent.account
  )

  const onSubmit = (values) => {
    const finalValues = {
      id,
      phoneNumber: values.phoneNumber,
    }
    dispatch(updatePhone(finalValues))
  }
  const formik = useFormik({
    initialValues: {
      phoneNumber: '',
    },
    validationSchema,
    onSubmit,
  })

  return (
    <ParentDashboardLayout>
      <div className={editPhoneContainer}>
        <form onSubmit={formik.handleSubmit}>
          <Card
            hoverable
            title={
              <Link href="/parent-dashboard/account-settings">
                <a>
                  <Typography.Title level={4} className={editPhoneTitle}>
                    <LeftOutlined /> Edit Phone Number
                  </Typography.Title>
                </a>
              </Link>
            }
          >
            <div>
              <Typography.Title level={5}>
                Phone Number:{' '}
                <span style={{ fontSize: '16px', fontWeight: 400 }}>
                  {phoneNumber}
                </span>
              </Typography.Title>
              <Typography.Title level={5}>New Phone Number</Typography.Title>
              <CustomInputPhone formik={formik} label="" name="phoneNumber" />
            </div>
            <Button
              type="primary"
              htmlType="submit"
              className={editPhoneButton}
            >
              update
            </Button>
          </Card>
        </form>
      </div>
    </ParentDashboardLayout>
  )
}

export default WithAuth(PhoneNumber)

import { Button, Typography, Card, Input, Modal } from 'antd'
import { LeftOutlined } from '@ant-design/icons'
import { ParentDashboardLayout, CustomInput } from 'components'
import { ReactElement } from 'react'
import { WithAuth } from 'hoc'
import styles from 'styles/account-settings/editPassword.module.scss'
import Link from 'next/link'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { updateParentPassword } from 'store/actions'

const validationSchema = Yup.object({
  current_pass: Yup.string().required('Password is Required'),
  new_pass: Yup.string().required('New Password is required'),
})

const Password = (): ReactElement => {
  const { editPasswordContainer, editPasswordTitle, editPasswordButton } =
    styles

  const { id } = useSelector(({ parent }: AppState) => parent.account)

  const dispatch = useDispatch()
  const onSubmit = (values) => {
    const finalValues = {
      id,
      current_pass: values.current_pass,
      new_pass: values.new_pass,
    }
    dispatch(updateParentPassword(finalValues))
  }

  const formik = useFormik({
    initialValues: {
      current_pass: '',
      new_pass: '',
    },
    validationSchema,
    onSubmit,
  })
  return (
    <ParentDashboardLayout>
      <div className={editPasswordContainer}>
        <form onSubmit={formik.handleSubmit}>
          <Card
            hoverable
            title={
              <Link href="/parent-dashboard/account-settings">
                <a>
                  <Typography.Title level={4} className={editPasswordTitle}>
                    <LeftOutlined /> Change Password
                  </Typography.Title>
                </a>
              </Link>
            }
          >
            <div>
              <Typography.Title level={5}>Current Password</Typography.Title>
              <CustomInput
                formik={formik}
                name="current_pass"
                type="password"
              />
              <Typography.Title level={5}>New Password</Typography.Title>
              <CustomInput formik={formik} name="new_pass" type="password" />
            </div>
            <Button
              type="primary"
              htmlType="submit"
              className={editPasswordButton}
            >
              update
            </Button>
          </Card>
        </form>
      </div>
    </ParentDashboardLayout>
  )
}

export default WithAuth(Password)

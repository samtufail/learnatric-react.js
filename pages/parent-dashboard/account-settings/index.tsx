import { Button, Typography, Card } from 'antd'
import { ParentDashboardLayout } from 'components'
import { ReactElement, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { WithAuth } from 'hoc'
import styles from 'styles/account-settings/index.module.scss'
import Link from 'next/link'

const AccountSettings = (): ReactElement => {
  const {
    accountSettingMainContainer,
    cardInputContainer,
    accountDetailsHeading,
    accountButton,
    childInfoContainer,
    childAvatar,
    childInfoMessage,
    childInfoHeading,
    childButton,
  } = styles
  const dispatch = useDispatch()
  useEffect(() => {
    // dispatch(setPage('Account Settings'))
  }, [])
  const { email, phoneNumber } = useSelector(
    ({ parent }: AppState) => parent.account
  )
  const children = useSelector(
    ({ parent }: AppState) => parent.childPerformance
  )

  return (
    <ParentDashboardLayout>
      <div className={accountSettingMainContainer}>
        <Card
          hoverable
          title={
            <Typography.Title level={4}>Account Settings</Typography.Title>
          }
        >
          <div className={cardInputContainer}>
            <Typography.Title className={accountDetailsHeading} level={5}>
              Email:{' '}
              <span style={{ fontSize: '16px', fontWeight: 400 }}>{email}</span>
            </Typography.Title>
            <Link href="/parent-dashboard/account-settings/edit-email">
              <a>
                <Button className={accountButton} type="primary">
                  Edit
                </Button>
              </a>
            </Link>
          </div>
          <div className={cardInputContainer}>
            <Typography.Title level={5} className={accountDetailsHeading}>
              Password:{' '}
              <span style={{ fontSize: '16px', fontWeight: 400 }}>
                ●●●●●●●●
              </span>
            </Typography.Title>
            <Link href="/parent-dashboard/account-settings/edit-password">
              <a>
                <Button className={accountButton} type="primary">
                  Edit
                </Button>
              </a>
            </Link>
          </div>
          <div className={cardInputContainer}>
            <Typography.Title level={5} className={accountDetailsHeading}>
              Phone Number:{' '}
              <span style={{ fontSize: '16px', fontWeight: 400 }}>
                {phoneNumber}
              </span>
            </Typography.Title>
            <Link href="/parent-dashboard/account-settings/edit-phone">
              <a>
                <Button className={accountButton} type="primary">
                  Edit
                </Button>
              </a>
            </Link>
          </div>
        </Card>
        <Card
          hoverable
          title={
            <Typography.Title level={4}>Student Information</Typography.Title>
          }
        >
          {children.map((child) => {
            return (
              <div className={childInfoContainer}>
                <div className={childInfoMessage}>
                  <Typography.Title level={5} className={childInfoHeading}>
                    Child Name :{' '}
                    <span style={{ fontSize: '16px', fontWeight: 400 }}>
                      {child?.firstName}
                    </span>
                  </Typography.Title>
                  <div>
                    <Link
                      href={`/parent-dashboard/account-settings/edit-child-username?id=${child?.childId}`}
                    >
                      <a>
                        <Button className={childButton} type="primary">
                          Edit Username
                        </Button>
                      </a>
                    </Link>
                  </div>
                  <div style={{ marginLeft: 10 }}>
                    <Link
                      href={`/parent-dashboard/account-settings/edit-child-password?id=${child?.childId}`}
                    >
                      <a>
                        <Button className={childButton} type="primary">
                          Edit Password
                        </Button>
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            )
          })}
        </Card>
      </div>
    </ParentDashboardLayout>
  )
}

export default WithAuth(AccountSettings)

/* eslint-disable no-constant-condition */
import { Button, Row, Col, Typography, Card, Avatar, Modal } from 'antd'
import {
  ParentDashboardLayout,
  Layout,
  Paragraph,
  CacheBuster,
} from 'components'
import LoadingOverlay from 'react-loading-overlay'
import { ReactElement, useEffect, useState } from 'react'
import {
  parentDashboardContainer,
  parentDashboardButtonContainer,
  dashboardButton,
  CardContainer,
  CardContainerCard,
  CardTagName,
  CardTagRating,
  ChartContainer,
  ChartContainerChart,
  activeDashboardButton,
  lineChart,
} from 'styles/parent-dashboard/index.module.scss'
// Coming Soon Imports
// import axios from 'axios'
import { Line } from 'react-chartjs-2'

import { WithAuth } from 'hoc'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { setPage, switchToChild, getChildPerformance } from 'store/actions'
import { PARENT_ACTION_TYPES } from 'store/actionTypes'
import axiosInstance from 'api/axios'
import { DashboardStaticCard } from 'sections'
import { toast } from 'react-toastify'

const { Meta } = Card
const props = { title: 'Parent Dashboard - Learnatric' }

const ParentDashboard = (): ReactElement => {
  // Coming Soon Imports
  const { account, childPerformance, loading } = useSelector(
    ({ parent }: AppState) => parent
  )
  // const { auth } = useSelector((root: AppState) => root)
  // const [data, setData] = useState<any>([])
  const [childData, setChildData] = useState<any>()
  const [topData, setTopData] = useState<any>()
  const [bottomData, setBottomData] = useState<any>()
  const [activeChild, setActiveChild] = useState<string>('')
  const [activeAvatar, setActiveAvatar] = useState<string>('')
  const [assessmentComplete, setAssessmentComplete] = useState<boolean>()

  const options: any = {
    responsive: true,
    maintainAspectRatio: false,
    animations: {
      tension: {
        duration: 1000,
        easing: 'easeInOutBounce',
        from: 1,
        to: 0,
        // loop: true
      },
    },
    plugins: {
      legend: {
        // fullSize: true,
        position: 'bottom',
        display: true,
        labels: {
          color: '#495f74',
          font: { size: 14 },
        },
      },
      title: {
        display: true,
        text: 'Domain Averages',
        color: '#495f74',
        font: {
          size: 18,
        },
      },
      tooltip: {
        backgroundColor: '#495f74',
      },
    },
    transitions: {
      show: {
        animations: {
          x: {
            from: 0,
          },
          y: {
            from: 0,
          },
        },
      },
      hide: {
        animations: {
          x: {
            to: 0,
          },
          y: {
            to: 0,
          },
        },
      },
    },
  }
  const router = useRouter()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setPage('Parent Dashboard'))
    //this should only happen if this page is routed to from create-child-profile page
    //otherwise this will happen in the userLogin action
    // if (childPerformance === null) {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    getChildPerformance().then((res) => {
      if (res.status === 200) {
        const { result } = res.data
        dispatch({
          type: PARENT_ACTION_TYPES.GET_CHILD_PERFORMANCE,
          payload: result,
        })
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      } else {
        console.log('Response', res)
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
        toast.error('Something Went Wrong')
      }
      dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
    })
    // }
  }, [])
  useEffect(() => {
    if (childPerformance !== null) {
      setChildData(childPerformance[0].formattedAvgs)
      setTopData(childPerformance[0].topAndBottom?.top)
      setBottomData(childPerformance[0].topAndBottom?.bottom)
      setActiveChild(childPerformance[0].firstName)
      setActiveAvatar(childPerformance[0].avatar)
      setAssessmentComplete(childPerformance[0].assessment_complete)
    }
  }, [childPerformance])

  const handleChild = (data) => {
    setActiveChild(data.firstName)
    setChildData(data.formattedAvgs)
    setTopData(data.topAndBottom.top)
    setBottomData(data.topAndBottom.bottom)
    setActiveAvatar(data.avatar)
    setAssessmentComplete(data.assessment_complete)
  }

  const childDashClick = () => {
    if (account.childs > 1) {
      Modal.info({
        title: 'Pick Child',
        // className: 'modal__container',
        okText: 'Cancel',
        content: childPerformance.map((child, idx) => (
          <div
            key={child.id}
            style={{
              display: 'flex',
              flexDirection: 'row',
              marginBottom: '8px',
              cursor: 'pointer',
            }}
            onClick={() => {
              dispatch(switchToChild(child.childId))
              Modal.destroyAll()
            }}
          >
            <Avatar src={child.avatar} size={45} />
            <Typography.Title level={5}>{child.firstName}</Typography.Title>
          </div>
        )),
      })
    } else {
      dispatch(switchToChild(childPerformance[0].childId))
    }
  }
  return (
    // <CacheBuster>
    <LoadingOverlay
      active={loading}
      spinner
      text="Gathering Child Performace Data..."
    >
      <ParentDashboardLayout {...props}>
        {childPerformance !== null && (
          <div className={parentDashboardContainer}>
            <div className={parentDashboardButtonContainer}>
              {childPerformance.length > 1 ? (
                childPerformance.map((data) => {
                  const { firstName } = data
                  const dashboardButtonClass =
                    firstName === activeChild
                      ? activeDashboardButton
                      : dashboardButton
                  return (
                    <Button
                      key={firstName}
                      className={`${dashboardButtonClass}`}
                      type="primary"
                      onClick={() => handleChild(data)}
                    >
                      {firstName}
                      {!data.is_active_subscription && (
                        <span style={{ color: 'red' }}>&nbsp;canceled</span>
                      )}
                    </Button>
                  )
                })
              ) : (
                <Typography.Title level={1} style={{ color: '#495f74' }}>
                  {`${activeChild}'s Progress`}
                </Typography.Title>
              )}
            </div>
            <div className={parentDashboardButtonContainer}>
              <Button
                type="primary"
                // size="large"
                className={activeDashboardButton}
                onClick={() => childDashClick()}
              >
                {'Go to Child Dashboard'}
              </Button>
            </div>
            {assessmentComplete ? (
              <div className={ChartContainer}>
                <div className={ChartContainerChart}>
                  <Line data={childData} options={options} />
                </div>
                {topData && (
                  <div className={CardContainer}>
                    <div className={CardContainerCard}>
                      <Card
                        hoverable
                        style={{ borderRadius: 7 }}
                        className="parent-card"
                      >
                        <Meta
                          title="Top 3 Tags"
                          className="parent-card-meta"
                          avatar={<Avatar src={activeAvatar} size={55} />}
                        />
                        {topData.map((topTag, idx) => {
                          let { tag_rating } = topTag
                          const tagName = topTag['Tag.tags']
                          tag_rating = tag_rating.toString()
                          tag_rating =
                            tag_rating.indexOf('.') > -1
                              ? tag_rating.slice(0, tag_rating.length - 1)
                              : `${tag_rating}.0`
                          return (
                            <>
                              <div
                                style={{
                                  marginTop: '5px',
                                  display: 'flex',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <Paragraph className={CardTagName} key={idx}>
                                  {tagName}
                                </Paragraph>
                                <div
                                  className={CardTagRating}
                                  style={{ color: 'green' }}
                                >
                                  <strong>{tag_rating}</strong>
                                </div>
                              </div>
                            </>
                          )
                        })}
                      </Card>
                    </div>
                    <div className={CardContainerCard}>
                      <Card
                        hoverable
                        bordered={true}
                        style={{ borderRadius: 7 }}
                        className="parent-card"
                      >
                        <Meta
                          title="Bottom 3 Tags"
                          className="parent-card-meta"
                          avatar={<Avatar src={activeAvatar} size={55} />}
                        />
                        {bottomData.map((bottomTag, idx) => {
                          let { tag_rating } = bottomTag
                          const tagName = bottomTag['Tag.tags']
                          tag_rating = tag_rating.toString()
                          tag_rating =
                            tag_rating.indexOf('.') > -1
                              ? Number.parseInt(tag_rating, 10).toFixed(1)
                              : `${tag_rating}.0`
                          return (
                            <>
                              <div
                                style={{
                                  marginTop: '5px',
                                  display: 'flex',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <Paragraph className={CardTagName} key={idx}>
                                  {tagName}
                                </Paragraph>
                                <div
                                  className={CardTagRating}
                                  style={{ color: 'red' }}
                                >
                                  <strong>{tag_rating}</strong>
                                </div>
                              </div>
                            </>
                          )
                        })}
                      </Card>
                    </div>
                    <div className={CardContainerCard}>
                      <DashboardStaticCard />
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <img src="https://res.cloudinary.com/learnatric/image/upload/v1642020917/components/Assessment_data_display_image_2_tlofbv.png" />
            )}
            {/* <DashboardCard /> */}
          </div>
        )}
      </ParentDashboardLayout>
    </LoadingOverlay>
    // </CacheBuster>
  )
}

export default WithAuth(ParentDashboard)

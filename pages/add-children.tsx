import { useSelector } from 'react-redux'
import { Layout } from 'components'
import { AddChildrenForm } from 'sections'
import LoadingOverlay from 'react-loading-overlay'
import { ReactElement } from 'react'
import { WithAuth } from 'hoc'
const AddChildren = (): ReactElement => {
  const { loading } = useSelector(({ parent }: AppState) => parent)
  return (
    <LoadingOverlay
      active={loading}
      spinner
      text="Adding Child..."
      className="h-screen"
    >
      <Layout title="Add Children - Learnatric">
        <div>
          <AddChildrenForm />
        </div>
      </Layout>
    </LoadingOverlay>
  )
}

export default WithAuth(AddChildren)

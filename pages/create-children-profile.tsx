import { useDispatch, useSelector } from 'react-redux'
import { Layout } from 'components'
import { CreateChildrenProfileForm } from 'sections'
import LoadingOverlay from 'react-loading-overlay'
import { ReactElement, useEffect } from 'react'
import { getChildsOfParent } from 'store/actions'
import { WithAuth } from 'hoc'

const CreateChildrenProfile = (): ReactElement => {
  const { loading } = useSelector(({ parent }: AppState) => parent)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getChildsOfParent())
  }, [])

  return (
    <LoadingOverlay
      active={loading}
      spinner
      text="Setting up Account..."
      className="h-screen"
    >
      <Layout title="Create Children Profile - Learnatric">
        <div>
          <CreateChildrenProfileForm />
        </div>
      </Layout>
    </LoadingOverlay>
  )
}

export default WithAuth(CreateChildrenProfile)

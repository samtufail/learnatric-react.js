import { ReactElement, useEffect } from 'react'
import { useRouter } from 'next/router'
import { ConfigProvider } from 'antd'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import LogRocket from 'logrocket'
import setupLogRocketReact from 'logrocket-react'
import { store, persistor } from 'store'
import * as gtag from '../lib/ga/index'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import * as fbq from '../lib/fbPixel/index'

import 'tailwindcss/tailwind.css'
import 'antd/dist/antd.css'
import 'styles/globals.scss'
interface Props {
  Component: any
  pageProps: any
}

const MyApp = ({ Component, pageProps }: Props): ReactElement => {
  const router = useRouter()
  useEffect(() => {
    const handleRouteChange = (url: URL) => {
      if (
        process.env.NODE_ENV === 'production' &&
        process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS &&
        process.env.NEXT_PUBLIC_FACEBOOK_PIXEL_ID
      ) {
        gtag.pageview(url)
        fbq.pageview(url)
      }
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])
  useEffect(() => {
    // console.log('log rocket?: ', process.env.NEXT_PUBLIC_LOGROCKET)
    if (
      process.env.NODE_ENV === 'production' &&
      process.env.NEXT_PUBLIC_LOGROCKET
    ) {
      LogRocket.init(process.env.NEXT_PUBLIC_LOGROCKET)
      setupLogRocketReact(LogRocket)
    }
  }, [])

  return (
    <ConfigProvider>
      <ToastContainer />
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Component {...pageProps} />
        </PersistGate>
      </Provider>
    </ConfigProvider>
  )
}

export default MyApp

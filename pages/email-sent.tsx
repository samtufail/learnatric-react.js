import { Button, Card, Typography } from 'antd'
import { Layout } from 'components'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { ReactElement } from 'react'
import { useSelector } from 'react-redux'
import styles from 'styles/email-sent.module.scss'
import { CheckCircleTwoTone } from '@ant-design/icons'

const AccountRecovered = (): ReactElement => {
  const {
    recoveredAccountMainContainer,
    recoveredCardTitle,
    recoveredCardBody,
    recoveredButtonContainer,
    recoveredButton,
  } = styles

  const router = useRouter()

  const { child } = router.query

  return (
    <Layout
      title="Account Recovered - Learnatric"
      description="Congractulations Your account is recovered"
    >
      <div
        className={recoveredAccountMainContainer}
        style={{ padding: '2rem' }}
      >
        <Card style={{ maxWidth: '500px' }} hoverable>
          <Typography.Title level={4} className={recoveredCardTitle}>
            <CheckCircleTwoTone
              style={{ fontSize: 62 }}
              twoToneColor="#52c41a"
            />
          </Typography.Title>
          <div className={recoveredCardBody}>
            {!child ? (
              <Typography.Title level={5}>
                <span style={{ fontSize: '18px', fontWeight: 500 }}>
                  We just emailed you a temporary password. Please check your
                  email.
                </span>
              </Typography.Title>
            ) : (
              <Typography.Title level={5}>
                <span style={{ fontSize: '17px', fontWeight: 500 }}>
                  We just emailed your parent a temporary password for you
                </span>
              </Typography.Title>
            )}
          </div>
          {/* <a className={frogetPassword}>Forgotten password?</a> */}

          <div className={recoveredButtonContainer}>
            <Link href="/login">
              <a>
                <Button type="primary" className={recoveredButton}>
                  Back to Login
                </Button>
              </a>
            </Link>
          </div>
        </Card>
      </div>
    </Layout>
  )
}

export default AccountRecovered

import { Button, Typography } from 'antd'
import { Paragraph, ChildDashboardHomeLayout } from 'components'
import Link from 'next/link'
import { ReactElement } from 'react'
import styles from 'styles/child-dashboard/new-home.module.scss'

const NewHome = (): ReactElement => {
  const {
    childDashboardHomeContainer,
    childDashboardHomeImageTablet,
    childDashboardHomeContent,
    contentHeading,
    contentText,
    homeButton,
    childDashboardHomeImage,
  } = styles
  return (
    <ChildDashboardHomeLayout title="home  design - Learnatric">
      <div className={childDashboardHomeContainer}>
        <div className={childDashboardHomeImageTablet}>
          <img src="/img/child-dashboard/home-banner.png" />
        </div>
        <div className={childDashboardHomeContent}>
          <Typography.Title level={1} className={contentHeading}>
            <span style={{ color: '#e66a2c ' }}>Learnatric:</span> <br />
            Personlized, Interactive <br /> Math Education
          </Typography.Title>
          <Paragraph className={contentText}>
            Learnatric empowers childrento:
            <ul>
              <li>Ignite a passion for learning.</li>
              <li>Learn efficiently with limited screen time.</li>
              <li>Work throught problems independently.</li>
              <li>Break down hard-to-reach concepts.</li>
              <li>Speed up learning through tutoring strategies.</li>
            </ul>
          </Paragraph>
          <Link href="/signup">
            <a>
              <Button className={homeButton} type="primary">
                SignUp For Free Trial
              </Button>
            </a>
          </Link>
        </div>
        <div className={childDashboardHomeImage}>
          <img src="/img/child-dashboard/home-banner.png" />
        </div>
      </div>
    </ChildDashboardHomeLayout>
  )
}

export default NewHome

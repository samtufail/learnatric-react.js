import { Layout as ChildLayout } from 'components'
import { DashboardWrapper } from 'sections'
import { ReactElement } from 'react'

const Rooms = (): ReactElement => {
  return (
    <ChildLayout title="Child Dashboard - Learnatric">
      <DashboardWrapper>
        <div>Rooms Child</div>
      </DashboardWrapper>
    </ChildLayout>
  )
}

export default Rooms

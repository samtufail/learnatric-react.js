import { LessonsActivityComponent } from '../../components/LessonsActivity/LessonsActivity.component'
import { DashboardWrapper } from 'sections'
import { Layout, ChildDashboardLayout } from 'components'
import { ReactElement } from 'react'
import { WithAuth } from 'hoc'
// import { ChildDashboardLayout } from 'components/Layout/ChildDashboardLayout.component'

const LessonsActivity = (): ReactElement => {
  return (
    <ChildDashboardLayout
      title="Lesson Activity - Learnatric"
      description="Where all the learning happens. We have multiple choice questions, number pad questions where after calculating the answer your child can enter their response, and tens frames that can be built used to teach your child to count in terms or tens."
    >
      <DashboardWrapper>
        <LessonsActivityComponent />
      </DashboardWrapper>
    </ChildDashboardLayout>
  )
}

export default WithAuth(LessonsActivity)

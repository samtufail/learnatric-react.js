import {
  ChildDashboardImages,
  ChildDashboardLayout,
  SelectBoyChildToy,
  SelectGirlChildToy,
} from 'components'
import {
  addUserLesson,
  getLessonForChild,
  startInitialVideo,
  changeVideoDisplay,
  getAssessmentQuestions,
  updateTrainWobble,
} from 'store/actions'
import LoadingOverlay from 'react-loading-overlay'
import React, { useState, ReactElement, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
// import { useRouter } from 'next/router'
import { useRouter } from 'next/dist/client/router'
import styles from 'styles/child-dashboard/new-design.module.scss'
import { CHILD_ACTION_TYPES } from 'store/actionTypes'
import {Next} from "icons"

const ChildDashboard = (): ReactElement => {
  const dispatch = useDispatch()
  const router = useRouter()
  const {
    loading,
    account,
    currentLesson,
    cameFromLesson,
    playInitialVideo,
    initialVideoDisplay,
    // wobble,
    // trainNumber,
  } = useSelector(({ child }: AppState) => child)

  let { toys } = account

  toys = toys ? JSON.parse(toys) : null
  useEffect(() => {
    // if (account.isFirstLogin) {
    //   dispatch(changeVideoDisplay('flex'))
    // }
    if (cameFromLesson) {
      changeLesson()
      setTimeout(() => {
        dispatch({
          type: CHILD_ACTION_TYPES.FINISHED_LESSON,
          payload: { bool: false },
        })
      }, 2000)
    }
    if (!account.assessment_complete) {
      dispatch(getAssessmentQuestions(account))
    } else {
      dispatch(getLessonForChild())
    }
  }, [])
  const onButtonClick = () => {
    if (account.assessment_complete) {
      dispatch(addUserLesson(currentLesson.id))
    }
    router.push('/child-dashboard/lesson-activity')
  }

  const { wobble, trainNumber } = account
  const [showModal, setShowModal] = useState(false)
  const changeLesson = async () => {
    // dispatch({ type: CHILD_ACTION_TYPES.WOBBLE, payload: wobble + 1 })
    dispatch(updateTrainWobble(account.id, wobble + 1, null))
    setTimeout(() => {
      if (trainNumber === 7) {
        // dispatch({ type: CHILD_ACTION_TYPES.WOBBLE, payload: 0 })
        // dispatch({ type: CHILD_ACTION_TYPES.TRAIN_NUMBER, payload: 1 })
        dispatch(updateTrainWobble(account.id, 0, 1))
        setShowModal(true)
      } else {
        dispatch(updateTrainWobble(account.id, null, trainNumber + 1))
      }
    }, 1000)
  }

  const {
    childDashboardContainer,
    childDashboardTrackImage,
    childDashboardChildImage,
    childDashboardTrainImageContainer,
    childDashboardTrainImage,
    childDashboardTrainImageText,
    childDashboardTitle,
    childDashboardTitleText,
    childDashboardButtonContainer,
    childDashboardButton,
    childDashboardButtonText,
  } = styles

  return (
    <>
      {localStorage.getItem('TYPE') === 'CHILD' ? (
        <LoadingOverlay
          active={loading}
          spinner
          text="Loading"
          className="h-full"
        >
          <ChildDashboardLayout title="Learnatric - Child Dashboard">
            <div className={childDashboardContainer}>
              <img
                className={childDashboardTrackImage}
                src="/img/child-dashboard/track-castle.png"
                alt="child-castle"
              />
              {account?.avatarLarge && (
                <img
                  className={childDashboardChildImage}
                  src={account.avatarLarge}
                  alt="child"
                />
              )}
              <div
                className={childDashboardTrainImageContainer}
                onAnimationEnd={() =>
                  dispatch({ type: CHILD_ACTION_TYPES.WOBBLE, payload: wobble })
                }
                // @ts-ignore
                wobble={wobble}
              >
                <img
                  className={childDashboardTrainImage}
                  src="/img/child-dashboard/train-new.png"
                  alt="milestone"
                />
                <span className={childDashboardTrainImageText}>
                  {trainNumber}
                </span>
              </div>

              <div className={childDashboardTitle}>
                <p className={childDashboardTitleText}>
                  {!account.assessment_complete
                    ? 'Assessment'
                    : `Lesson: ${currentLesson && currentLesson.lesson_name}`}
                </p>
              </div>
              <div className={childDashboardButtonContainer}>
                <button
                  className={childDashboardButton}
                  onClick={onButtonClick}
                >
                  <img src="/img/child-dashboard/123.png" alt="button" />
                  <span className={childDashboardButtonText}>
                    {!account.assessment_complete
                      ? 'Take Assessment'
                      : 'Begin Lesson'}
                  <Next/>
                  </span>
                </button>
                {/* <button
                  type="button"
                  className={childDashboardButton}
                  onClick={changeLesson}
                >
                  <img src="/img/child-dashboard/123.png" alt="button" />
                  <span className={childDashboardButtonText}>Next</span>
                </button> */}
              </div>
              <ChildDashboardImages toys={toys} />
              {account.gender === 'Male' && (
                <SelectBoyChildToy
                  show={showModal}
                  setShow={setShowModal}
                  toys={toys}
                />
              )}
              {account.gender === 'Female' && (
                <SelectGirlChildToy
                  show={showModal}
                  setShow={setShowModal}
                  toys={toys}
                />
              )}
            </div>
          </ChildDashboardLayout>
        </LoadingOverlay>
      ) : null}
    </>
  )
}
export default ChildDashboard

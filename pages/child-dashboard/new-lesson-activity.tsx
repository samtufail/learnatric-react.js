import { ChildDashboardLayout } from 'components/Layout/ChildDashboardLayout.component'
import { LessonsActivityComponent } from 'components/LessonsActivity/LessonsActivity.component'
import { ReactElement } from 'react'
import { DashboardWrapper } from 'sections/Dashboard/DashboardWrapper'
import styles from 'styles/child-dashboard/new-lesson-activity.module.scss'

const NewLessonActivity = (): ReactElement => {
  return (
    <ChildDashboardLayout title="Learnatric">
      <DashboardWrapper>
        <LessonsActivityComponent />
      </DashboardWrapper>
    </ChildDashboardLayout>
  )
}

export default NewLessonActivity

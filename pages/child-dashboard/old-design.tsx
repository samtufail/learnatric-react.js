import { Layout as ChildLayout, Body20Bold, CacheBuster } from 'components'
import ReactPlayer from 'react-player'
import { ReactElement, useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Image from 'next/image'
import { Star } from '../../components/Star/Star.component'
import { dashboardBackgroundUrls } from '../../components/Constants/index'
import { Button, Typography } from 'antd'
import { CloseCircleOutlined } from '@ant-design/icons'
import { DashboardWrapper } from 'sections'
import { WithAuth } from 'hoc'

import {
  childDashboardContainer,
  childDashboardImageContainer,
  childDashboardLessontext,
  childDashboardAvatarContainer,
  childDashboardAvatar,
  childDashboardButton,
  containerHeight,
  ChildDashboardStar,
} from 'styles/child-dashboard.module.scss'

import {
  addUserLesson,
  getLessonForChild,
  startInitialVideo,
  changeVideoDisplay,
  getAssessmentQuestions,
} from 'store/actions'
import { useRouter } from 'next/router'
import LoadingOverlay from 'react-loading-overlay'

const ChildDashboard = (): ReactElement => {
  const dispatch = useDispatch()
  const router = useRouter()
  const {
    loading,
    account,
    currentLesson,
    cameFromLesson,
    playInitialVideo,
    initialVideoDisplay,
  } = useSelector(({ child }: AppState) => child)
  const { grade } = account
  // const [videoDisplay, setVideoDisplay] = useState<string>('flex')
  const [isVideoPlaying, setIsVideoPlaying] = useState<boolean>(true)
  const values = ['Pre-K', 'Kindergarten', 'First', 'Second', 'Third']
  // eslint-disable-next-line prefer-template
  const index = values.indexOf(grade) + ''

  const backgdURL = dashboardBackgroundUrls[index]?.url

  useEffect(() => {
    if (account.isFirstLogin) {
      dispatch(changeVideoDisplay('flex'))
    }

    if (!account.assessment_complete) {
      dispatch(getAssessmentQuestions(account))
    } else {
      dispatch(getLessonForChild())
    }
  }, [])

  const onButtonClick = () => {
    if (account.assessment_complete) {
      dispatch(addUserLesson(currentLesson.id))
    }
    router.push('/child-dashboard/lesson-activity')
  }

  return (
    // <CacheBuster>
    <ChildLayout
      title="Child Dashboard - Learnatric"
      description="Dashboard displaying your childs avatar and next lesson description"
    >
      <LoadingOverlay
        active={loading}
        spinner
        text="Loading"
        className="h-full"
      >
        <div className={containerHeight}>
          <DashboardWrapper>
            <div className={childDashboardContainer}>
              {cameFromLesson && (
                <div className={ChildDashboardStar}>
                  <Star />
                </div>
              )}
              {(account.isFirstLogin || playInitialVideo) && (
                <div
                  style={{
                    display: `${initialVideoDisplay}`,
                    flexDirection: 'column',
                    position: 'absolute',
                    alignSelf: 'center',
                    alignContent: 'space-around',
                    zIndex: 10,
                    height: '92vh',
                    width: '100%',
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                  }}
                >
                  <div
                    style={{
                      position: 'absolute',
                      top: 0,
                      right: '10px',
                    }}
                  >
                    <Button
                      type="text"
                      icon={
                        <CloseCircleOutlined
                          style={{
                            fontSize: '38px',
                            color: 'white',
                          }}
                        />
                      }
                      onClick={() => {
                        dispatch(changeVideoDisplay('none'))
                        setIsVideoPlaying(false)
                        dispatch(startInitialVideo(false))
                      }}
                    ></Button>
                  </div>
                  <ReactPlayer
                    url="https://vimeo.com/638761876"
                    width="80%"
                    height="100%"
                    playing={playInitialVideo}
                    controls={true}
                    style={{ marginLeft: 'auto', marginRight: 'auto' }}
                  />
                </div>
              )}
              <div
                className={childDashboardImageContainer}
                style={{
                  backgroundImage: `linear-gradient(0deg, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)),url(${backgdURL})`,
                }}
              >
                {/* {lessonsAndQuestions?.id ? ( */}
                <div className={childDashboardLessontext}>
                  <Body20Bold
                    style={{
                      color: 'white',
                      fontSize: '22px',
                      fontWeight: '600',
                    }}
                  >
                    {!account.assessment_complete
                      ? 'Assessment'
                      : `Lesson: ${currentLesson && currentLesson.lesson_name}`}
                  </Body20Bold>
                </div>
                {/* ) : (
                  <></>
                )} */}
                <div className={childDashboardAvatarContainer}>
                  <div className={childDashboardAvatar}>
                    {account?.avatarLarge && (
                      <Image
                        src={account.avatarLarge}
                        height="240px"
                        width="100px"
                      />
                    )}
                  </div>
                  <div>
                    <Button
                      type="primary"
                      className={childDashboardButton}
                      onClick={onButtonClick}
                    >
                      {!account.assessment_complete
                        ? 'Click To Begin Assessment'
                        : 'Click To Begin Lesson'}
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </DashboardWrapper>
        </div>
      </LoadingOverlay>
    </ChildLayout>
    // </CacheBuster>
  )
}

export default WithAuth(ChildDashboard)

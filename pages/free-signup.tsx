import { Row, Col, Typography } from 'antd'
import { useAppSelector } from 'store'
import Lottie from 'react-lottie'
import LoadingOverlay from 'react-loading-overlay'

import { Layout, Stripe } from 'components'
import animationData from 'animations/signup.json'
import styles from 'styles/Signup.module.scss'
import { SignupForm, TestimonialSection } from 'sections'
import { ReactElement, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { setPage } from 'store/actions'

const FreeSignup = (): ReactElement => {
  const { auth } = useSelector((root: AppState) => root)
  const { signup, featuresListContainer, featuresList } = styles
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(setPage('Free Signup'))
  }, [])
  return (
    <LoadingOverlay active={auth.loading} spinner text="Signing up...">
      <Stripe>
        <Layout
          title="Free Signup - Learnatric"
          description="Sign up now and help us build our platform."
        >
          <Row className={signup} align="top">
            <Col sm={24} md={24} lg={12}>
              <Typography.Title
                level={2}
                style={{ textAlign: 'center', marginBottom: 0 }}
              >
                Create Parent Account
              </Typography.Title>
              <Typography.Title
                level={4}
                style={{ textAlign: 'center', marginTop: 0 }}
              >
                Fit for your Child&apos;s Needs!
              </Typography.Title>
              <div>
                <Lottie
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData,
                    rendererSettings: {
                      preserveAspectRatio: 'xMidYMid slice',
                    },
                  }}
                />
              </div>
              <div className={featuresListContainer}>
                <ul className={featuresList}>
                  <li>Curated Lessons</li>
                  <li>&quot;We do&quot; activities</li>
                  <li>Curated Problems</li>
                </ul>
              </div>
            </Col>
            <Col sm={24} md={12} lg={12}>
              <Row align="middle" justify="center">
                <Col
                  span={12}
                  style={{
                    display: 'inline-flex',
                    justifyContent: 'center',
                  }}
                >
                  <SignupForm messageEmail={''} />
                </Col>
              </Row>
            </Col>
          </Row>
          <div>
            <TestimonialSection />
          </div>
        </Layout>
      </Stripe>
    </LoadingOverlay>
  )
}

export default FreeSignup

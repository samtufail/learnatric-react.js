import { Row, Col, Typography } from 'antd'
import { Layout, Stripe } from 'components'
import Lottie from 'react-lottie'
import animationData from 'animations/login.json'
import { LoginForm } from 'sections'
import styles from 'styles/Login.module.scss'
import { ReactElement } from 'react'
import LoadingOverlay from 'react-loading-overlay'
import { useSelector } from 'react-redux'

const Login = (): ReactElement => {
  const { auth } = useSelector((root: AppState) => root)
  const { lottieAnimation } = styles
  return (
    <LoadingOverlay
      active={auth.loading}
      spinner
      text="loading"
      className="h-screen"
    >
      <Stripe>
        <Layout
          title="Login - Learnatric"
          description="Login to continue yor personalized learing experience"
        >
          <Row align="middle" style={{ padding: '2rem' }}>
            <Col sm={24} md={24} lg={12} className="md:hidden">
              <div>
                <Lottie
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData,
                    rendererSettings: {
                      preserveAspectRatio: 'xMidYMid slice',
                    },
                  }}
                />
              </div>
            </Col>
            <Col sm={24} md={24} lg={12}>
              <Row align="middle" justify="center">
                <Typography.Title
                  level={2}
                  style={{ textAlign: 'center', marginBottom: 20 }}
                >
                  Login
                </Typography.Title>
              </Row>
              <Row align="middle" justify="center">
                <Col
                  span={12}
                  style={{ display: 'inline-flex', justifyContent: 'center' }}
                >
                  <LoginForm />
                </Col>
              </Row>
            </Col>
            <Col sm={24} md={24} lg={12} className="hidden md:block">
              <div className={lottieAnimation}>
                <Lottie
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData,
                    rendererSettings: {
                      preserveAspectRatio: 'xMidYMid slice',
                    },
                  }}
                />
              </div>
            </Col>
          </Row>
        </Layout>
      </Stripe>
    </LoadingOverlay>
  )
}

export default Login

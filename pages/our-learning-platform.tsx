import { Typography } from 'antd'
import { Layout, Header } from 'components'
import { ReactElement } from 'react'
import { QuoteSection, WorksSection } from 'sections'
import styles from 'styles/our-learning-platform.module.scss'

export const OurLearningPlatform = (): ReactElement => {
  const { ourLearningPlatformContainer, textContainer } = styles
  return (
    <Layout
      title="Our Learning Platform - Learnatric"
      description="There are over a thousand different lessons for children in Preschool through third grade. Currently, Learnatric offers lessons in math. As Learnatric grows, so will its subject areas. These will include reading, phonics, science, and social studies. Each lesson and activity employs a variety of learning strategies. Instruction is tailored, scaffolded, and differentiated so no one falls through the cracks. Learnatric gives the power back to your child. This approach can bring an average student to the 98th percentile of performance. It's like having your own personal tutor at your child's fingertips."
    >
      <Header
        image="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244723/AboutUs/Header/about-us_lvntvx.jpg"
        heading="How does Learnatric Work?"
        description=""
        curriculum={false}
      />
      <div className={ourLearningPlatformContainer}>
        <div className={textContainer}>
          <Typography.Paragraph>
            Learnatric uses cutting-edge AI technology to customize the right
            learning plan for your child. It’s rooted in the latest educational
            design of Threshold Concepts. These concepts need a higher-level of
            thinking.
          </Typography.Paragraph>
          <Typography.Paragraph>
            {' '}
            Learnatric has a tracking program that guides the student through a
            unique sequence of activities. Then, it introduces Threshold Concept
            lessons when students are 100% equipped.
          </Typography.Paragraph>
          <Typography.Paragraph>
            {' '}
            This means your child isn’t introduced to a Threshold Concept until
            he or she is ready for it. The key building blocks of learning
            needed to succeed will have already taken place. Your child will
            thrive and be successful. This elevates their commitment to
            challenging themselves and continuing.
          </Typography.Paragraph>
          <Typography.Paragraph>
            {' '}
            Inevitably, your child will feel confident in all areas of their
            life, not just school.
          </Typography.Paragraph>
          <Typography.Title
            level={2}
            style={{ color: 'rgba(35, 28, 76, 0.75)' }}
          >
            What does Learnatric supplement?
          </Typography.Title>
          <Typography.Paragraph>
            There are over a thousand different lessons for children in
            Preschool through third grade. Currently, Learnatric offers lessons
            in math. As Learnatric grows, so will its subject areas. These will
            include reading, phonics, science, and social studies.
          </Typography.Paragraph>
          <Typography.Paragraph>
            Each lesson and activity employs a variety of learning strategies.
            Instruction is tailored, scaffolded, and differentiated so no one
            falls through the cracks.
          </Typography.Paragraph>
          <Typography.Paragraph>
            Learnatric gives the power back to your child. This approach can
            bring an average student to the 98th percentile of performance. It's
            like having your own personal tutor at your child's fingertips.
          </Typography.Paragraph>
        </div>
        <QuoteSection />

        <WorksSection />
        <div className={textContainer}>
          <Typography.Title
            level={2}
            style={{ color: 'rgba(35, 28, 76, 0.75)' }}
          >
            Learnatric is compatible with Computers, Smartphones, and Tablets!
          </Typography.Title>
          <Typography.Paragraph>
            Learnatric protects the privacy of its users.
          </Typography.Paragraph>
        </div>
      </div>
    </Layout>
  )
}

export default OurLearningPlatform

import { Layout, Header } from 'components'
import { CaseSection, RoboClass, MathCurricula } from 'sections'
import styles from 'styles/our-curriculum.module.scss'
import { ReactElement } from 'react'

const OurCurriculum = (): ReactElement => {
  const { CurriculumContainer } = styles
  return (
    <Layout
      title="Our Curriculum - Learnatric"
      description="A brief overview of curriculum your child will learn: Kindergarten - compare numbers, count to 100. First Grade - Addition and subtraction, telling time, bar graphs, money denominations, word problems. Second Grade - Measure length in feet/inches and meters/centimeters, two-step word problems, multiplication and division. Third Grade - Fractions, multiples of 10, round to 10 and to 100, multiplication and division up to 100, two-step word problems."
    >
      {/* <Header
        image=""
        curriculum={true}
        heading={
          <>Learnatric Gives Your Child The Chance To Grow As A Student</>
        }
        description={''}
      /> */}
      <img
        src="https://learnatric-website-images.s3.amazonaws.com/12.16.2021/Our+Curriculum.png"
        alt=""
        width="100%"
        height="70%"
      />
      <div className={CurriculumContainer}>
        <CaseSection />
        <RoboClass
          heading={'View a Class with Robo:'}
          url={
            'https://player.vimeo.com/video/577336483?dnt=1&amp;app_id=122963&amp;h=a11fa171ae'
          }
        />
        <MathCurricula />
      </div>
    </Layout>
  )
}

export default OurCurriculum

/* eslint-disable no-useless-escape */
import { LeftOutlined } from '@ant-design/icons'
import { Button, Card, Row, Typography, Modal } from 'antd'
import { CustomInput, Layout } from 'components'
import { useFormik } from 'formik'
import Link from 'next/link'
import router, { useRouter } from 'next/router'
import { ReactElement } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { forgetChildPassword, forgetParentPassword } from 'store/actions'
import styles from 'styles/forgotten-password.module.scss'
import * as Yup from 'yup'

// eslint-disable-next-line consistent-return
const validateInput = (username) => {
  // console.log(`Email: ${email}, USername: ${username}`)
  const emailRegex =
    /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
  const userRegex = /^[^\\\/&]*$/
  let isValidEmail = emailRegex.test(username)
  let isValidName = userRegex.test(username)
  if (isValidEmail) {
    console.log('Returned true')
    return true
  } else if (isValidName) {
    return false
  }
}

const validationSchema = Yup.object({
  username: Yup.string().required('Please enter valid username or email'),
})

const ForgotPassword = (): ReactElement => {
  const {
    forgetPasswordContainer,
    forgetCardTitle,
    forgetPasswordButton,
    forgetPasswordButtonContainer,
  } = styles

  const dispatch = useDispatch()
  const onSubmit = (values: { username?: string; email?: string }) => {
    values = {...values,username:values?.username?.toLocaleLowerCase()}
    if (validateInput(values.username)) {
      const finalValues = {
        email: values.username,
      }
      dispatch(forgetParentPassword(finalValues, router))
    } else {
      const finalValues = {
        username: values.username,
      }
      dispatch(forgetChildPassword(finalValues, router))
    }
  }

  const formik = useFormik({
    initialValues: {
      username: '',
    },
    validationSchema,
    onSubmit,
  })

  return (
    <Layout title="Forgot-password - Learnatric" description="">
      <div className={forgetPasswordContainer} style={{ padding: '2rem' }}>
        <Card
          style={{ maxWidth: '500px' }}
          hoverable
          title={
            <Link href="/login">
              <a>
                <Typography.Title level={4} className={forgetCardTitle}>
                  <LeftOutlined /> Find Your Account
                </Typography.Title>
              </a>
            </Link>
          }
        >
          <form onSubmit={formik.handleSubmit}>
            <div>
              <Typography.Title level={5}>
                <span style={{ fontSize: '17px', fontWeight: 500 }}>
                  Please enter your email address or username to search for your
                  account
                </span>
              </Typography.Title>
              <CustomInput
                name="username"
                // value="username"
                formik={formik}
                type="text"
                // placeholder="Enter new email address here."
              />
            </div>
            <div className={forgetPasswordButtonContainer}>
              <Button
                type="primary"
                htmlType="submit"
                className={forgetPasswordButton}
              >
                Send
              </Button>
            </div>
          </form>
        </Card>
      </div>
    </Layout>
  )
}

export default ForgotPassword

import { Layout, Header, Curriculum } from 'components'
import { ReactElement } from 'react'
import { Typography } from 'antd'
import styles from '../styles/complete-curriculum.module.scss'
import {
  kinderCurriculum,
  firstGradeCurriculum,
  secondGradeCurriculum,
  thirdGradeCurriculum,
} from 'lib/completeCurriculum'

const completeCurriculumData = [
  {
    id: 'kinder',
    heading: 'Kindergarten Curriculum',
    curriculumClass: kinderCurriculum,
  },
  {
    id: 'first',
    heading: 'First Grade Curriculum',
    curriculumClass: firstGradeCurriculum,
  },
  {
    id: 'second',
    heading: 'Second Grade Curriculum',
    curriculumClass: secondGradeCurriculum,
  },
  {
    id: 'third',
    heading: 'Third Grade Curriculum',
    curriculumClass: thirdGradeCurriculum,
  },
]

const CurriculumDataContainer = ({
  id,
  heading,
  curriculumClass,
}): ReactElement => {
  const { headers } = styles
  return (
    <section id={id}>
      <div className={headers}>
        <Typography.Title level={2} style={{ color: 'rgba(35, 28, 76, 0.75)' }}>
          {heading}
        </Typography.Title>
      </div>
      {curriculumClass.map((props, indx) => (
        <Curriculum key={indx} {...props} />
      ))}
    </section>
  )
}

const CompleteCurriculum = (): ReactElement => {
  const { mathCurriculaContainer, textContainer } = styles
  return (
    <Layout
      title="Complete Curriculum - Learnatric"
      description="Our complete list of curriculum taught for each grade level"
    >
      <Header
        image="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632248725/CompleteCurriculum/Header/learning-platform_zxtcsf.jpg"
        heading={<>Complete Curriculum</>}
        description={''}
        curriculum={false}
      />
      <div className={mathCurriculaContainer}>
        <div className={textContainer}>
          {completeCurriculumData.map((props) => (
            <CurriculumDataContainer key={props.id} {...props} />
          ))}
        </div>
      </div>
    </Layout>
  )
}

export default CompleteCurriculum

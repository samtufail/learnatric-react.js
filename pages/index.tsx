import { Layout } from 'components'
import styles from 'styles/Home.module.scss'
import {
  BannerSection,
  FeaturesSection,
  LearnatricSection,
  SocialSubscribeSection,
} from 'sections'
import React, { ReactElement, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'

const Home = (): ReactElement => {
  const { homeContainer } = styles
  const router = useRouter()
  // const { parent } = useSelector(({ user }: AppState) => user)
  // const { account } = parent
  // const { step } = account

  useEffect(() => {
    let TYPE = localStorage.getItem('TYPE')
    if (TYPE === 'PARENT') {
      // switch (step) {
      //   case '1':
      //     router.push('/add-children')
      //     break
      //   case '2':
      //     router.push('/create-children-profile')
      //     break
      //   case '3':
      //     router.push('/parent-dashboard')
      //     break
      //   default:
      //     break
      // }
      router.push('parent-dashboard')
    } else if (TYPE === 'CHILD') {
      router.push('child-dashboard')
    }
  }, [])

  return (
    <Layout
      title="Home - Learnatric"
      description="Learnatric is an online learning platform for children ages Pre-K to Kindergarten. Like a tutor, Learnatric responds to students’ progress in real time by recognizing their strengths and providing extra support for their challenge areas. While your child is learning the curriculum, Learnatric is learning your child’s unique learning patterns. By personalizing your child’s curriculum, Learnatric’s innovative AI technology and machine learnig algorithms  actually accelerates the learning process. Ad free"
    >
      <div className={homeContainer}>
        <BannerSection />
        {/* <FeaturesSection /> */}
        <LearnatricSection />
        <SocialSubscribeSection />
      </div>
    </Layout>
  )
}

export default Home

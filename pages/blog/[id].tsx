import { ReactElement } from 'react'
import { useRouter } from 'next/router'
import { blogPost } from 'lib/blogPosts'
import _ from 'lodash'
import styles from 'styles/Post.module.scss'
import { Heading, Layout, PostQouteCard } from 'components'
import { Typography } from 'antd'
import ReactPlayer from 'react-player'

const BlogPost = (): ReactElement => {
  const router = useRouter()

  const { id } = router.query

  const post = _.find(blogPost, (post) => {
    return post.id === id
  })
  const {
    postContainer,
    postContainerWrapper,
    postTitle,
    postImg,
    postText,
    postQoutation,
    postVideo,
  } = styles

  return (
    <>
      {post ? (
        <Layout title={`${post.title} | Learnatric`}>
          <div className={postContainer}>
            <div className={postContainerWrapper}>
              <Heading level={1} className={postTitle}>
                {post.title}
              </Heading>
              <p>
                by <span style={{ fontSize: 20 }}>{post.author}</span>
              </p>
              <div className={postImg}>
                <img src={post.postImage} alt="post" width="300" height="200" />
              </div>
              {post.long ?
                <div className={postText}>
                  {post.content.map(content => {
                    return (
                      <div key={content.id}>
                        <Typography.Title level={3}>
                          {content.heading}
                        </Typography.Title>
                        {content.paragraphs && 
                          content.paragraphs.map(paragraph => {
                            return (
                              <>
                                <Typography.Paragraph key={paragraph.id}>{paragraph.text}</Typography.Paragraph>
                                {paragraph.list && 
                                  paragraph.list.map(item => {
                                    return (
                                      <div style={{marginLeft: '15px', marginBottom: '10px'}}>
                                        <li style={{fontSize: '20px'}}>
                                          <Typography.Text>{item}</Typography.Text>
                                        </li>
                                      </div>
                                    )
                                  })
                                }
                                {paragraph.quote &&
                                  <div className={postQoutation}>
                                    <PostQouteCard qoutationText={paragraph.quote } />
                                  </div>
                                }
                              </>
                            )
                          })
                        }
                      </div>
                    )
                  })}
                </div>
                : 
                <>
                  <div className={postText}>
                    <Typography.Paragraph>{post.text}</Typography.Paragraph>
                  </div>
                  {post.qoute ? (
                    <div className={postQoutation}>
                      <PostQouteCard qoutationText={post.qoute} />
                    </div>
                  ) : (
                    <></>
                  )}
                  {post.videoUrl ? (
                    <div className={postVideo}>
                      <ReactPlayer
                        url={post.videoUrl}
                        width="100%"
                        height="450px"
                        playing={true}
                        controls={true}
                      />
                    </div>
                  ) : (
                    <></>
                  )}
                </>
              }
            </div>
          </div>
        </Layout>
      ) : (
        <></>
      )}
    </>
  )
}

export default BlogPost

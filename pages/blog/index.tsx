import { Layout, Heading } from 'components'
import { ReactElement } from 'react'
import { PostCards } from 'sections/BlogSections'
import styles from 'styles/blog.module.scss'

const Blog = (): ReactElement => {
  const { blogContainer, blogWrapper, blogHeading } = styles
  return (
    <Layout title="Blog - Learnatric"
      description="Learnatrics own blog with discussion topics such as data science, machine learning, and adaptive learning technologies"
    >
      <div className={blogContainer}>
        <div className={blogWrapper}>
          <div>
            <Heading className={blogHeading}>
              Read the best Learnatric posts
            </Heading>
          </div>
          <PostCards />
        </div>
      </div>
    </Layout>
  )
}

export default Blog

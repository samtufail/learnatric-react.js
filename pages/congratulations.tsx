import { Layout } from 'components'
import Lottie from 'react-lottie'
import animationData from 'animations/congratulations.json'
import { Button, Typography } from 'antd'
import Link from 'next/link'
import {
  congratulations,
  congratulationsWrapper,
  congratulationsHeading,
  congratulationsDescription,
  congratulationsButton,
} from 'styles/Congratulations.module.scss'
import { ReactElement, useEffect } from 'react'
import axiosInstance from 'api/axios'

const Congratulations = (): ReactElement => {
  return (
    <Layout title="Congratulations - Learnatric">
      <div className={congratulations}>
        <div className={congratulationsWrapper}>
          <Typography.Title level={3} className={congratulationsHeading}>
            Congratulations!
          </Typography.Title>
          <Typography.Paragraph className={congratulationsDescription}>
            You are now an official member of Learnatric!
          </Typography.Paragraph>
          {/* <Typography.Paragraph className={congratulationsDescription}>
            We just emailed you your receipt!
          </Typography.Paragraph> */}
          <div>
            <Lottie
              options={{
                loop: true,
                autoplay: true,
                animationData,
                rendererSettings: {
                  preserveAspectRatio: 'xMidYMid slice',
                },
              }}
              height={400}
              width={400}
            />
          </div>
          <Link href="/login">
            <a>
              <Button
                className={congratulationsButton}
                type="primary"
                htmlType="submit"
              >
                Login to add your children
              </Button>
            </a>
          </Link>
        </div>
      </div>
    </Layout>
  )
}

export default Congratulations

import { useRouter } from 'next/router'

export const WithAuth = (WrappedComponent) => {
  return (props) => {
    if (typeof window !== 'undefined') {
      const Router = useRouter()

      const TOKEN = localStorage.getItem('TOKEN')
      const TYPE = localStorage.getItem('TYPE')
      let isAuthenticated
      if (TYPE === 'PARENT') {
        const PARENT_ID = localStorage.getItem('PARENT_ID')
        isAuthenticated = TOKEN && PARENT_ID
      } else if (TYPE === 'CHILD') {
        const CHILD_ID = localStorage.getItem('CHILD_ID')
        isAuthenticated = TOKEN && CHILD_ID
      }
      if (!isAuthenticated) {
        Router.replace('/')
        return null
      }

      return <WrappedComponent {...props} />
    }
    return null
  }
}

import { Button } from 'antd'
import {
  CustomInput,
  StepPanel,
  AvatarSelector,
  PickChildToLoginModal,
} from 'components'
import { useFormik } from 'formik'
import { useRouter } from 'next/router'
import { useState, useEffect, ReactElement } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { updateChild } from 'store/actions'
import * as Yup from 'yup'

import {
  createChildrenProfileFormContainer,
  headerStyles,
} from './CreateChildrenProfileForm.module.scss'

const initialValues = {
  username: '',
  password: '',
  avatarSmall: '',
  avatarLarge: '',
}

const validationSchema = Yup.object().shape({
  username: Yup.string()
    .matches(/^(?!\s+$).*/gm, '* First Name cannot contain only blankspaces')
    .required('Username is required!'),
  password: Yup.string().required('Password is required!'),
  avatarSmall: Yup.string().required('Avatar is required!'),
})

export interface AddChildProps {
  next: any
  last: any
  parentId: number
  childId: number
  childArray: any
  setChildArray: any
}

export const AddChildForm = ({
  next,
  last,
  parentId,
  childId,
  setChildArray,
  childArray,
}: AddChildProps) => {
  const dispatch = useDispatch()
  const onSubmit = async (values, { resetForm }) => {
    const finalValues = {
      ...values,
      parentId,
      childId,
    }
    setChildArray([...childArray, finalValues])
    dispatch(updateChild(finalValues, resetForm, next, last))
  }

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  })

  return (
    <form
      onSubmit={formik.handleSubmit}
      className="add-child__form-wrapper add-child-profile__form-wrapper"
    >
      <div className="add-child__form">
        <CustomInput
          formik={formik}
          name="username"
          label="Username (not an email)*"
        />
        <CustomInput
          formik={formik}
          name="password"
          type="password"
          label="Password *"
        />
        <AvatarSelector formik={formik} />
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </div>
    </form>
  )
}

// Main Section Part Starts Here
export const CreateChildrenProfileForm = (): ReactElement => {
  const { account } = useSelector(({ parent }: AppState) => parent)
  const { childs, childrens, noOfChildsCredsCompleted } = account

  const [activeStep, setActiveStep] = useState(noOfChildsCredsCompleted)
  const [childArray, setChildArray] = useState([])

  const [visible, setVisible] = useState(false)

  const router = useRouter()
  const next = () => {
    if (activeStep < childs - 1) {
      const nextStep = activeStep + 1
      setActiveStep(nextStep)
    }
  }

  const last = () => {
    if (activeStep === childs - 1) {
      router.push('/parent-dashboard')
    }
  }

  // useEffect(() => {
  //   if (childArray.length === childs) {
  //   }
  // }, [childArray])

  return (
    <>
      {visible && (
        <PickChildToLoginModal visible={visible} setVisible={setVisible} />
      )}
      <div className={createChildrenProfileFormContainer}>
        <div className={headerStyles}>
          Set up student username and password.
        </div>
        {childrens && childrens.length > 0 && (
          <StepPanel
            steps={childrens.map(({ id, firstName, parentId }) => ({
              title: `Finish ${firstName}'s Profile`,
              content: (
                <AddChildForm
                  key={id}
                  next={next}
                  last={last}
                  parentId={parentId}
                  childId={id}
                  childArray={childArray}
                  setChildArray={setChildArray}
                />
              ),
            }))}
            activeStep={activeStep}
          />
        )}
      </div>
    </>
  )
}

import dynamic from 'next/dynamic'
import { CreateChildrenProfileForm as CreateChildrenProfileFormImport } from './CreateChildrenProfileForm'

export const CreateChildrenProfileForm = dynamic(
  () =>
    import('./CreateChildrenProfileForm').then(
      (module) => module.CreateChildrenProfileForm
    ) as any
) as typeof CreateChildrenProfileFormImport

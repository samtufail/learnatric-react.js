import { ReactElement } from 'react'
import { Typography } from 'antd'
import styles from './MathCurricula.module.scss'
import { Curriculum } from 'components'

const curriculumIntro = [
  {
    heading: 'Kindergarten:',
    courseChapters: [
      'Recognize and compare numbers',
      'Count to 100 by ones, fives, and tens',
      'Understand relationships between numbers and quantities',
      'Recognize mathematical symbols, shapes, and measurements',
      'Introduction to addition and subtraction',
    ],
    courseLink: '/complete-curriculum#kinder',
    courseLinkText: 'Link to Complete Kindergarten Curriculum',
  },
  {
    heading: 'First Grade:',
    courseChapters: [
      'Addition & subtraction within 100',
      'Telling time',
      'Area, bar graphs, and picture graphs',
      'Money denominations',
      'Introduction to word problems',
    ],
    courseLink: '/complete-curriculum#first',
    courseLinkText: 'Link to Complete First Grade Curriculum',
  },
  {
    heading: 'Second Grade:',
    courseChapters: [
      'Addition & subtraction within 1,000',
      'Estimate & measure length in feet/inches and meters/centimeters',
      'Bar graphs and picture graphs with four categories',
      'Skip counting by 5 and 10',
      'Two-step word problems',
      'Introduction to multiplication and division',
      'Introduction to fractions',
    ],
    courseLink: '/complete-curriculum#second',
    courseLinkText: 'Link to Complete Second Grade Curriculum',
  },
  {
    heading: 'Third Grade:',
    courseChapters: [
      'Identify and compare fractions, generate equivalent fractions',
      'Strategies for addition and subtraction within 1,000',
      'Multiply one-digit whole numbers by multiples of 10',
      'Round to 10 and to 100',
      'Multiplication and division up to 100',
      'Two-step word problems requiring multiplication and division',
      'Time and money word problems',
    ],
    courseLink: '/complete-curriculum#third',
    courseLinkText: 'Link to Complete Third Grade Curriculum',
  },
]

export const MathCurricula = (): ReactElement => {
  const {
    mathCurriculaContainer,
    mathCurriculaContainerInner,
    textContainer,
    textContainerheading,
    textContainerContent,
  } = styles
  return (
    <div className={mathCurriculaContainer}>
      <div className={mathCurriculaContainerInner}>
        <div className={textContainer}>
          <Typography.Title className={textContainerheading}>
            Math Curricula Your Child will Learn
          </Typography.Title>
          <div className={textContainerContent}>
            {curriculumIntro.map((props) => (
              <Curriculum key={props.heading} {...props} />
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

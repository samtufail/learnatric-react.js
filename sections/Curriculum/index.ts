import dynamic from 'next/dynamic'
import { RoboClass as RoboClassImport } from './RoboClass.section'
import { CaseSection as CaseSectionImport } from './Case.section'
import { MathCurricula as MathCurriculaImport } from './MathCurricula.section'

export const RoboClass = dynamic(
  () => import('./RoboClass.section').then((module) => module.RoboClass) as any
) as typeof RoboClassImport
export const CaseSection = dynamic(
  () => import('./Case.section').then((module) => module.CaseSection) as any
) as typeof CaseSectionImport
export const MathCurricula = dynamic(
  () =>
    import('./MathCurricula.section').then(
      (module) => module.MathCurricula
    ) as any
) as typeof MathCurriculaImport

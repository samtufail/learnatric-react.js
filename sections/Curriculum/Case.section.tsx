import { Typography } from 'antd'
import { ReactElement } from 'react'
import styles from './Case.module.scss'

export const CaseSection = (): ReactElement => {
  const {
    caseContainer,
    caseContainerHeading,
    caseContainerBox,
    caseContainerBoxInner,
    caseImage,
    caseText,
  } = styles
  return (
    <div className={caseContainer}>
      <div className={caseContainerBox}>
        <div className={caseContainerBoxInner}>
          <img
            className={caseImage}
            src="https://learnatric-website-images.s3.amazonaws.com/Website+Images+6.1+Michael+and+Olivia+(2600+x+1200+px).svg"
            alt="child case"
            width="100%"
            height="200"
          />
          {/* <div className={caseText}>
            <Typography.Title className={caseContainerHeading} level={4}>
              The Case of Michael and Sarah
            </Typography.Title>
            <Typography.Paragraph className={caseText}>
              Meet Michael and Sarah. They’re twins, and they’ve both failed
              their math test. But for different reasons.
            </Typography.Paragraph>
            <Typography.Paragraph className={caseText}>
              Michael failed because he struggles with word problems. He’s
              behind in his reading comprehension.
            </Typography.Paragraph>
            <Typography.Paragraph className={caseText}>
              Sarah failed because she isn’t strong in her math facts.
            </Typography.Paragraph>
            <Typography.Paragraph className={caseText}>
              Michael and Sarah need fundamentally different kinds of
              instruction to meet their individual needs. And yet, the
              likelihood of their classroom teacher continuing to teach to the
              middle is high. Michael and Sarah need an attentive teacher who
              recognizes their unique learning obstacles and designs a learning
              sequence specifically for them.
            </Typography.Paragraph>
            <Typography.Paragraph className={caseText}>
              That’s where Learnatric comes in.
            </Typography.Paragraph>
            <Typography.Paragraph className={caseText}>
              Learnatric uses over 100 mini-assessments that continually tracks
              how students are doing. Then, based on the data, we choose the
              best lesson to deliver, just like a personal tutor would.
            </Typography.Paragraph>
          </div> */}
        </div>
      </div>
    </div>
  )
}

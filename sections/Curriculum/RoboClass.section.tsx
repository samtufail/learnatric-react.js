import { ReactElement } from 'react'
import { Typography } from 'antd'
import styles from './RoboClass.module.scss'
import ReactPlayer from 'react-player'

export const RoboClass = ({ url, heading }): ReactElement => {
  const { roboBox, roboBoxInner, headingRobo, videoRoboContainer, videoRobo } =
    styles
  return (
    <div className={roboBox}>
      <div className={roboBoxInner}>
        <Typography.Title level={1} className={headingRobo}>
          {heading}
        </Typography.Title>
        <div className={videoRoboContainer}>
          <ReactPlayer
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              width: '100%',
              height: '100%',
            }}
            url={url}
            width="100%"
            height="100%"
            playing={false}
            controls={true}
          />
          {/* <iframe
            loading="lazy"
            title="B1 L1-1 I Do Tag9.1"
            src={url}
            allow="fullscreen; picture-in-picture"
            allowFullScreen
            frameBorder="0"
            className={videoRobo}
          /> */}
        </div>
      </div>
    </div>
  )
}

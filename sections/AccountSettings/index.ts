import dynamic from 'next/dynamic'
import { BillingForm as BillingFormImport } from './Billing/BillingForm.section'
import { AdjustChildren as AdjustChildrenImport } from './Billing/AdjustChildren.section'
import { CancelSub as CancelSubImport} from './Billing/CancelSub.section'

export const CancelSub = dynamic(
  () => 
    import('./Billing/CancelSub.section').then(
      (module) => module.CancelSub
    ) as any
) as typeof CancelSubImport

export const BillingForm = dynamic(
  () =>
    import('./Billing/BillingForm.section').then(
      (module) => module.BillingForm
    ) as any
) as typeof BillingFormImport

export const AdjustChildren = dynamic(
  () =>
    import('./Billing/AdjustChildren.section').then(
      (module) => module.AdjustChildren
    ) as any
) as typeof AdjustChildrenImport
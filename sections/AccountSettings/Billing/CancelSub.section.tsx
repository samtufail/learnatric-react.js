import { Button, Row, Col, Typography, Card, Avatar } from 'antd'
import { toast } from 'react-toastify'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  AUTH_ACTION_TYPES,
  PARENT_ACTION_TYPES,
} from '../../../store/actionTypes'
import axiosInstance from '../../../api/axios'
import styles from './CancelSub.module.scss'

export const CancelSub = (): ReactElement => {
  const { CancelButton } = styles
  const { account, billingDetails } = useSelector(
    ({ parent }: AppState) => parent
  )
  const { stripeSubsID, id } = account
  const dispatch = useDispatch()

  const handleCancel = async () => {
    const res = await axiosInstance.put('/parent/cancel-subscription', {
      id,
      stripeSubsID,
    })
    if (res.data.code === 200) {
      const { status } = res.data.result
      let actStatus =
        status === 'trialing'
          ? 'Trial'
          : res.data.result.status.charAt(0).toUpperCase() +
            res.data.result.status.slice(1)
      actStatus = actStatus === 'Trialing' ? 'Trial' : actStatus
      const details = {
        ...res.data.result,
        actStatus: actStatus,
      }
      dispatch({
        type: PARENT_ACTION_TYPES.SAVE_BILLING_DETAILS,
        payload: details,
      })
      toast.success(res.data.message)
    } else {
      toast.error(res.data.message)
    }
  }

  const handleResume = async () => {
    const res = await axiosInstance.put('/parent/resume-subscription', {
      id,
      stripeSubsID,
    })
    if (res.data.code === 200) {
      const { status } = res.data.result
      let actStatus =
        status === 'trialing'
          ? 'Trial'
          : res.data.result.status.charAt(0).toUpperCase() +
            res.data.result.status.slice(1)
      actStatus = actStatus === 'Trialing' ? 'Trial' : actStatus
      const details = {
        ...res.data.result,
        actStatus: actStatus,
      }
      dispatch({
        type: PARENT_ACTION_TYPES.SAVE_BILLING_DETAILS,
        payload: details,
      })
      toast.success(res.data.message)
    } else {
      toast.error(res.data.message)
    }
  }
  return (
    <>
      {billingDetails.cancel_at_period_end ? (
        <Button type="primary" onClick={() => handleResume()}>
          Resume
        </Button>
      ) : (
        <Button
          className={CancelButton}
          type="primary"
          danger
          onClick={() => handleCancel()}
        >
          Cancel
        </Button>
      )}
    </>
  )
}

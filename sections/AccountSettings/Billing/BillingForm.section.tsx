import { Button, Row, Col, Typography, Card, Avatar } from 'antd'
import { toast } from 'react-toastify'
import { Layout, Paragraph, Stripe, CardInput, CustomInput } from 'components'
import { useStripe, useElements, CardElement } from '@stripe/react-stripe-js'
import { ReactElement, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import styles from './BillingForm.module.scss'
import { AUTH_ACTION_TYPES } from '../../../store/actionTypes'
import axiosInstance from '../../../api/axios'
import { update } from 'lodash'

const initialValues = {
  firstName: '',
  lastName: '',
  email: '',
}

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required('First Name is required!'),
  lastName: Yup.string().required('Last Name is required!'),
  email: Yup.string().email('Email is invalid!').required('Email is required!'),
})

export const BillingForm = (): ReactElement => {
  const { account } = useSelector(({ parent }: AppState) => parent)
  const dispatch = useDispatch()

  const stripe = useStripe()
  const elements = useElements()

  const clearCard = () => {
    const cardElement = elements.getElement(CardElement)
    // ensure the Element is still mounted
    if (cardElement) {
      cardElement.clear()
    }
  }

  const onSubmit = async (values) => {
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: elements.getElement(CardElement),
      billing_details: {
        email: values.email,
        name: `${values.firstName} ${values.lastName}`,
      },
    })
    if (!error) {
      const data = {
        paymentMethod,
        values,
        parentId: account.id,
        custId: account.stripeId,
        subId: account.stripeSubsID,
      }
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START })
      const updateCard = await axiosInstance.put('/parent/update-card', data)
      toast.success(updateCard.data.message)
      formik.resetForm()
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
    } else {
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
      toast.error(error.message)
    }
    clearCard()
  }

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  })

  const {
    cardInputContainer,
    cardLabel,
    cardInputHolder,
    billingFormButtonContainer,
    billingFormButton,
  } = styles
  return (
    <form onSubmit={formik.handleSubmit}>
      <CustomInput formik={formik} label="First Name" name="firstName" />
      <CustomInput formik={formik} label="Last Name" name="lastName" />
      <CustomInput formik={formik} label="Email" name="email" isEmail />
      <div className={cardInputContainer}>
        <Typography.Title level={5} className={cardLabel}>
          Enter new card information
        </Typography.Title>
        <div className={cardInputHolder}>
          <CardInput />
        </div>
      </div>
      <div className={billingFormButtonContainer}>
        <Button className={billingFormButton} type="primary" htmlType="submit">
          Update
        </Button>
      </div>
    </form>
  )
}

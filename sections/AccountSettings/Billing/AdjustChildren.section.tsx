import { Button, Row, Col, Typography, Card, Avatar, Modal } from 'antd'
import { toast } from 'react-toastify'
import {
  Layout,
  Paragraph,
  Stripe,
  CardInput,
  CustomInputNumber,
} from 'components'
import { ReactElement, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import axiosInstance from '../../../api/axios'
import { PARENT_ACTION_TYPES } from '../../../store/actionTypes'
import router from 'next/router'
import styles from './AdjustChildren.module.scss'

export const AdjustChildren = (): ReactElement => {
  const {
    addRemoveChildButton,
    adjustChildrenButtonContainer,
    adjustChildrenformContainer,
  } = styles
  const { account } = useSelector(({ parent }: AppState) => parent)
  const dispatch = useDispatch()
  const initialValues = {
    childs: account.childs,
  }
  const validationSchema = Yup.object().shape({
    // @ts-ignore
    childs: Yup.number('It should be a number')
      .integer()
      .required('Number of children is required!'),
  })

  const onSubmit = async (vals) => {
    //this func only gets called if removing children
    const removeChild = async (id: number) => {
      const data = {
        id: account.id,
        childs: vals.childs,
        subId: account.stripeSubsID,
        noOfChildsCredsCompleted: account.noOfChildsCredsCompleted,
        noOfChildsProfileCompleted: account.noOfChildsProfileCompleted,
        childId: id,
        isLessChildren: true,
      }
      const update = await axiosInstance.put(
        '/parent/billing-children-update',
        data
      )
      const { status } = update
      const { result } = update.data
      if (status === 200) {
        toast.success(update.data.message)
        dispatch({
          type: PARENT_ACTION_TYPES.SAVE_PARENT_ACCOUNT_DETAILS,
          payload: result,
        })
      } else {
        toast.error(update.data.message)
      }
    }

    if (vals.childs > account.childs) {
      axiosInstance
        .put('/parent/billing-children-update', {
          id: account.id,
          childs: vals.childs,
          subId: account.stripeSubsID,
          isLessChildren: false,
        })
        .then((response) => {
          const { status } = response
          const { result } = response.data
          if (status === 200) {
            toast.success(response.data.message)
            dispatch({
              type: PARENT_ACTION_TYPES.SAVE_PARENT_ACCOUNT_DETAILS,
              payload: result,
            })
            router.push('/add-children')
          } else {
            toast.error(response.data.message)
          }
        })
    } else {
      const childrenData = await axiosInstance.get(
        `/parent/child-names?id=${account.id}`
      )
      const { result } = childrenData.data
      Modal.info({
        title: 'Pick Child to Remove',
        // className: 'modal__container',
        okText: 'Cancel',
        content: result.map((child: any, idx: number): ReactElement => {
          if (child.is_active_subscription) {
            return (
              <div
                key={child.id}
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  marginBottom: '8px',
                  cursor: 'pointer',
                }}
                onClick={() => {
                  removeChild(child.id)
                  Modal.destroyAll()
                }}
              >
                <Avatar src={child.avatarSmall} size={45} />
                <Typography.Title level={5}>{child.firstName}</Typography.Title>
              </div>
            )
          }
          return null
        }),
      })
    }
  }

  const childCountChange = (e: any) => {
    formik.setFieldValue('childs', e)
  }
  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  })
  return (
    <form onSubmit={formik.handleSubmit}>
      <CustomInputNumber
        formik={formik}
        customOnChange={childCountChange}
        min={1}
        max={10}
        defaultValue={account.childs}
        name="childs"
        label="Choose number of children"
      />
      <div className={adjustChildrenButtonContainer}>
        <Button
          className={addRemoveChildButton}
          type="primary"
          htmlType="submit"
        >
          Save
        </Button>
      </div>
    </form>
  )
}

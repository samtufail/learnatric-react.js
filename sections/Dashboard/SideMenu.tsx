import React, { ReactElement } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Menu, Layout } from 'antd'
import { UserSwitchOutlined, VideoCameraOutlined } from '@ant-design/icons'
import Image from 'next/image'
import { Body20Bold } from 'components'
import { useRouter } from 'next/router'
import classes from 'styles/side-menu.module.scss'
import styles from './SideMenu.module.scss'
import {
  switchToParent,
  startInitialVideo,
  changeVideoDisplay,
} from '../../store/actions'

const { Sider } = Layout

export const SideMenu = (): ReactElement => {
  const { account, playInitialVideo } = useSelector(
    ({ child }: AppState) => child
  )
  const Router = useRouter()
  const dispatch = useDispatch()
  const { dashboardSider } = styles

  const handleParentClick = () => {
    if (!playInitialVideo) {
      dispatch(switchToParent(account.id))
    }
  }
  const handleVideoClick = () => {
    dispatch(startInitialVideo(true))
    dispatch(changeVideoDisplay('flex'))
  }
  const handleChildClick = () => {
    if (!playInitialVideo) {
      Router.push('/child-dashboard')
    }
  }
  const { sideMenuText } = styles

  return (
    <Sider className={dashboardSider} width={120}>
      <Menu
        mode="vertical"
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        style={{ height: '100%', padding: 10 }}
      >
        <div
          onClick={() => handleChildClick()}
          style={{ marginBottom: '15px' }}
        >
          <div className={classes.avatar}>
            {account.avatarLarge && (
              <Image
                src={account.avatarLarge}
                alt="Learning Path"
                height="100"
                width="100%"
                objectFit="contain"
                layout="responsive"
              />
            )}
          </div>
          <Body20Bold className={sideMenuText}>{account.firstName}</Body20Bold>
        </div>
        <div
          onClick={() => handleParentClick()}
          style={{ marginBottom: '15px' }}
        >
          <div
            className={classes.avatar}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <UserSwitchOutlined style={{ fontSize: '55px' }} />
          </div>
          <Body20Bold className={sideMenuText}>Parent Dashboard</Body20Bold>
        </div>
        <div
          onClick={() => handleVideoClick()}
          // style={{marginBottom: '25px'}}
        >
          <div
            className={classes.avatar}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <VideoCameraOutlined style={{ fontSize: '55px' }} />
          </div>
          <Body20Bold className={sideMenuText}>Welcome Video</Body20Bold>
        </div>
      </Menu>
    </Sider>
  )
}

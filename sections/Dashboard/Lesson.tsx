import React, { useEffect, useState, ReactElement } from 'react'
import Router from 'next/router'
import { useSelector } from 'react-redux'
import { Col } from 'antd'

import classes from 'styles/child-dashboard.module.scss'

export interface LessonProps {
  containerStyles: any
  currentLesson?: boolean
  lessonTitle?: string
}

export const Lesson = ({
  containerStyles,
  currentLesson,
  lessonTitle,
}: LessonProps): ReactElement => {
  const { account } = useSelector(({ child }: AppState) => child)
  const [openLesson, setOpenLesson] = useState(false)
  const [onImage, setOnImage] = useState(false)
  const [onLessonDetail, setOnLessonDetail] = useState(false)

  useEffect(() => {
    if (!onImage && !onLessonDetail) {
      setOpenLesson(false)
    } else {
      setOpenLesson(true)
    }
  }, [onImage, onLessonDetail])

  const onLessonClicked = () => {
    Router.push('/lesson-activity')
  }
  const onNextClicked = () => {
    Router.push('/lesson-activity')
  }

  return (
    <div className={classes.lesson}>
      <>
        <img
          onMouseOverCapture={() => {
            setOnImage(true)
          }}
          onMouseLeave={() => {
            setOnImage(false)
          }}
          className={classes.studentAvatar}
          alt={account.firstName}
          src={account.avatarLarge}
          style={{
            height: 150,
            position: 'absolute',
            zIndex: 100,
          }}
        />
        <div onClick={onNextClicked}>next lesson</div>
        {openLesson && (
          <div
            onClick={onLessonClicked}
            onMouseLeave={() => {
              setOnLessonDetail(false)
            }}
            onMouseOverCapture={() => {
              setOnLessonDetail(true)
            }}
            className={classes.lessonDescription}
          >
            <img
              alt="lesson preview"
              src="/img/learningPathBackground.jpg"
              style={{
                height: 60,
                transform: 'rotate(-20deg)',
                borderRadius: 12,
                border: '1px solid rgba(58, 162, 37, 255)',
              }}
            />
            <Col>
              <span>{lessonTitle}</span>
            </Col>
          </div>
        )}
      </>
    </div>
  )
}

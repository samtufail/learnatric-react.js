import dynamic from 'next/dynamic'
import { SideMenu as SideMenuImport } from './SideMenu'
import { DashboardWrapper as DashboardWrapperImport } from './DashboardWrapper'
import { Lesson as LessonImport } from './Lesson'

export const SideMenu = dynamic(
  () => import('./SideMenu').then((module) => module.SideMenu) as any
) as typeof SideMenuImport

export const DashboardWrapper = dynamic(
  () =>
    import('./DashboardWrapper').then(
      (module) => module.DashboardWrapper
    ) as any
) as typeof DashboardWrapperImport

export const Lesson = dynamic(
  () => import('./Lesson').then((module) => module.Lesson) as any
) as typeof LessonImport

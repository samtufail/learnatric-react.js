import { useSelector } from 'react-redux'
import { Layout } from 'antd'
import Image from 'next/image'

import classes from 'styles/side-menu.module.scss'
import { useRouter } from 'next/router'

const { Header } = Layout

export default function MobileMenu() {
  const { account } = useSelector(({ child }: AppState) => child)
  const Router = useRouter()

  return (
    <Header className="md:hidden bg-secondary px-2 py-1 overflow-x-scroll">
      <div
        className={classes.avatar}
        onClick={() => Router.push('child-dashboard')}
      >
        {account.avatarLarge && (
          <Image
            src={account.avatarLarge}
            alt="Learning Path"
            height="20px"
            width="20px"
            objectFit="contain"
            layout="responsive"
          />
        )}
      </div>
    </Header>
  )
}

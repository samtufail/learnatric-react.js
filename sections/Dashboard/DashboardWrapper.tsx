import React, { ReactElement } from 'react'
import { Layout } from 'antd'
import { SideMenu } from 'sections/Dashboard'

const { Content } = Layout

export interface DashboardWrapperProps {
  children: ReactElement | ReactElement[]
}

export const DashboardWrapper = ({
  children,
}: DashboardWrapperProps): ReactElement => {
  return (
    <Content>
      <Layout
        className="site-layout-background"
        style={{ minHeight: 'calc(100vh - 94px)' }}
      >
        {/* <SideMenu /> */}
        <Content>{children}</Content>
      </Layout>
    </Content>
  )
}

import { Button, Tag } from 'antd'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'
import { useAppSelector } from 'store'
import { CustomInput } from 'components'
import { CloseCircleOutlined } from '@ant-design/icons'

import { userLogin } from 'store/actions'
import styles from './LoginForm.module.scss'
import { ReactElement } from 'react'
import Link from 'next/link'

const initialValues = {
  username: '',
  password: '',
}

const validationSchema = Yup.object().shape({
  username: Yup.string().required('Username is required!'),
  password: Yup.string().required('Password is required!'),
})

export const LoginForm = (): ReactElement => {
  const { loginForm, loginFormButton, errorText, frogetPassword } = styles
  const dispatch = useDispatch()
  // const { loginError, errorMessage } = useAppSelector(({ user }) => user)

  const onSubmit = async ({ username, password }) => {
    // Check if email
    if (username.includes("@")) {
       //its email address : convert it to lowercase
       username=username?.toLowerCase()
    }

    const data = { username, password }
    console.log("data",data)
    dispatch(userLogin(data))
  }

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  })

  return (
    <div className={loginForm}>
      <form onSubmit={formik.handleSubmit}>
        <CustomInput
          formik={formik}
          label="Username or Email"
          name="username"
        />
        <CustomInput
          formik={formik}
          label="Password"
          name="password"
          type="password"
        />
        {/* {loginError && (
          <Tag
            icon={<CloseCircleOutlined />}
            className={errorText}
            color="error"
          >
            {errorMessage}
          </Tag>
        )} */}
        <Link href="/forgotten-password">
          <a className={frogetPassword}>Forgot password?</a>
        </Link>
        <Button className={loginFormButton} type="primary" htmlType="submit">
          Login
        </Button>
      </form>
    </div>
  )
}

import dynamic from 'next/dynamic'
import { LoginForm as LoginFormImport } from './LoginForm.section'

export const LoginForm = dynamic(
  () => import('./LoginForm.section').then((module) => module.LoginForm) as any
) as typeof LoginFormImport

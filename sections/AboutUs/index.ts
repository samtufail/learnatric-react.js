import dynamic from 'next/dynamic'
import { DescriptionSection as DescriptionSectionImport } from './Description.section'
import { TeamSection as TeamSectionImport } from './Team.section'

export const DescriptionSection = dynamic(
  () =>
    import('./Description.section').then(
      (module) => module.DescriptionSection
    ) as any
) as typeof DescriptionSectionImport
export const TeamSection = dynamic(
  () => import('./Team.section').then((module) => module.TeamSection) as any
) as typeof TeamSectionImport

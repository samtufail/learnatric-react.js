import { Typography } from 'antd'
import { ReactElement } from 'react'
import styles from './Description.module.scss'

export const DescriptionSection = (): ReactElement => {
  const { descContainer } = styles
  return (
    <div className={descContainer}>
      <Typography.Paragraph
        style={{
          fontSize: '18px',
          lineHeight: '31px',
          color: 'rgba(35, 28, 76, 0.75)',
        }}
      >
        The inspiration for Learnatric originated from David Shpigler, a father
        of seven children. David worked with each of his children on math and
        reading, trying to prepare them for school as best he could. With seven
        children, he developed a methodology that seemed to work. He found,
        however, that his approach didn’t work quite as well with his youngest
        daughter. For some reason, the methodology didn’t seem to “click” with
        her as it did with the others. After trying to understand why the
        approach worked for his other children and not for her, he realized that
        he was trying to force fit her into a pre-existing educational approach
        rather than finding the one that best fit her needs. As he conducted
        research into this issue, David began to realize that there any many
        students that suffer in the same way. Students benefit when they receive
        personalized education. The concept of dynamic education was born.
      </Typography.Paragraph>
    </div>
  )
}

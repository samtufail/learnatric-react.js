import { Typography } from 'antd'
import { TeamCard } from 'components'
import { ReactElement } from 'react'
import { teamContainer, teamHeading, teamMembers } from './Team.module.scss'
import { team } from 'components/Constants/team'

export const TeamSection = (): ReactElement => {
  return (
    <div className={teamContainer}>
      <Typography.Title level={1} className={teamHeading}>
        Meet The Team
      </Typography.Title>
      <div className={teamMembers}>
        {team.map((member) => {
          return <TeamCard key={member.name} {...member} />
        })}
      </div>
    </div>
  )
}

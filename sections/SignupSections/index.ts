import dynamic from 'next/dynamic'
import { SignupForm as SignupFormImport } from './SignupForm.section'
import { TestimonialSection as TestimonialSectionImport } from './Testimonial.section'

export const SignupForm = dynamic(
  () =>
    import('./SignupForm.section').then((module) => module.SignupForm) as any
) as typeof SignupFormImport

export const TestimonialSection = dynamic(
  () =>
    import('./Testimonial.section').then(
      (module) => module.TestimonialSection
    ) as any
) as typeof TestimonialSectionImport

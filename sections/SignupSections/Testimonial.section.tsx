import { TestimonialCard } from 'components'
import { ReactElement } from 'react'
import styles from './Testimonial.module.scss'

import { testimonial2 } from 'components/Constants/testimonials'

export const TestimonialSection = (): ReactElement => {
  const { testimonialContainer } = styles
  return (
    <div className={testimonialContainer}>
      {testimonial2.map((testimonial) => (
        <TestimonialCard key={testimonial.name} {...testimonial} />
      ))}
    </div>
  )
}

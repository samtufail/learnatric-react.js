/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/jsx-no-comment-textnodes */
import { useRouter } from 'next/router'
import { Button, Radio, Typography, Statistic } from 'antd'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import {
  CardInput,
  CustomInput,
  CustomInputNumber,
  CustomPhoneSelect,
} from 'components'
import { useStripe, useElements, CardElement } from '@stripe/react-stripe-js'
import { ReactElement, useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import { parentSignup } from 'store/actions'
import styles from './SignupForm.module.scss'
import { AUTH_ACTION_TYPES } from 'store/actionTypes'

const options = [
  { value: 'MONTHLY', title: 'Monthly' },
  { value: 'YEARLY', title: 'Yearly Save 17%' },
]

const initialValues = {
  firstName: '',
  lastName: '',
  email: '',
  phoneNumber: '',
  password: '',
  hearedFrom: '',
  plan_selected: 'MONTHLY',
  childs: 1,
  price: 29,
}
// const phoneRegExp =
//   /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .matches(/^(?!\s+$).*/gm, '* First Name cannot contain only blankspaces')
    .required('First Name is required!'),
  lastName: Yup.string()
    .matches(/^(?!\s+$).*/gm, '* Last Name cannot contain only blankspaces')
    .required('Last Name is required!'),
  email: Yup.string().email('Email is invalid!').required('Email is required!'),
  // phoneNumber: Yup.string()
  //   .matches(phoneRegExp, 'Phone number is not valid')
  //   .required('Phone number is required!'),
  password: Yup.string().required('Password is required!'),
  hearedFrom: Yup.string(),
  plan_selected: Yup.string().required('Please choose a plan'),
  // @ts-ignore
  childs: Yup.number('It should be a number')
    .integer()
    .required('Number of children is required!'),
})

export interface SignupFormProps {
  messageEmail: string
}

export const SignupForm = ({ messageEmail }: SignupFormProps): ReactElement => {
  const { auth } = useSelector((root: AppState) => root)
  const [statisticsTitle, setStatisticsTitle] = useState(
    'Price Per Month (Each additional child is $24.00)'
  )

  const stripe = useStripe()
  const elements = useElements()

  const router = useRouter()
  const dispatch = useDispatch()

  const onSubmit = async (values) => {
    values = {...values,email:values?.email?.toLowerCase()}
    try {
      if (auth.page !== 'Free Signup') {
        const { error, paymentMethod } = await stripe.createPaymentMethod({
          type: 'card',
          card: elements.getElement(CardElement),
          billing_details: {
            email: values.email,
            name: `${values.firstName} ${values.lastName}`,
          },
        })
        const { plan_selected } = values
        if (!error) {
          const data = {
            paymentMethod,
            values,
            plan_selected,
            is_free_signup: false,
          }
          dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START })
          dispatch(parentSignup(data))
        } else {
          dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
          toast.error(error.message)
        }
      } else {
        const data = {
          values,
          plan_selected: '3Free',
          paymentMethod: null,
          is_free_signup: true,
        }
        dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START })
        dispatch(parentSignup(data))
      }
    } catch (error) {
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
      toast.error(error.message)
      // console.log('Error occured while submitting the form', error)
    }
  }

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  })

  const setPrice = ({ plan_selected, childs }) => {
    if (plan_selected === 'YEARLY') {
      const childYearCount = childs - 1
      const newYearPrice = childYearCount * 240 + 290
      setStatisticsTitle('Price Per Year (Each additional child is $240.00)')
      formik.setFieldValue('price', newYearPrice)
    } else {
      const childMonthCount = childs - 1
      const newMonthPrice = childMonthCount * 24 + 29
      setStatisticsTitle('Price Per Month (Each additional child is $24.00)')
      formik.setFieldValue('price', newMonthPrice)
    }
  }

  const childCountChange = (e) => {
    setPrice({ plan_selected: formik.values.plan_selected, childs: e })
  }

  const planChange = (e) => {
    formik.setFieldTouched('plan_selected')
    formik.setFieldValue('plan_selected', e.target.value)
    const {
      values: { childs },
    } = formik
    setPrice({ plan_selected: e.target.value, childs })
  }

  const {
    signupForm,
    signupFormButton,
    radioButtonsContainer,
    radioButtonLabel,
    customRadioButton,
    cardInputContainer,
    cardLabel,
    cardInputHolder,
  } = styles

  const isEmailMessage = () => {
    return messageEmail && messageEmail != '' && messageEmail != undefined
  }
  return (
    <div className={signupForm}>
      {auth.page !== 'Free Signup' ? (
        <>
          <Typography.Title level={3}>
            Try your first month free!
          </Typography.Title>
          <Typography.Paragraph>
            Learnatric offers monthly and yearly subscriptions following a
            30-day free trial. Sign up today!
          </Typography.Paragraph>
        </>
      ) : (
        <>
          <Typography.Title level={3}>
            Try your first 3 months free!
          </Typography.Title>
          <Typography.Paragraph>
            Be among the first Learnatric members and help build our platform
            with your feedback!
          </Typography.Paragraph>
        </>
      )}
      <form onSubmit={formik.handleSubmit}>
        <CustomInput formik={formik} label="First Name" name="firstName" />
        <CustomInput formik={formik} label="Last Name" name="lastName" />
        <CustomInput formik={formik} label="Email" name="email" isEmail />
        {isEmailMessage() && <p style={{ color: 'red' }}>{messageEmail}</p>}
        <CustomPhoneSelect formik={formik} label="Phone" name="phoneNumber" />

        <CustomInput
          formik={formik}
          label="Password"
          name="password"
          type="password"
        />
        <CustomInput
          formik={formik}
          label="How did you first hear about us?"
          name="hearedFrom"
        />
        <CustomInputNumber
          formik={formik}
          customOnChange={childCountChange}
          min={1}
          max={10}
          defaultValue={1}
          name="childs"
          label="Choose number of children"
        />
        {auth.page !== 'Free Signup' ? (
          <>
            <div className={radioButtonsContainer}>
              <Typography.Title level={5} className={radioButtonLabel}>
                Choose Plan
              </Typography.Title>
              <div className="signup__radio">
                <Radio.Group
                  value={formik.values.plan_selected}
                  onChange={(e) => planChange(e)}
                  buttonStyle="solid"
                >
                  {options.map(({ value, title }) => (
                    <Radio.Button
                      className={customRadioButton}
                      key={value}
                      value={value}
                    >
                      {title}
                    </Radio.Button>
                  ))}
                </Radio.Group>
              </div>
            </div>
            <div className="signup__statistics">
              <Statistic
                title={statisticsTitle}
                prefix={<>$</>}
                value={formik.values.price}
                precision={2}
              />
            </div>
            <div className={cardInputContainer}>
              {/* <div>Price: {parentValues.price} per {planSelected === 'Monthly' ? 'Month' : 'Year'}</div> */}
              <Typography.Title level={5} className={cardLabel}>
                Enter payment information
              </Typography.Title>
              <div className={cardInputHolder}>
                <CardInput />
              </div>
            </div>
            <Typography.Paragraph
              style={{ fontSize: '10px', marginTop: '15px' }}
            >
              By clicking Join, you acknowledge that you have read and agree to
              our Terms of Service. You also agree to allow Learnatric to store
              your card information for the purpose of automatically renewing
              your membership and processing any future account upgrades.
            </Typography.Paragraph>
          </>
        ) : (
          <div></div>
        )}
        <Button className={signupFormButton} type="primary" htmlType="submit">
          Signup
        </Button>
      </form>
    </div>
  )
}

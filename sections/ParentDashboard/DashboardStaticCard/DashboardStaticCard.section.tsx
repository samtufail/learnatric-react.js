import { ReactElement } from 'react'
import { Card } from 'antd'
import styles from './DashboardStaticCard.module.scss'
const { Meta } = Card

const Ranges = [
  'Pre - K: 0 - 49',
  'Kindergarten: 50 - 99',
  'First: 100 - 199',
  'Second: 200 - 299',
  'Third: 300 - 399',
]

export const DashboardStaticCard = (): ReactElement => {
  const { CardContainer, CardText } = styles
  return (
    <>
      {/* <div className={CardContainer}> */}
      <Card
        hoverable
        className="parent-dashboard-static-card"
        style={{ borderRadius: 7 }}
      >
        <Meta title="Ranges" className="parent-card-meta" />
        <div
          style={{
            marginBottom: '4px',
            color: '#495f74',
            fontWeight: 'bold',
            textAlign: 'center',
          }}
        >
          Grade level ranges for tag scores
        </div>
        <ul>
          {Ranges.map((example, indx) => (
            <li className={CardText} key={indx}>
              {example}
            </li>
          ))}
        </ul>
      </Card>
      {/* </div> */}
    </>
  )
}

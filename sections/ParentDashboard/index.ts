import dynamic from 'next/dynamic'
import { LearningAssessment as LearningAssessmentImport } from './LearningAssessment/LearningAssessment.section'
import { DashboardStaticCard as DashboardStaticCardImport } from './DashboardStaticCard/DashboardStaticCard.section'

export const LearningAssessment = dynamic(
  () =>
    import('./LearningAssessment/LearningAssessment.section').then(
      (module) => module.LearningAssessment
    ) as any
) as typeof LearningAssessmentImport

export const DashboardStaticCard = dynamic(
  () =>
    import('./DashboardStaticCard/DashboardStaticCard.section').then(
      (module) => module.DashboardStaticCard
    ) as any
) as typeof DashboardStaticCardImport

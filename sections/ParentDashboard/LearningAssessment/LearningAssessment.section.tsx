import { Card, Typography } from 'antd'
import clsx from 'clsx'
import { ClockCircleOutlined } from '@ant-design/icons'
import { BarChart } from 'components'
import styles from './LearningAssessment.module.scss'
import { ReactElement } from 'react'

const DashboardCard = ({
  title,
  titlePostfix,
  description,
  icon,
}): ReactElement => {
  const {
    card,
    cardContent,
    cardHeading,
    cardHeadingPost,
    cardDescription,
    cardIcon,
  } = styles
  return (
    <Card className={card}>
      <div className={cardContent}>
        <Typography.Title level={4} className={clsx('mb-0', cardHeading)}>
          {title} <span className={cardHeadingPost}>{titlePostfix}</span>
        </Typography.Title>
        <Typography.Paragraph className={clsx(cardDescription)}>
          {description}
        </Typography.Paragraph>
      </div>
      <div className={cardIcon}>{icon}</div>
    </Card>
  )
}

const data = [
  { name: 'Geometry', value: 286 },
  { name: 'Measurement and Data', value: 254 },
  { name: 'Numbers and Operations in Base 10', value: 310 },
  { name: 'Operations and Algabric Thinking', value: 263 },
  { name: 'Functions', value: 295 },
]

const ChartCard = (): ReactElement => {
  const { card } = styles
  return (
    <Card className={card}>
      <BarChart data={data} />
    </Card>
  )
}

export const LearningAssessment = (): ReactElement => {
  const { cards, cardsCard, cardsChart } = styles
  return (
    <div className={cards}>
      <div className={cardsCard}>
        <DashboardCard
          title="125"
          titlePostfix="Minutes"
          description="Spent Last 30 Days"
          icon={<ClockCircleOutlined />}
        />
      </div>
      <div className={cardsCard}>
        <DashboardCard
          title="44"
          titlePostfix="Minutes"
          description="Spent Last 7 Days"
          icon={<ClockCircleOutlined />}
        />
      </div>
      <div className={cardsChart}>
        <ChartCard />
      </div>
    </div>
  )
}

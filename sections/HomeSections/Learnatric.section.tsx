import { Heading, TestimonialCard } from 'components'
import { ReactElement } from 'react'
import { Button } from 'antd'
import styles from './Learnatric.module.scss'
import { testimonialData } from 'components/Constants/testimonials'
import { RoboClass } from 'sections'
import Link from 'next/link'

export const LearnatricSection = (): ReactElement => {
  const {
    learnatricSection,
    learnatricMainContent,
    learnatricHeading,
    testimonialContainer,
    bannerButtonContainer,
    bannerSignupButton,
  } = styles
  return (
    <section className={learnatricSection}>
      {/* <RoboClass 
        heading={"What makes Learnatric unique"}
        url={"https://learnatric-other-videos.s3.amazonaws.com/webSiteVideo.mp4"}
      /> */}
      <div className={learnatricMainContent}>
        <Heading className={learnatricHeading}>
          Hear What Other Parents Have to Say
        </Heading>
      </div>
      <div className={testimonialContainer}>
        {testimonialData.map((testimonial) => (
          <TestimonialCard key={testimonial.name} {...testimonial} />
        ))}
      </div>
      <img
        src="https://learnatric-website-images.s3.amazonaws.com/Data+Collection+2.0+(70+x+38.74+in).svg"
        alt="banner"
        width="100%"
        height="70%"
      />
      <div className={bannerButtonContainer} style={{ marginTop: '10px' }}>
        <Link href="/signup">
          <a>
            <Button className={bannerSignupButton} type="primary">
              Sign Up For Free Trial
            </Button>
          </a>
        </Link>
      </div>
    </section>
  )
}

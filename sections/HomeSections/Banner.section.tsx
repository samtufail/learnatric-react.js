import Link from 'next/dist/client/link'
import { ReactElement } from 'react'
import styles from './Banner.module.scss'
import { Button } from 'antd'
import { Paragraph } from 'components'
import Image from 'next/image'
import { RoboClass } from 'sections'
const bannerSectionText = [
  'Imagine hearing your child add up the grocery bill.',
  'Or counting down the seconds left on the microwave.',
  'Or reading the time on the clock.',
  'Imagine working alongside your child, while she works independently next to you.',
  'These are just a few of the experiences children have with Learnatric.',
  'Learnatric is a personalized learning plan made just for your child.',
]

export const BannerSection = (): ReactElement => {
  const {
    bannerContainer,
    bannerImage,
    bannerContent,
    bannerContentText,
    bannerContentImage,
    bannerContentTextHeading,
    bannerContentTextHeading2,
    bannerContentTextDescription,
    bannerSignupButton,
    bannerButtonContainer,
  } = styles
  return (
    <div className={bannerContainer}>
      {/* <div className={bannerImage}> */}
      <img
        src="https://learnatric-website-images.s3.amazonaws.com/12.16.2021/Website+banner+1.3.png"
        alt="banner"
        width="100%"
        height="70%"
      />
      {/* </div> */}
      {/* <div className={bannerContent}>
        <div className={bannerContentText}> */}
      <div className={bannerButtonContainer} style={{ marginBottom: '10px' }}>
        <Link href="/signup">
          <a>
            <Button className={bannerSignupButton} type="primary">
              Sign Up For Free Trial
            </Button>
          </a>
        </Link>
      </div>
      <img
        src="https://learnatric-website-images.s3.amazonaws.com/Website+Images+2.2+Overview+(2300+x+980+px).svg"
        alt="banner"
        width="100%"
        height="70%"
      />
      <div className={bannerButtonContainer} style={{ marginBottom: '30px' }}>
        <Link href="/signup">
          <a>
            <Button className={bannerSignupButton} type="primary">
              Sign Up For Free Trial
            </Button>
          </a>
        </Link>
      </div>
      {/* <div
        className={bannerButtonContainer}
        style={{ marginBottom: '30px', marginTop: '10px' }}
        >
        <Link href="/signup">
        <a>
        <Button className={bannerSignupButton} type="primary">
        Sign Up For Free Trial
        </Button>
        </a>
        </Link>
      </div> */}
      {/* <img
        src="https://learnatric-website-images.s3.amazonaws.com/Website+Image+3.1+Bloom+quote.svg"
        alt="banner"
        width="100%"
        height="70%"
      /> */}
      <img
        src="https://learnatric-website-images.s3.amazonaws.com/Website+Images+4.3+Lesson+Format+(2300+x+981+px).svg"
        alt="banner"
        width="100%"
        height="70%"
      />
      <div
        className={bannerButtonContainer}
        style={{ marginBottom: '30px', marginTop: '10px' }}
      >
        <Link href="/signup">
          <a>
            <Button className={bannerSignupButton} type="primary">
              Sign Up For Free Trial
            </Button>
          </a>
        </Link>
      </div>
      <RoboClass
        heading={'What makes Learnatric unique:'}
        url={
          'https://learnatric-other-videos.s3.amazonaws.com/webSiteVideo.mp4'
        }
      />
      <div
        className={bannerButtonContainer}
        style={{ marginBottom: '30px', marginTop: '10px' }}
      >
        <Link href="/signup">
          <a>
            <Button className={bannerSignupButton} type="primary">
              Sign Up For Free Trial
            </Button>
          </a>
        </Link>
      </div>
      {/* <img
        src="https://learnatric-website-images.s3.amazonaws.com/Data+Collection+2.0+(70+x+38.74+in).svg"
        alt="banner"
        width="100%"
        height="70%"
      />
      <div className={bannerButtonContainer} style={{ marginTop: '10px' }}>
        <Link href="/signup">
          <a>
            <Button className={bannerSignupButton} type="primary">
              Sign Up For Free Trial
            </Button>
          </a>
        </Link>
      </div> */}
      {/* </div>
      </div> */}
      {/* <div className={bannerContent}>
        <div className={bannerContentText}>
          <div className={bannerContentTextHeading}>
            <strong>Learnatric:</strong> Personalized, interactive math
            education
          </div>
          <br />
          <div className={bannerContentTextHeading2}> 
            Dynamic, new learning paths empowered by AI-technology
          </div>
          <br />
          <Link href="/signup">
            <a>
              <Button className={bannerSignupButton} type="primary">
                Sign Up For Free Trial
              </Button>
            </a>
          </Link>
          {bannerSectionText !== undefined && bannerSectionText.length ? (
            bannerSectionText.map((paragraph, idx) => (
              <Paragraph key={idx} className={bannerContentTextDescription}>
                {paragraph}
              </Paragraph>
            ))
          ) : (
            <></>
          )}
          <Paragraph className={bannerContentTextDescription}>
            <strong>Learnatric empowers children to:</strong>
            <Paragraph className={bannerContentTextDescription}>
              <ul>
                <li>Ignite a passion for learning</li>
                <li>Learn efficiently with limited screen time</li>
                <li>Work through problems independently</li>
                <li>Break down hard-to-reach concepts</li>
                <li>
                  Speed up learning through personalized tutoring strategies
                </li>
              </ul>
            </Paragraph>
            We’ll meet your child where they are, and help expand their skills,
            and increase their confidence
          </Paragraph>
        </div>
        <div className={bannerContentImage}>
          <Image
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632243757/HomePage/BannerSection/banner_img_a83iji.png"
            alt="banner-girl"
            width="350"
            height="500"
          />
        </div>
      </div> */}
    </div>
  )
}

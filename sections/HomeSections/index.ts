import dynamic from 'next/dynamic'
import { FeaturesSection as FeaturesSectionImport } from './Features.section'
import { BannerSection as BannerSectionImport } from './Banner.section'
import { LearnatricSection as LearnatricSectionImport } from './Learnatric.section'
import { SocialSubscribeSection as SocialSubscribeSectionImport } from './SocialSubscribe.section'

export const FeaturesSection = dynamic(
  () =>
    import('./Features.section').then((module) => module.FeaturesSection) as any
) as typeof FeaturesSectionImport

export const BannerSection = dynamic(
  () => import('./Banner.section').then((module) => module.BannerSection) as any
) as typeof BannerSectionImport

export const LearnatricSection = dynamic(
  () =>
    import('./Learnatric.section').then(
      (module) => module.LearnatricSection
    ) as any
) as typeof LearnatricSectionImport

export const SocialSubscribeSection = dynamic(
  () =>
    import('./SocialSubscribe.section').then(
      (module) => module.SocialSubscribeSection
    ) as any
) as typeof SocialSubscribeSectionImport

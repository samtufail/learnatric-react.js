import { Heading } from 'components'
import { ReactElement } from 'react'
import styles from './SocialSubscribe.module.scss'

export const SocialSubscribeSection = (): ReactElement => {
  const { socialContainer, socialHeading, socialSubHeading, socialIcons } =
    styles

  return (
    <>
      <section className={socialContainer}>
        <Heading level={3} className={socialHeading}>
          Stay up to date with Learnatric
        </Heading>
        <Heading level={4} className={socialSubHeading}>
          Follow us on Social!
        </Heading>
        <div className={socialIcons}>
          <a
            href="https://www.facebook.com/Learnatric-102698231917165"
            target="_blank"
            rel="noreferrer"
          >
            {' '}
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244022/HomePage/SocialSubscribe/facebook_qp5hlt.png"
              alt="facebook"
              width="80"
              height="80"
            />
          </a>
          <a
            href="https://www.linkedin.com/company/learnatric"
            target="_blank"
            rel="noreferrer"
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244023/HomePage/SocialSubscribe/linkedin_piw09e.png"
              alt="linkedin"
              width="80"
              height="80"
            />
          </a>
        </div>
      </section>
    </>
  )
}

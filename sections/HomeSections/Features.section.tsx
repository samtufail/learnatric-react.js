import { Heading, Paragraph } from 'components'
import { ReactElement } from 'react'
import styles from './Features.module.scss'
import Link from 'next/link'
import { Button } from 'antd'

const featureSecionText = [
  `Many online learning platforms make children complete a mountain of
  tasks to prove themselves. They complete assignments, level-by-level,
  in a prepackaged curriculum. This “fixed” approach of teaching offers
  only one way to learn. The monotony of these tasks does nothing to
  develop your child’s love of learning.`,
  `Think about it.`,
  `any of the other programs rely heavily on repetition. Expecting a
  student to prove they can do something over and over isn’t learning.
  Worse, if a mistake happens, the student gets docked points, or a
  random extra set of problems are added onto the assignment. This
  approach creates feelings of indifference and boredom.`,
  `Learnatric is different. Like a tutor, Learnatric responds to
  students’ progress in real time by recognizing their strengths and
  providing extra support for their challenge areas. While your child is
  learning the curriculum, Learnatric is learning your child’s unique
  learning patterns. By personalizing your child’s curriculum,
  Learnatric’s innovative AI technology actually accelerates the
  learning process.`,
  `Other programs can’t automate and “bend” in this way because they
  simply weren’t designed for it. Learnatric can, and does, which is
  what makes it such a valuable learning tool.`,
]

export const FeaturesSection = (): ReactElement => {
  const {
    features,
    featuresHeadingContainer,
    featuresHeadingText,
    bannerContentTextDescription,
    signupFormButton,
  } = styles
  return (
    <section className={features}>
      <div className={featuresHeadingContainer}>
        <Heading className={featuresHeadingText}>
          <p>Why choose Learnatric over other online educational programs?</p>
        </Heading>
      </div>
      <div className={bannerContentTextDescription}>
        {featureSecionText !== undefined && featureSecionText.length ? (
          featureSecionText.map((text, idx) => (
            <Paragraph key={idx}>{text}</Paragraph>
          ))
        ) : (
          <></>
        )}
      </div>
      <Link href="/signup">
        <a>
          <Button className={signupFormButton} type="primary">
            Sign Up Today
          </Button>
        </a>
      </Link>
    </section>
  )
}

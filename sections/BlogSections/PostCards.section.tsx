import { PostCard } from 'components'
import { ReactElement } from 'react'
import { blogPost } from '../../lib/blogPosts'
import styles from './PostCards.module.scss'

export const PostCards = (): ReactElement => {
  const { postCardsContainer, postCards } = styles
  return (
    <div className={postCardsContainer}>
      <div className={postCards}>
        {blogPost.map((post) => (
          <PostCard key={post.id} {...post} />
        ))}
      </div>
    </div>
  )
}

import dynamic from 'next/dynamic'
import { PostCards as PostCardsImport } from './PostCards.section'

export const PostCards = dynamic(
  () => import('./PostCards.section').then((module) => module.PostCards) as any
) as typeof PostCardsImport

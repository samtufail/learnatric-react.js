import dynamic from 'next/dynamic'
import { AddChildrenForm as AddChildrenFormImport } from './AddChildrenForm'

export const AddChildrenForm = dynamic(
  () =>
    import('./AddChildrenForm').then((module) => module.AddChildrenForm) as any
) as typeof AddChildrenFormImport

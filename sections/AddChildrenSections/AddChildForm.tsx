import { Button, Typography } from 'antd'
import { CustomInput, CustomInputDate, CustomSelect } from 'components'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'

import moment from 'moment'
import {
  addChildrenFormButton,
  birthdayDateInput,
} from './AddChildrenForm.module.scss'
import { addChild } from 'store/actions'

const initialValues = {
  firstName: '',
  lastName: '',
  dob: '',
  gender: '',
  grade: '',
  // Optional Fields
  teacherName: '',
  teacherEmail: '',
  school: '',
  schoolDistrict: '',
  canEmailTeacher: true,
  parentId: '',
}

const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .matches(/^(?!\s+$).*/gm, '* First Name cannot contain only blankspaces')
    .required('First Name is required!'),
  lastName: Yup.string()
    .matches(/^(?!\s+$).*/gm, '* First Name cannot contain only blankspaces')
    .required('Last Name is required!'),
  // dob: Yup.date().required('Birthdate is required!'),
  gender: Yup.string().required('Gender is required!'),
  grade: Yup.string().required('Grade is required!'),
})

export const AddChildForm = ({ next, last, parentId }) => {
  const dispatch = useDispatch()
  const onSubmit = async (values, { resetForm }) => {
    // Hint: Always store moment object when change on datepicker and format while submitting
    const dob = moment(values.dob).format('MM-DD-YYYY')
    const finalValues = {
      ...values,
      // dob,
      parentId,
    }
    dispatch(addChild(finalValues, resetForm, next, last))
  }

  // End Logic for Submit Children
  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  })

  return (
    <form onSubmit={formik.handleSubmit} className="add-child__form-wrapper">
      <Typography.Paragraph style={{ marginBottom: 0 }}>
        Fields marked with (*) are required to continue.
      </Typography.Paragraph>
      <div className="add-child__form">
        <CustomInput formik={formik} name="firstName" label="First Name *" />
        <CustomInput formik={formik} name="lastName" label="Last Name *" />
        {/* <CustomInputDate
          className={birthdayDateInput}
          formik={formik}
          name="dob"
          label="Birthday *"
          disableFuture
        /> */}
        <CustomSelect
          formik={formik}
          name="gender"
          label="Gender *"
          options={[
            { value: 'Male', title: 'Male' },
            { value: 'Female', title: 'Female' },
            { value: 'Other', title: 'Other' },
          ]}
        />

        <CustomSelect
          formik={formik}
          name="grade"
          label="Grade *"
          options={[
            { value: 'Pre-K', title: 'Pre-K' },
            { value: 'Kindergarten', title: 'Kindergarten' },
            { value: 'First', title: 'First' },
            { value: 'Second', title: 'Second' },
            { value: 'Third', title: 'Third' },
          ]}
        />
        <Typography.Title level={5} className="add-child__form-optional">
          Get the Full Experience (Optional):
        </Typography.Title>
        <CustomSelect
          formik={formik}
          name="canEmailTeacher"
          label="Can We Email Your Teacher? *"
          options={[
            { value: true, title: 'Yes' },
            { value: false, title: 'No' },
          ]}
        />
        <CustomInput
          formik={formik}
          name="teacherName"
          label="Teacher's Name"
        />
        <CustomInput
          formik={formik}
          name="teacherEmail"
          label="Teacher's Email"
        />
        <CustomInput formik={formik} name="school" label="School" />
        <CustomInput
          formik={formik}
          name="schoolDistrict"
          label="School District"
        />
        <Button
          className={addChildrenFormButton}
          type="primary"
          htmlType="submit"
        >
          Submit
        </Button>
      </div>
    </form>
  )
}

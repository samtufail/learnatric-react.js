import { StepPanel } from 'components'
import { ReactElement, useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
// import { fetchChildren } from 'store/actions'

import { addChildrenFormContainer } from './AddChildrenForm.module.scss'
import { AddChildForm } from './AddChildForm'
import router from 'next/router'

// Main Section Part Starts Here
export const AddChildrenForm = (): ReactElement => {
  const { account } = useSelector(({ parent }: AppState) => parent)
  const { childs, id, noOfChildsProfileCompleted } = account

  const [activeStep, setActiveStep] = useState(noOfChildsProfileCompleted)

  const next = () => {
    if (activeStep < childs - 1) {
      const nextStep = activeStep + 1
      setActiveStep(nextStep)
    }
  }

  const last = async () => {
    if (activeStep === childs - 1) {
      router.push('/create-children-profile')
    }
  }

  const steps = []
  for (let i = 1; i <= childs; i += 1) {
    steps.push({
      title: `Add Child ${i}`,
      content: <AddChildForm next={next} last={last} parentId={id} />,
    })
  }

  return (
    <div className={addChildrenFormContainer}>
      <StepPanel steps={steps} activeStep={activeStep} />
    </div>
  )
}

import { Typography } from 'antd'
import styles from './Testimonial.module.scss'

import { TestimonialCard } from 'components'
import { ReactElement } from 'react'

import { testimonials } from 'components/Constants/testimonials'

export const LPTestimonialSection = (): ReactElement => {
  const {
    testimonial,
    testimonialHeading,
    testimonialBox,
    testimonialBoxInner,
    testimonialContainer,
  } = styles
  return (
    <div className={testimonial}>
      <Typography.Title level={1} className={testimonialHeading}>
        See What Other Parents Are Saying
      </Typography.Title>
      <div className={testimonialBox}>
        <div className={testimonialBoxInner}>
          <div className={testimonialContainer}>
            {testimonials.map((testimonial) => (
              <TestimonialCard key={testimonial.name} {...testimonial} />
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

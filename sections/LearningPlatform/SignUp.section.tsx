import Link from 'next/link'
import { Typography, Button } from 'antd'
import styles from './SignUp.module.scss'
import { ReactElement } from 'react'

export const LPsignUp = (): ReactElement => {
  const { signup, signupHeading, signupFormButton } = styles
  return (
    <div className={signup}>
      <Typography.Title className={signupHeading}>
        Ready to Sign Up?
      </Typography.Title>
      <Link href="/signup">
        <a>
          <Button className={signupFormButton} type="primary">
            Signup
          </Button>
        </a>
      </Link>
    </div>
  )
}

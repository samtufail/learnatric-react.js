import { Button, Typography } from 'antd'
import Link from 'next/link'
import { ReactElement } from 'react'
import styles from './Quote.module.scss'

export const QuoteSection = (): ReactElement => {
  const {
    quoteContainer,
    quote,
    quoteText,
    quotation,
    quotationImage,
    quotationBold,
    author,
    learningPlatformButton,
    learningPlatformSignupButton,
  } = styles
  return (
    <div className={quoteContainer}>
      <div className={quote}>
        <div className={quoteText}>
          <Typography.Paragraph className={quotation}>
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246634/LearningPlatform/LearningPlatformQuote/learning-platform-quote_e9fyby.png"
              width="32"
              height="32"
              className={quotationImage}
            />
            The <strong className={quotationBold}>tutoring process</strong>{' '}
            demonstrates that most of the students do have the potential to
            reach this high level of learning [i.e., the 98th percentile]. I
            believe an important task of research and instruction is to seek
            ways of accomplishing this under more practical and realistic
            conditions than the{' '}
            <strong className={quotationBold}>one-to-one</strong> tutoring
            [option], which is too costly for most societies to bear on a large
            scale.
          </Typography.Paragraph>
          <Typography.Paragraph className={author}>
            <strong>Dr. Benjamin S. Bloom</strong>, American Educational
            Psychologist
          </Typography.Paragraph>
        </div>
      </div>
      <div className={learningPlatformButton}>
        <Link href="/signup">
          <a>
            <Button className={learningPlatformSignupButton} type="primary">
              Sign Up For Free Trial
            </Button>
          </a>
        </Link>
      </div>
    </div>
  )
}

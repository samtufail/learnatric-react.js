import dynamic from 'next/dynamic'
import { QuoteSection as QuoteSectionImport } from './Quote.section'
import { WorksSection as WorksSectionImport } from './Works.section'
import { LPTestimonialSection as LPTestimonialSectionImport } from './Testimonial.section'
import { LPsignUp as LPsignUpImport } from './SignUp.section'

export const QuoteSection = dynamic(
  () => import('./Quote.section').then((module) => module.QuoteSection) as any
) as typeof QuoteSectionImport

export const WorksSection = dynamic(
  () => import('./Works.section').then((module) => module.WorksSection) as any
) as typeof WorksSectionImport

export const LPTestimonialSection = dynamic(
  () =>
    import('./Testimonial.section').then(
      (module) => module.LPTestimonialSection
    ) as any
) as typeof LPTestimonialSectionImport

export const LPsignUp = dynamic(
  () => import('./SignUp.section').then((module) => module.LPsignUp) as any
) as typeof LPsignUpImport

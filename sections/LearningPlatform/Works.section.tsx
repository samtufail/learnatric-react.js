import { Typography } from 'antd'
import { FeaturesCard } from 'components'
import { ReactElement } from 'react'
import styles from './Works.module.scss'
import { featuresContent2 } from 'components/Constants/features'

export const WorksSection = (): ReactElement => {
  const {
    worksBox,
    worksBoxInner,
    worksHeading,
    worksCardsContainer,
    worksCards,
  } = styles
  return (
    <div className={worksBox}>
      <div className={worksBoxInner}>
        <Typography.Title level={1} className={worksHeading}>
          How It Works
        </Typography.Title>
        <div className={worksCardsContainer}>
          <div className={worksCards}>
            {featuresContent2.map((feature) => (
              <FeaturesCard key={feature.heading} {...feature} />
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

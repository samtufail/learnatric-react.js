export interface Parent {
  id: number
  firstName: string
  lastName: string
  email: string
  hearedFrom: string
  childs: number
  stripeId: string
  phoneNumber: string
  stripeSubsID: string
  step: string
  createdAt: string
  updatedAt: string
  noOfChildsProfileCompleted: number
  noOfChildsCredsCompleted: number
  childrensProfiles: any[]
  childrens: any[]
}

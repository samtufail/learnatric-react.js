export interface Question {
  question_id: number
  lesson_id: number
  tagret: string
  question_text: string
  correct_answer: string
  question_rating: string
  p_tag: string
  s_tag1: string
  s_tag2: string
  s_tag3: string
  s_tag4: string
  s_tag5: string
  s_tag6: string
  audio_url: string
  illustration_url: string
  question_type: string
  mc1: string
  mc2: string
  mc3: string
  mc4: string
  times_attempted?: number
  varience?: number
}

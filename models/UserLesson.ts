export interface UserLesson {
  user_lesson_id: number
  child_id: number
  lesson_id: number
  instruction_completed: Boolean
  activity_completed: Boolean
  problem_set: Boolean
  problem_questions_attempted: number
  start_date_time: string
  end_date_time: string
}

export interface Lesson {
  id: number
  tag_id: number
  lesson_name: string
  lesson_description: string
  standard_id: string
  grade: string
  i_do_url: string
  we_do_url1: string
  we_do_url2: string
  we_do_url3: string
  we_do_question_text: string
  question_type: string
  answer: string
  mc1: string
  mc2: string
  mc3: string
  mc4: string
}

/* eslint-disable */
import axios, { AxiosResponse, AxiosRequestConfig } from 'axios'
import Router from 'next/router'

const axiosInstance = axios.create({
  // baseURL: 'http://localhost:8081/api/v1',
  // baseURL: 'https://learnatric.com/api/v1',
  //test api for all Questions
  baseURL: 'http://3.144.39.140:8081/api/v1',
})
const ResponseInterceptor = (response: AxiosResponse) => {
  return response
}
const RequestInterceptor = (config: AxiosRequestConfig) => {
  config.headers.Authorization = 'Bearer ' + localStorage.getItem('TOKEN')
  return config
}
axiosInstance.interceptors.request.use(RequestInterceptor)
axiosInstance.interceptors.response.use(ResponseInterceptor, (error) => {
  const expectedErrors =
    error.response &&
    error.response.status >= 400 &&
    error.response.status <= 500
  if (!expectedErrors) {
    const unExpectedError = new Error('Unexpected Error Occured')
    return Promise.reject(unExpectedError)
  } else {
    if (error.response.status === 401) {
      localStorage.removeItem('TOKEN')
      localStorage.removeItem('PARENT_ID')
      localStorage.removeItem('CHILD_ID')
      localStorage.removeItem('TYPE')
      Router.push('/login')
    }
    return Promise.reject(error)
  }
})

export default axiosInstance

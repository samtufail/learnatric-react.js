import Image from 'next/image'
import { Menu, Dropdown, Avatar, Typography } from 'antd'
import {
  UserSwitchOutlined,
  VideoCameraOutlined,
  LogoutOutlined,
} from '@ant-design/icons'
import Link from 'next/link'
import { ReactElement } from 'react'
import {
  navbarContainer,
  logo,
  userProfileContainer,
  userProfile,
  userProfileText,
  dropdownMenu,
  dropdownButton,
  userProfileTextName,
  userProfileTextDesignation,
  menuItemsContainer,
  linkText,
} from './ChildNavbar.module.scss'
import { Down } from 'icons'
import { useSelector, useDispatch } from 'react-redux'
import {
  switchToParent,
  startInitialVideo,
  changeVideoDisplay,
  logoutUser,
  setPage,
} from '../../store/actions'
import { useRouter } from 'next/router'

export const ChildNavbar = (): ReactElement => {
  const {
    loading,
    account,
    currentLesson,
    cameFromLesson,
    playInitialVideo,
    initialVideoDisplay,
  } = useSelector(({ child }: AppState) => child)
  const dispatch = useDispatch()
  const router = useRouter()
  const handleParentClick = () => {
    // if (!playInitialVideo) {
    dispatch(switchToParent(account.id))
    // }
  }
  const handleClickLogout = () => {
    dispatch(setPage('Home'))
    dispatch(logoutUser(router))
  }
  const menu = (
    <Menu>
      <Menu.Item key="0">
        <div className={menuItemsContainer} onClick={handleParentClick}>
          <UserSwitchOutlined
            style={{ color: 'green', fontSize: '24px', marginRight: '3px' }}
          />
          <Typography.Paragraph className={linkText}>
            Switch to Parent
          </Typography.Paragraph>
          {/* <p>Switch to Parent</p> */}
        </div>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="1">
        <div className={menuItemsContainer} onClick={handleClickLogout}>
          <LogoutOutlined
            style={{ color: 'red', fontSize: '24px', marginRight: '3px' }}
          />
          <Typography.Paragraph className={linkText}>
            Logout
          </Typography.Paragraph>
        </div>
      </Menu.Item>
      {/* <Menu.Item key="1">
          <a href="https://www.aliyun.com">2nd menu item</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">3rd menu item</Menu.Item> */}
    </Menu>
  )
  // const {
  //   navbarContainer,
  //   logo,
  //   userProfileContainer,
  //   userProfile,
  //   userProfileText,
  //   dropdownMenu,
  //   dropdownButton,
  //   userProfileTextName,
  //   userProfileTextDesignation,
  //   menuItemsContainer,
  // } = styles
  return (
    <>
      <div className={navbarContainer}>
        <Link href="/">
          <a>
            <div className={logo}>
              <Image
                src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632250728/components/Navbar/logo_tg0sgi.png"
                alt="learnatric-logo"
                width="200"
                height="40"
              />
            </div>
          </a>
        </Link>
        <div className={userProfileContainer}>
          <Dropdown overlay={menu} trigger={['click']}>
            <a
              className="ant-dropdown-link child-navbar__dropdown-link"
              onClick={(e) => e.preventDefault()}
            >
              <div className={userProfile}>
                <div className={userProfileText}>
                  <p className={userProfileTextName}>{account.firstName}</p>
                  <p className={userProfileTextDesignation}>Student</p>
                </div>
                <div className={dropdownMenu}>
                  <Avatar src={account.avatarSmall} alt="avatar" size="large" />
                  <Down />
                </div>
              </div>
            </a>
          </Dropdown>
        </div>
      </div>
    </>
  )
}

import { MenuOutlined } from '@ant-design/icons'
import { Button, Drawer } from 'antd'
import Image from 'next/image'
import Link from 'next/link'
import { ReactElement, useState } from 'react'
import styles from './ChildNavbarHome.module.scss'

const initialLinksValue = [
  { title: 'Home', href: '/' },
  { title: 'About Us', href: '/about-us' },
  { title: 'Our Learning Platform', href: '/our-learning-platform' },
  { title: 'Our Curriculum', href: '/our-curriculum' },
  { title: 'Blog', href: '/blog' },
]
export const ChildNavbarHome = (): ReactElement => {
  const [open, setOpen] = useState(false)

  const {
    childNavbarHomeContainer,
    logoContainer,
    logo,
    linksContainer,
    childNavbarHomeButton,
    menuButton,
    mobileLinksContainer,
    drawerContainer,
    drawerLinksContainer,
  } = styles
  return (
    <div className={childNavbarHomeContainer}>
      <div className={logoContainer}>
        <Link href="/">
          <a>
            <div className={logo}>
              <Image
                src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632250728/components/Navbar/logo_tg0sgi.png"
                alt="learnatric-logo"
                width="200"
                height="40"
              />
            </div>
          </a>
        </Link>
      </div>
      <div className={linksContainer}>
        {initialLinksValue.map(({ title, href }) => (
          <Link href={href} key={title}>
            <a>{title}</a>
          </Link>
        ))}

        <Link href="/signup">
          <a>
            <Button className={childNavbarHomeButton} type="primary">
              Login/signup
            </Button>
          </a>
        </Link>
      </div>
      <div className={mobileLinksContainer}>
        <div className="antd-nav-button">
          <Button
            onClick={() => setOpen(true)}
            className={menuButton}
            type="primary"
            icon={<MenuOutlined />}
          />
        </div>
        <Drawer
          title="Learnatric"
          placement="right"
          closable={false}
          onClose={() => setOpen(false)}
          visible={open}
          className={drawerContainer}
          headerStyle={{
            fontSize: '25px',
          }}
        >
          <div className={drawerLinksContainer}>
            {initialLinksValue.map(({ title, href }) => (
              <Link href={href} key={title}>
                <a>{title}</a>
              </Link>
            ))}
            <Link href="/signup">
              <a>
                <Button className={childNavbarHomeButton} type="primary">
                  Login/signup
                </Button>
              </a>
            </Link>
          </div>
        </Drawer>
      </div>
    </div>
  )
}

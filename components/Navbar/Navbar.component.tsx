import { useState, useEffect, ReactElement } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { useSelector, useDispatch } from 'react-redux'
import { Drawer, Button } from 'antd'
import { MenuOutlined } from '@ant-design/icons'

import styles from './Navbar.module.scss'
import { Body20Bold } from 'components'
import { logoutUser, setPage } from 'store/actions'
import { useRouter } from 'next/router'
import { useMediaQuery } from 'react-responsive'

const initialLinksValue = [
  { title: 'Home', href: '/' },
  // { title: 'Our Learning Platform', href: '/our-learning-platform' },
  { title: 'Our Curriculum', href: '/our-curriculum' },
  { title: 'Meet the Team', href: '/meet-team' },
  { title: 'Login', href: '/login' },
  { title: 'Sign Up', href: '/signup' },
  { title: 'Blog', href: '/blog' },
]

const loggedInLinksValue = [{ title: 'Logout', href: '/' }]

const initialParentLinkValue = [
  {
    title: 'Parent Dashboard',
    href: '/parent-dashboard',
    hiddenOnDesktop: true,
  },
  {
    title: 'Account Settings',
    href: '/parent-dashboard/account-settings',
    hiddenOnDesktop: true,
  },
  {
    title: 'Billing',
    href: '/parent-dashboard/billing',
    hiddenOnDesktop: true,
  },
  {
    title: 'Help and Support',
    href: '/parent-dashboard/help-support',
    hiddenOnDesktop: true,
  },
  { title: 'Logout', href: '/' },
]
// const settingsParentLinksValue = [
//   { title: 'Logout', href: '/' },
//   { title: 'Parent Dashboard', href: '/parent-dashboard' },
// ]

const CustomLink = ({ title, href, onClick }) => (
  <div style={{ cursor: 'pointer' }} onClick={() => onClick(title)}>
    <Link href={href}>{title}</Link>
  </div>
)

export const Navbar = (): ReactElement => {
  const { account } = useSelector(({ child }: AppState) => child)
  const { isLoggedIn, type, page } = useSelector(({ auth }: AppState) => auth)
  const router = useRouter()
  const dispatch = useDispatch()
  const [open, setOpen] = useState(false)
  const [links, setLinks] = useState(initialLinksValue)
  const isDesktop = useMediaQuery({
    query: '(min-width: 1024px)',
  })

  useEffect(() => {
    if (isLoggedIn && type === 'PARENT') {
      if (isDesktop) {
        setLinks(loggedInLinksValue)
      } else {
        setLinks(initialParentLinkValue)
      }
    } else if (isLoggedIn && type === 'CHILD') {
      setLinks(loggedInLinksValue)
    } else {
      setLinks(initialLinksValue)
    }
  }, [isLoggedIn, page])

  const handleLinkClick = (link) => {
    if (link.title === 'Logout') {
      dispatch(setPage('Home'))
      dispatch(logoutUser(router))
    } else {
      dispatch(setPage(link.title))
      router.push(link.href)
    }
  }

  const {
    nav,
    logoContainer,
    logo,
    linksContainer,
    mobileLinksContainer,
    menuButton,
    drawerContainer,
    drawerLinksContainer,
    drawerAvatarContainer,
    drawerAvatar,
  } = styles
  return (
    <div className={nav}>
      <div className={logoContainer}>
        <Link href="/">
          <a>
            <div className={logo}>
              <Image
                src="https://learnatric-website-images.s3.amazonaws.com/12.16.2021/Learnatric+logo+2.3.png"
                alt="learnatric-logo"
                width="210"
                height="50"
              />
            </div>
          </a>
        </Link>
      </div>
      <div className={linksContainer}>
        {links.map((link) => (
          <CustomLink
            onClick={() => handleLinkClick(link)}
            key={link.title}
            {...link}
          />
        ))}
      </div>
      <div className={mobileLinksContainer}>
        <div className="antd-nav-button">
          <Button
            onClick={() => setOpen(true)}
            className={menuButton}
            type="primary"
            icon={<MenuOutlined />}
          />
        </div>
        <Drawer
          title="Learnatric"
          placement="right"
          closable={false}
          onClose={() => setOpen(false)}
          visible={open}
          className={drawerContainer}
          headerStyle={{
            fontSize: '25px',
          }}
        >
          <div className={drawerLinksContainer}>
            {links.map((link) => (
              <CustomLink
                onClick={() => handleLinkClick(link)}
                key={link.title}
                {...link}
              />
            ))}
          </div>
          {isLoggedIn && type === 'CHILD' ? (
            <div className={drawerAvatarContainer}>
              <div className={drawerAvatar}>
                {account?.avatarLarge && (
                  <Image
                    src={account.avatarLarge}
                    alt="Learning Path"
                    height="90"
                    width="90"
                    objectFit="contain"
                    layout="responsive"
                  />
                )}
                {/* <span className={classes.gold}>50</span> */}
              </div>
              <div>
                <Body20Bold style={{ marginTop: 15 }}>
                  {/* {parent.firstName + ' ' + parent.lastName} */}
                </Body20Bold>
              </div>
            </div>
          ) : (
            <></>
          )}
        </Drawer>
      </div>
    </div>
  )
}

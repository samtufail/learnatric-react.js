import { Paragraph } from 'components'
import styles from './PostQouteCard.module.scss'

export const PostQouteCard = ({ qoutationText }) => {
  const { qoute, quoteImage, qouteText } = styles
  return (
    <div className={qoute}>
      <img
        className={quoteImage}
        src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632250849/components/PostQouteCard/quote_pgoigp.png"
        alt="quote"
      />
      <Paragraph className={qouteText}>{qoutationText}</Paragraph>
    </div>
  )
}

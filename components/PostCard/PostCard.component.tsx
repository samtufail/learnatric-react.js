import { ReactElement, useState } from 'react'
import { Button, Card, Typography } from 'antd'
import styles from './PostCard.module.scss'
import Link from 'next/link'
const { Paragraph } = Typography
const { Meta } = Card

interface PostCardProps {
  id: string
  title: string
  author: string
  text: string
  postImage: string
}

export const PostCard = (props: PostCardProps): ReactElement => {
  const { id, title, author, text, postImage } = props
  const [ellipsis, setEllipsis] = useState(true)
  const { cardContainer, card, cardDescription, blogCardButton } = styles
  return (
    <div className={cardContainer}>
      <Card
        hoverable
        style={{ width: 300, borderRadius: 7 }}
        cover={<img alt="example" src={postImage} />}
      >
        <Meta title={title} description={author} />
        <Paragraph
          className={cardDescription}
          ellipsis={ellipsis ? { rows: 3, expandable: false } : false}
        >
          {text}
        </Paragraph>
        <Link href={`/blog/${id}`}>
          <a>
            <Button className={blogCardButton}>Read More</Button>
          </a>
        </Link>
      </Card>
    </div>
  )
}

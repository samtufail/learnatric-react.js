import { Card } from 'antd'
import { ReactElement } from 'react'
import styles from './ParentDashboardDataCard.module.scss'

interface CardProps {
  data: any
}
const { Meta } = Card

export const DashboardDataCard = ({ data }: CardProps): ReactElement => {
  const { CardContainer } = styles
  return (
    <>
      <div className={CardContainer}>
        <Card hoverable style={{ width: 350, borderRadius: 7 }}>
          <Meta title="Top 3 Tags" style={{ marginBottom: 15 }} />
          <li>data</li>
        </Card>
      </div>
    </>
  )
}

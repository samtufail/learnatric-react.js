import { useState, ReactElement, useEffect } from 'react'
import { Button, Layout, Modal, Typography } from 'antd'
import styles from './LessonsActivity.module.scss'
import ReactPlayer from 'react-player'
import Questions from './Questions'
import { tensFrameUrls } from 'components/Constants'
import NumberPad from './Questions/NumberPad.component'
import MultipleChoice from './Questions/MultipleChoice.component'
import TensFrame from './Questions/TensFrames.component'
import { Heading1 } from '../atoms/Text/Text'
import LoadingOverlay from 'react-loading-overlay'
import 'antd/dist/antd.css'
import { useDispatch, useSelector } from 'react-redux'
import {
  addAnswerResult,
  updateUserLesson,
  curateQuestion,
  handleAssessmentAnswers,
  handleAssessmentIdx,
} from 'store/actions'
import { Body20Bold } from 'components'
import {Next} from "icons"
import Image from 'next/dist/client/image'
import { PauseCircleFilled, PlayCircleFilled } from '@ant-design/icons'
const { Content } = Layout

const {
  nextPopup,
  containerheight,
  valueButton,
  inputNumberType,
  numberPadBgContainer,
  playBarContainerWeDo,
  customReactPlayerIDo,
  customReactPlayer,
  playBarContainer,
  playBarContainerButton,
  playBarContainerButtonIcon,
  numberPadHeadingText,
  padHeadingText,
  lessonActivityNumberPad,
  slideContainer,
  lessonActivityButtonContainer,
  lessonActivityButton,
  lessonActivityButtonText,
  lessonsActivityButtonContainer,
  lessonsActivityButton,
  lessonsActivityButtonText,
  modalTicCrossImage,
  antModalContent,
  modalButtonContainer,
  modalButton,
  modalButtonText,
  questionOuterContainer,
  questionInnerContainer,
} = styles

export const LessonsActivityComponent = (): ReactElement => {
  const dispatch = useDispatch()

  const {
    loading,
    account,
    userLesson,
    currentLesson,
    currentLessonQuestions,
  } = useSelector(({ child }: AppState) => child)
  const { question, index } = useSelector(
    ({ assessment }: AppState) => assessment
  )
  const { questionIndex } = account
  const IS_I_DO = account.assessment_complete && account.stepInLesson === null
  const IS_WE_DO = account.stepInLesson === 'we_do'
  const IS_YOU_DO = account.stepInLesson === 'question_set'
  const IS_ASSESSMENT = !account.assessment_complete
  const isNumpad = IS_ASSESSMENT
    ? question.question_type === 'number pad'
    : IS_YOU_DO
    ? currentLessonQuestions?.question_type === 'number pad'
    : IS_WE_DO
    ? currentLesson?.question_type === 'number pad'
    : false
  const isMCQS = currentLesson?.question_type === 'multiple choice'
  const isTenFrame = currentLesson?.question_type === 'tens frame'

  const we_dos_array: Array<any> = [
    currentLesson?.we_do_url1,
    currentLesson?.we_do_url2,
    currentLesson?.we_do_url3,
  ]

  const [inputNumbers, setInputNumbers] = useState<string | number>('')
  const [selectedFrame, setSelectedFrame] = useState<
    { [key: string]: any } | undefined
  >({})
  const [selectedFrameIndex, setSelectedFrameIndex] = useState(0)
  const [weDoIndex, setWeDoIndex] = useState<number>(0)
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false)
  const [shouldVidPlay, setShouldVidPlay] = useState<boolean>(true)
  const [weDoPlaying, setWeDoPlaying] = useState<boolean>(true)

  const onPlayClick = () => {
    setShouldVidPlay(!shouldVidPlay)
    setWeDoPlaying(!weDoPlaying)
  }

  const onSelectNumber = (input) => setInputNumbers(`${inputNumbers}${input}`)
  const selectedRadio = (input) => setInputNumbers(`${input}`)
  const onRemove = () => {
    setInputNumbers(
      `${inputNumbers}`.substring(0, `${inputNumbers}`.length - 1)
    )
  }

  const [animationClass, setAnimationClass] = useState<string>('')

  useEffect(() => {
    if (!IS_ASSESSMENT) {
      console.log('the we do you are on is: ', currentLesson)
    }
  }, [currentLesson])
  useEffect(() => {
    if (!IS_ASSESSMENT) {
      console.log('the question you are on is: ', currentLessonQuestions)
    }
  }, [currentLessonQuestions])
  useEffect(() => {
    if (isTenFrame) {
      setSelectedFrame(tensFrameUrls[0])
    }
  }, [weDoIndex, questionIndex])

  const startStopAnimation = (): void => {
    setAnimationClass(slideContainer)
    setTimeout(() => {
      setAnimationClass('')
    }, 2500)
  }

  const playModalAudio = (src: string): void => {
    new Audio(src).play()
  }

  const openPopup = (onOK, content, url) => {
    playModalAudio(url)
    Modal.info({
      title: '',
      className: 'modal__container',
      okText: (
        <>
          <div className={modalButtonContainer}>
            <button className={modalButton}>
              <img
                src="/img/child-dashboard/123.png"
                width="200px"
                alt="button"
              />
              <span className={modalButtonText}>Next</span>
            </button>
          </div>
        </>
      ),

      content: <div className={nextPopup}>{content}</div>,
      onOk() {
        onOK()
      },
    })
  }

  const incrementNumber = () => {
    if (selectedFrameIndex < 20) {
      setSelectedFrame(tensFrameUrls[selectedFrameIndex + 1])
      setSelectedFrameIndex(selectedFrameIndex + 1)
      setInputNumbers((selectedFrameIndex + 1).toString())
    }
  }

  const decrementNumber = () => {
    if (selectedFrameIndex) {
      setSelectedFrame(tensFrameUrls[selectedFrameIndex - 1])
      setSelectedFrameIndex(selectedFrameIndex - 1)
      setInputNumbers((selectedFrameIndex - 1).toString())
    }
  }

  const on_I_DO_Click = () => {
    dispatch(
      updateUserLesson(
        userLesson.user_lesson_id,
        'instruction_completed',
        localStorage.getItem('CHILD_ID')
      )
    )
    setInputNumbers('')
  }
  const on_We_DO_Click = () => {
    setShouldVidPlay(false)
    setIsModalOpen(true)
    let data = {
      answerResult: {
        parent_id: account.parentId,
        child_id: localStorage.getItem('CHILD_ID'),
        lesson_id: userLesson.lesson_id,
        question_id: userLesson.lesson_id,
        text_answer: inputNumbers,
        is_correct: currentLesson.answer === inputNumbers,
        is_we_do: true,
      },
      question: currentLesson,
      lastQuestion: false,
      userLesson_ID: userLesson.user_lesson_id,
    }

    if (inputNumbers === currentLesson.answer) {
      const content = (
        <>
          <img
            className={modalTicCrossImage}
            src="/img/child-dashboard/modal-tic.png"
            alt="modal Success"
          />
          <Typography.Paragraph className={antModalContent}>
            Great work!
          </Typography.Paragraph>
        </>
      )
      openPopup(
        () => {
          dispatch(addAnswerResult(data, true))
          dispatch(
            updateUserLesson(
              userLesson.user_lesson_id,
              'activity_completed',
              localStorage.getItem('CHILD_ID'),
              true
            )
          )
          dispatch(curateQuestion(currentLesson.tag_id, currentLesson.id))
          startStopAnimation()
          setIsModalOpen(false)
          setShouldVidPlay(true)
          setSelectedFrameIndex(0)
        },
        content,
        'https://learnatric-audio.s3.amazonaws.com/1.7.Correct+Answer.mp3'
      )
    } else {
      const content = (
        <>
          <img
            className={modalTicCrossImage}
            src="/img/child-dashboard/modal-cross.png"
            alt="modal Success"
          />
          <Typography.Paragraph className={antModalContent}>
            Try Again
          </Typography.Paragraph>
        </>
      )
      // eslint-disable-next-line no-lonely-if
      if (weDoIndex < 2) {
        openPopup(
          () => {
            setWeDoIndex(weDoIndex + 1)
            dispatch(addAnswerResult(data))
            setIsModalOpen(false)
            setShouldVidPlay(true)
            setSelectedFrameIndex(0)
          },
          content,
          "https://learnatric-audio.s3.amazonaws.com/Incorrect+Popup%2C+Let's+try+it+again.mp3"
        )
      } else {
        openPopup(
          () => {
            dispatch(addAnswerResult(data))
            dispatch(
              updateUserLesson(
                userLesson.user_lesson_id,
                'start-over',
                localStorage.getItem('CHILD_ID')
              )
            )
            setWeDoIndex(0)
            setIsModalOpen(false)
            setShouldVidPlay(true)
            setSelectedFrameIndex(0)
          },
          content,
          "https://learnatric-audio.s3.amazonaws.com/Incorrect+Popup%2C+Let's+try+it+again.mp3"
        )
      }
    }
    setInputNumbers('')
  }

  const onClickNext = () => {
    // Creating The Data
    setIsModalOpen(true)
    if (isTenFrame) {
      setSelectedFrameIndex(0)
      setSelectedFrame(tensFrameUrls[0])
    }

    let data = {
      answerResult: {
        parent_id: account.parentId,
        child_id: localStorage.getItem('CHILD_ID'),
        lesson_id: userLesson.lesson_id,
        question_id: currentLessonQuestions.question_id,
        text_answer: inputNumbers,
        is_correct: currentLessonQuestions.correct_answer === inputNumbers,
      },
      questionIndex: questionIndex + 1,
      question: currentLessonQuestions,
      lastQuestion: false,
      userLesson_ID: userLesson.user_lesson_id,
    }
    // Checking If Last Question
    if (questionIndex + 1 === 5) {
      data = { ...data, lastQuestion: true }
      dispatch(
        updateUserLesson(
          userLesson.user_lesson_id,
          'problem_set',
          localStorage.getItem('CHILD_ID')
        )
      )
    }
    // Type of Question
    // if (currentLessonQuestions.question_type === 'tens frame') {
    //   console.log('Tens Frame Question Answered')
    // }
    // Open the modal here

    let content: any = null
    const url: string = data.answerResult.is_correct
      ? 'https://learnatric-audio.s3.amazonaws.com/1.7.Correct+Answer.mp3'
      : "https://learnatric-audio.s3.amazonaws.com/Incorrect+Popup%2C+Let's+try+it+again.mp3"
    if (!data.answerResult.is_correct) {
      content = (
        <>
          <img
            className={modalTicCrossImage}
            src="/img/child-dashboard/modal-cross.png"
            alt="modal Success"
          />
          <Typography.Paragraph className={antModalContent}>
            The correct answer is: {currentLessonQuestions.correct_answer}
          </Typography.Paragraph>
        </>
      )
    } else {
      content = (
        <>
          <img
            className={modalTicCrossImage}
            src="/img/child-dashboard/modal-tic.png"
            alt="modal Success"
          />
          <Typography.Paragraph className={antModalContent}>
            Great work!
          </Typography.Paragraph>
        </>
      )
    }
    if (questionIndex + 1 !== 5) {
      openPopup(
        () => {
          dispatch(addAnswerResult(data))
          setIsModalOpen(false)
          startStopAnimation()
          setSelectedFrameIndex(0)
          dispatch(curateQuestion(currentLesson.tag_id, currentLesson.id))
        },
        content,
        url
      )
    } else {
      console.log('questionIndex: ', questionIndex)
      openPopup(
        () => {
          dispatch(addAnswerResult(data))
          setIsModalOpen(false)
          startStopAnimation()
          setSelectedFrameIndex(0)
          // dispatch(curateQuestion(currentLesson.tag_id))
        },
        content,
        url
      )
    }
    // Save the data
    setInputNumbers('')
  }

  const keyDownHandler = (event) => {
    if (!isModalOpen && (IS_WE_DO || IS_ASSESSMENT) && isNumpad) {
      let key = window.event ? event.keyCode : event.which
      console.log('key: ', key)
      if (key == 8) {
        //remove last character of inputed string
        setInputNumbers(
          `${inputNumbers}`.substring(0, `${inputNumbers}`.length - 1)
        )
        return true
      } else if (key >= 48 && key <= 57) {
        //allow only number
        setInputNumbers(inputNumbers + String.fromCharCode(key))
        return true
      } else if (key >= 96 && key <= 105) {
        //allow only number
        setInputNumbers(inputNumbers + String.fromCharCode(key - 48))
        return true
      } else if (key === 13) {
        if (!inputNumbers) {
          setIsModalOpen(true)
          const content = (
            <>
              <Body20Bold>Oops! You forgot to answer the question</Body20Bold>
            </>
          )
          openPopup(
            () => {
              setIsModalOpen(false)
            },
            content,
            null
          )
        } else {
          // eslint-disable-next-line no-lonely-if
          if (IS_WE_DO) {
            on_We_DO_Click()
          } else if (IS_YOU_DO) {
            onClickNext()
          } else if (IS_ASSESSMENT) {
            console.log('in here!!!!')
            onAssessmentClick()
          }
        }
        return true
      } else if (key === 110 || key === 190) {
        setInputNumbers(`${inputNumbers}`.concat('.'))
        return true
      } else {
        return false
      }
    } else if (!isModalOpen && IS_YOU_DO && isNumpad) {
      let key = window.event ? event.keyCode : event.which
      if (key == 8) {
        //remove last character of inputed string
        setInputNumbers(
          `${inputNumbers}`.substring(0, `${inputNumbers}`.length - 1)
        )
        return true
      } else if (key >= 48 && key <= 57) {
        //allow only number
        setInputNumbers(inputNumbers + String.fromCharCode(key))
        return true
      } else if (key >= 96 && key <= 105) {
        //allow only number
        setInputNumbers(inputNumbers + String.fromCharCode(key - 48))
        return true
      } else if (key === 13) {
        if (!inputNumbers) {
          setIsModalOpen(true)
          const content = (
            <>
              <Body20Bold>Oops! You forgot to answer the question</Body20Bold>
            </>
          )
          openPopup(
            () => {
              setIsModalOpen(false)
            },
            content,
            null
          )
        } else {
          // eslint-disable-next-line no-lonely-if
          if (IS_WE_DO) {
            on_We_DO_Click()
          } else if (IS_YOU_DO) {
            onClickNext()
          }
        }
        return true
      } else if (key === 110 || key === 190) {
        setInputNumbers(`${inputNumbers}`.concat('.'))
        return true
      } else {
        return false
      }
    }
    return true
  }

  useEffect(() => {
    window.addEventListener('keydown', keyDownHandler)
    return () => {
      window.removeEventListener('keydown', keyDownHandler)
    }
  })

  const [answers, setAnswers] = useState<any>([])

  const onAssessmentClick = () => {
    console.log('queston i just answered: ', question)
    console.log('answers: ', answers)
    const questionTags = [
      question.p_tag,
      question.s_tag1 !== '' ? question.s_tag1 : null,
      question.s_tag2 !== '' ? question.s_tag2 : null,
      question.s_tag3 !== '' ? question.s_tag3 : null,
      question.s_tag4 !== '' ? question.s_tag4 : null,
      question.s_tag5 !== '' ? question.s_tag5 : null,
      question.s_tag6 !== '' ? question.s_tag6 : null,
    ]
    const data = {
      parent_id: account.parentId,
      child_id: account.id,
      question_id: question.question_id,
      question_rating: question.question_rating,
      text_answer: inputNumbers,
      is_correct: question.correct_answer === inputNumbers,
      tag_ids: questionTags,
      domain_code: question.domain_code,
    }
    // setInputNumbers('')
    // setAnswers([...answers, data])
    // if (index === question.length - 1) {
    //   dispatch(handleAssessmentAnswers(data))
    //   setAnswers([])
    // } else {
    //   dispatch(handleAssessmentIdx(index + 1))
    // }
    dispatch(handleAssessmentAnswers(data))
    dispatch(handleAssessmentIdx(index + 1))
    setInputNumbers('')
  }
  return (
    <Content>
      <Layout className="site-layout-background">
        <Content>
          <div className={containerheight}>
            <LoadingOverlay
              active={loading}
              spinner
              text="Loading"
              className="h-screen lesson-activity"
            >
              <div>
                {IS_I_DO && currentLesson && (
                  <div
                    style={{ width: '100%', minHeight: 'calc(100vh - 300px)' }}
                  >
                    <div className={customReactPlayerIDo}>
                      <div className={numberPadHeadingText}>
                        <Heading1 className={padHeadingText}>
                          {currentLesson?.lesson_name}
                        </Heading1>
                      </div>
                      <div className={customReactPlayer}>
                        <ReactPlayer
                          url={currentLesson.i_do_url}
                          width="100%"
                          height="auto"
                          playing={shouldVidPlay}
                          controls={false}
                        />
                      </div>
                    </div>
                    <div className={playBarContainer}>
                      <button
                        onClick={onPlayClick}
                        className={playBarContainerButton}
                      >
                        {' '}
                        {shouldVidPlay === true ? (
                          <PauseCircleFilled
                            className={playBarContainerButtonIcon}
                          />
                        ) : (
                          <PlayCircleFilled
                            className={playBarContainerButtonIcon}
                          />
                        )}
                      </button>
                    </div>

                    <div className={lessonActivityButtonContainer}>
                      <button
                        type="button"
                        className={lessonActivityButton}
                        onClick={on_I_DO_Click}
                      >
                        <img src="/img/child-dashboard/123.png" alt="button" />
                        <span className={lessonActivityButtonText}>Next <Next/></span>
                      </button>
                    </div>
                  </div>
                )}
                {IS_WE_DO && (
                  <>
                    <div>
                      <div
                        style={{
                          width: '100%',
                          minHeight: 'calc(100vh - 300px)',
                        }}
                      >
                        <div className={numberPadBgContainer}>
                          <div className={numberPadHeadingText}>
                            <Heading1 className={padHeadingText}>
                              {currentLesson.we_do_question_text}
                            </Heading1>
                          </div>
                          <div className={customReactPlayer}>
                            <ReactPlayer
                              url={we_dos_array[weDoIndex]}
                              width="100%"
                              height="auto"
                              playing={shouldVidPlay}
                              controls={false}
                            />
                          </div>
                          {isNumpad && (
                            <div className={lessonActivityNumberPad}>
                              <NumberPad
                                onSelectNumber={onSelectNumber}
                                inputNumbers={inputNumbers}
                                onRemove={onRemove}
                              />
                            </div>
                          )}
                          {isMCQS && (
                            <MultipleChoice
                              selectedQuestion={currentLesson}
                              selectedRadio={selectedRadio}
                              inputNumbers={inputNumbers}
                            />
                          )}
                          {isTenFrame && (
                            <div
                              style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                              }}
                            >
                              <div style={{ alignSelf: 'center' }}>
                                {selectedFrame.url && (
                                  <Image
                                    src={selectedFrame.url}
                                    alt=""
                                    width={200}
                                    height={200}
                                  />
                                )}
                              </div>
                              <TensFrame
                                incrementNumber={incrementNumber}
                                decrementNumber={decrementNumber}
                                selectedFrameIndex={selectedFrameIndex}
                                style={valueButton}
                                inputStyle={inputNumberType}
                              />
                            </div>
                          )}
                        </div>
                        <div className={playBarContainerWeDo}>
                          <button
                            onClick={onPlayClick}
                            className={playBarContainerButton}
                          >
                            {' '}
                            {shouldVidPlay === true ? (
                              <PauseCircleFilled
                                className={playBarContainerButtonIcon}
                              />
                            ) : (
                              <PlayCircleFilled
                                className={playBarContainerButtonIcon}
                              />
                            )}
                          </button>
                        </div>
                        <div className={lessonsActivityButtonContainer}>
                          <button
                            className={lessonsActivityButton}
                            // type="primary"
                            // size={'large'}
                            onClick={on_We_DO_Click}
                          >
                            <img
                              src="/img/child-dashboard/123.png"
                              width={250}
                              alt="button"
                            />
                            <span className={lessonsActivityButtonText}>
                              {IS_I_DO ? 'Next' : 'Enter'}
                            </span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </>
                )}
                {IS_YOU_DO && currentLessonQuestions && (
                  <div className={questionOuterContainer}>
                    <div
                      className={`${animationClass} ${questionInnerContainer}`}
                    >
                      <Questions
                        //Num pad
                        onRemove={onRemove}
                        onSelectNumber={onSelectNumber}
                        //Tens Frame
                        selectedFrameIndex={selectedFrameIndex}
                        selectedFrame={selectedFrame}
                        incrementNumber={incrementNumber}
                        decrementNumber={decrementNumber}
                        //MCQS
                        selectedQuestion={currentLessonQuestions}
                        // selectedQuestion={currentLessonQuestions[questionIndex]}
                        onClickNextQuestion={onClickNext}
                        selectedRadio={selectedRadio}
                        inputNumbers={inputNumbers}
                      />
                    </div>
                  </div>
                )}
                {IS_ASSESSMENT && (
                  <div className={questionOuterContainer}>
                    <div
                      className={`${animationClass} ${questionInnerContainer}`}
                    >
                      <Questions
                        //Num pad
                        onRemove={onRemove}
                        onSelectNumber={onSelectNumber}
                        //Tens Frame
                        selectedFrameIndex={selectedFrameIndex}
                        selectedFrame={selectedFrame}
                        incrementNumber={incrementNumber}
                        decrementNumber={decrementNumber}
                        //MCQS
                        selectedQuestion={question}
                        // selectedQuestion={currentLessonQuestions[questionIndex]}
                        // onClickNextQuestion={onClickNext}
                        onAssessmentClick={onAssessmentClick}
                        selectedRadio={selectedRadio}
                        inputNumbers={inputNumbers}
                      />
                    </div>
                  </div>
                )}
              </div>
            </LoadingOverlay>
          </div>
        </Content>
      </Layout>
    </Content>
  )
}

import Image from 'next/dist/client/image'
import { Button, Grid } from 'antd'
import AudioPlayer from 'react-h5-audio-player'
import 'react-h5-audio-player/lib/styles.css'
import styles from './Question.module.scss'
import MultipleChoice from './Questions/MultipleChoice.component'
import TensFrame from './Questions/TensFrames.component'
import NumberPad from './Questions/NumberPad.component'

import { Heading1 } from '../atoms/Text/Text'
import { ReactElement, useEffect, useRef } from 'react'

const { useBreakpoint } = Grid

const Questions = ({
  onRemove,
  onSelectNumber,
  selectedFrameIndex,
  selectedFrame,
  incrementNumber,
  decrementNumber,
  selectedQuestion,
  onClickNextQuestion = null,
  onAssessmentClick = null,
  selectedRadio,
  inputNumbers,
}): ReactElement => {
  const { question_text, illustration_url, question_type, audio_url } =
    selectedQuestion

  const player = useRef()

  useEffect(() => {
    if (player && player.current) {
      // @ts-ignore
      player.current.audio.current.pause()
      // @ts-ignore
      player.current.audio.current.currentTime = 0
      // @ts-ignore
      player.current.audio.current.play()
    }
  }, [question_text])

  let audioPlayerWidth
  const { xs, sm, md, lg, xl } = useBreakpoint()

  if (xs) {
    audioPlayerWidth = '100%'
  } else if (!xs && sm && !md) {
    audioPlayerWidth = '100%'
  } else if (!xs && sm && md) {
    audioPlayerWidth = '100%'
  }

  const {
    questionCotainer,
    mainQuestionContainer,
    questionIframe,
    questionAudioPlayer,
    questionHeadingText,
    headingText,
    questionButtonContainer,
    questionButton,
    questionButtonText,
    questionBgContainer,
  } = styles
  return (
    <>
      <div className={questionBgContainer}>
        <div className={questionHeadingText}>
          <Heading1 className={headingText}>
            {selectedQuestion.question_text}
          </Heading1>
        </div>
        <div className="flex justify-center">
          {illustration_url ? (
            <div className={questionCotainer}>
              <img src={illustration_url} alt="" />
            </div>
          ) : question_type === 'tens frame' ? (
            <div className={questionCotainer}>
              <img src={selectedFrame.url} alt="" />
            </div>
          ) : (
            <></>
          )}
        </div>

        <div className={mainQuestionContainer}>
          <div>
            {selectedQuestion.question_type == 'tens frame' && (
              <TensFrame
                incrementNumber={incrementNumber}
                decrementNumber={decrementNumber}
                selectedFrameIndex={selectedFrameIndex}
                style={{}}
                inputStyle={{}}
              />
            )}
            {selectedQuestion.question_type == 'multiple choice' && (
              <MultipleChoice
                selectedQuestion={selectedQuestion}
                selectedRadio={selectedRadio}
                inputNumbers={inputNumbers}
              />
            )}
            {selectedQuestion.question_type === 'number pad' && (
              <NumberPad
                onSelectNumber={onSelectNumber}
                inputNumbers={inputNumbers}
                onRemove={onRemove}
              />
            )}
          </div>

          <div className={questionAudioPlayer}>
            <div
              className={questionIframe}
              style={audio_url ? { width: audioPlayerWidth } : {}}
            >
              {audio_url && (
                <AudioPlayer
                  ref={player}
                  src={audio_url}
                  autoPlay
                  showJumpControls={false}
                />
              )}
            </div>
          </div>
        </div>
      </div>
      <div className={questionButtonContainer}>
        <button
          className={questionButton}
          type="button"
          disabled={!inputNumbers}
          onClick={
            onClickNextQuestion ? onClickNextQuestion : onAssessmentClick
          }
        >
          <img src="/img/child-dashboard/123.png" alt="button" />
          <span className={questionButtonText}>Enter This Answer</span>
        </button>
      </div>
    </>
  )
}

export default Questions

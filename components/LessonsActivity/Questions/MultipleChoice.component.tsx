import { Radio, RadioChangeEvent } from 'antd'
import { ReactElement } from 'react'
const MultipleChoice = ({
  selectedQuestion,
  selectedRadio,
  inputNumbers,
}): ReactElement => {
  const choice1 =
    selectedQuestion.mc1[0] === '`'
      ? selectedQuestion.mc1[1]
      : selectedQuestion.mc1
  const choice2 =
    selectedQuestion.mc2[0] === '`'
      ? selectedQuestion.mc2[1]
      : selectedQuestion.mc2
  const choice3 =
    selectedQuestion.mc3[0] === '`'
      ? selectedQuestion.mc3[1]
      : selectedQuestion.mc3
  const choice4 =
    selectedQuestion.mc4[0] === '`'
      ? selectedQuestion.mc4[1]
      : selectedQuestion.mc4

  return (
    <div className="lesson-activity__radio">
      <Radio.Group
        onChange={(e: RadioChangeEvent) => selectedRadio(e.target.value)}
        buttonStyle="solid"
        defaultValue={null}
        value={inputNumbers}
      >
        {selectedQuestion.mc1 && (
          <Radio.Button value={selectedQuestion.mc1}>{choice1}</Radio.Button>
        )}
        {selectedQuestion.mc2 && (
          <Radio.Button value={selectedQuestion.mc2}>{choice2}</Radio.Button>
        )}
        {selectedQuestion.mc3 && (
          <Radio.Button value={selectedQuestion.mc3}>{choice3}</Radio.Button>
        )}
        {selectedQuestion.mc4 && (
          <Radio.Button value={selectedQuestion.mc4}>{choice4}</Radio.Button>
        )}
      </Radio.Group>
    </div>
  )
}

export default MultipleChoice

import { ReactElement } from 'react'
interface TensFrameProps {
  incrementNumber: any
  decrementNumber: any
  selectedFrameIndex: any
  // eslint-disable-next-line react/no-unused-prop-types
  style: any
  // eslint-disable-next-line react/no-unused-prop-types
  inputStyle: any
}

const TensFrame = ({
  incrementNumber,
  decrementNumber,
  selectedFrameIndex,
}: TensFrameProps): ReactElement => {
  return (
    <div className="flex justify-center items-center">
      <div
        className="h-10 w-10 flex justify-center items-center text-lg bg-white cursor-pointer"
        id="decrease"
        onClick={decrementNumber}
      >
        -
      </div>
      <div
        className="h-10 w-10 flex justify-center items-center text-lg bg-blue-100"
        id="number"
      >
        {selectedFrameIndex}
      </div>
      {/* <input
        className="h-10 w-10 text-lg"
        // className={inputStyle}
        type="number"
        id="number"
        value={selectedFrameIndex}
        readOnly
      /> */}
      <div
        className="h-10 w-10 flex justify-center items-center text-lg bg-white cursor-pointer"
        id="increase"
        onClick={incrementNumber}
      >
        +
      </div>
    </div>
  )
}

export default TensFrame

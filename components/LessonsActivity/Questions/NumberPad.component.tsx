import { ReactElement } from 'react'
import NumberKeypad from '../../NumberKeypad'

const NumberPad = ({
  onSelectNumber,
  inputNumbers,
  onRemove,
}): ReactElement => {
  return (
    <div className="flex justify-center">
      <div>
        <div className="w-full flex justify-center">
          <NumberKeypad onSelectNumber={onSelectNumber} onRemove={onRemove} />
        </div>
        <div className="flex justify-center">
          <input
            type="text"
            name="code"
            value={inputNumbers}
            maxLength={4}
            className="mt-4 p-2 w-80 h-10 rounded-xl"
            readOnly={true}
          />
        </div>
      </div>
    </div>
  )
}

export default NumberPad

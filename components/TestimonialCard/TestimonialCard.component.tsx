import { Paragraph } from 'components'
import { Heading } from '../atoms'
import styles from './TestimonialCard.module.scss'

export const TestimonialCard = ({ testimonialText, name }) => {
  const {
    testimonial,
    testimonialQuote,
    testimonialParagraph,
    testimonialName,
  } = styles
  return (
    <div className={testimonial}>
      <img
        className={testimonialQuote}
        src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632250849/components/PostQouteCard/quote_pgoigp.png"
        alt="quote"
        width="80"
        height="80"
      />
      <Paragraph className={testimonialParagraph}>{testimonialText}</Paragraph>
      <Heading className={testimonialName}>{name}</Heading>
    </div>
  )
}

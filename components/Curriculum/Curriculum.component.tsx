import { Paragraph } from 'components'
import Link from 'next/link'
import { ReactElement } from 'react'
import styles from './Curriculum.module.scss'

export interface CurriculumProps {
  heading: string
  courseChapters: string[]
  courseLink?: string
  courseLinkText?: string
  examples?: string[]
}

export const Curriculum = ({
  heading,
  courseChapters,
  courseLink,
  courseLinkText,
  examples,
}: CurriculumProps): ReactElement => {
  const { textContainerContent } = styles
  return (
    <Paragraph className={textContainerContent}>
      <>
        <strong style={{ fontSize: '22px' }}>{heading}</strong>
        <div style={{ marginLeft: '14px', marginTop: '10px' }}>
          {Array.isArray ? (
            courseChapters.map((course, index) => <li key={index}>{course}</li>)
          ) : (
            <></>
          )}
          {examples !== undefined && examples.length ? (
            <div style={{ marginLeft: '14px' }}>
              <ul>
                &nbsp;<strong>Examples:</strong>
                {examples.map((example, indx) => (
                  <li key={indx}>{example}</li>
                ))}
              </ul>
            </div>
          ) : (
            <></>
          )}
        </div>
        {courseLink ? (
          <Link href={courseLink}>
            <a>{courseLinkText}</a>
          </Link>
        ) : (
          <></>
        )}
      </>
    </Paragraph>
  )
}

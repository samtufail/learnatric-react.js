import { ReactElement } from 'react'
import { Typography } from 'antd'
import styles from './Header.module.scss'

interface HeaderProps {
  image: string
  heading: string | ReactElement | ReactElement[]
  description: string | ReactElement | ReactElement[]
  curriculum: boolean
}

export const Header = ({
  image,
  heading,
  description,
  curriculum = false,
}: HeaderProps): ReactElement => {
  const {
    header,
    headerContent,
    headerImage,
    headerText,
    headerHeading,
    headerDescription,
  } = styles
  return (
    <div className={header}>
      <div className={headerContent}>
        {!curriculum && (
          <div className={headerImage}>
            <img src={image} alt="header" height="200" width="80%" />
          </div>
        )}
        <div className={headerText}>
          <div className={headerHeading}>
            <Typography.Title level={1}>{heading}</Typography.Title>
          </div>
          <div className={headerDescription}>
            <Typography.Title level={2}>{description}</Typography.Title>
          </div>
        </div>
      </div>
    </div>
  )
}

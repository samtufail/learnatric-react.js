import dynamic from 'next/dynamic'
import { StepPanel as StepPanelImport } from './StepPanel.component'

export const StepPanel = dynamic(
  () =>
    import('./StepPanel.component').then((module) => module.StepPanel) as any
) as typeof StepPanelImport

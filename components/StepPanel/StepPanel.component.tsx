import { Steps } from 'antd'
import { ReactElement } from 'react'

export interface StepPanelProps {
  steps: any
  activeStep: any
}

export const StepPanel = ({
  steps,
  activeStep,
}: StepPanelProps): ReactElement => (
  <>
    {steps.length ? (
      <div className="step-panel__container">
        <Steps current={activeStep} direction="horizontal" responsive={true}>
          {steps.map((item) => (
            <Steps.Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">
          {steps[activeStep] && steps[activeStep].content}
        </div>
      </div>
    ) : null}
  </>
)

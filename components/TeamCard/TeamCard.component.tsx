import { Card, Typography } from 'antd'
import { LinkedInIcon } from 'icons'
import { ReactElement } from 'react'
import {
  cardContainer,
  cardImage,
  cardName,
  cardDesignation,
  cardDescription,
  cardLinkedinIcon,
} from './TeamCard.module.scss'

interface TeamCardProps {
  img: string
  name: string
  designation: string
  description: string
  linkedIn: string
}

export const TeamCard = ({
  img,
  name,
  designation,
  description,
  linkedIn,
}: TeamCardProps): ReactElement => {
  return (
    <div className={cardContainer}>
      <Card
        hoverable
        style={{
          width: '100%',
          minHeight: 610,
          borderRadius: 10,
          cursor: 'unset',
        }}
        cover={
          <img
            className={cardImage}
            alt="team"
            src={img}
            height="330"
            width="330"
          />
        }
      >
        <Typography.Title level={4} className={cardName}>
          {name}
        </Typography.Title>
        <Typography.Title level={5} className={cardDesignation}>
          {designation}
        </Typography.Title>
        <a href={linkedIn} target="_blank" className={cardLinkedinIcon}>
          <LinkedInIcon />
        </a>
        <Typography.Paragraph className={cardDescription}>
          {description}
        </Typography.Paragraph>
      </Card>
    </div>
  )
}

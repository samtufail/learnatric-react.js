import { get } from 'lodash'
import { InputNumber, Typography } from 'antd'
import styles from './styles.module.scss'
import { CommonInputProps } from './types'

export interface CustomInputNumberProps<T> extends CommonInputProps<T> {
  customOnChange?: (value: string) => void
  customError?: string
  isEmail?: boolean
  min: number
  max: number
  // TODO: Add proper types for commented props
  // parser
  // formatter
  defaultValue: number
}

export function CustomInputNumber<T>({
  formik,
  name,
  placeholder,
  min,
  max,
  label,
  // parser,
  // formatter,
  defaultValue,
  customOnChange,
}: CustomInputNumberProps<T>) {
  const {
    customInputContainer,
    customInput,
    customInputNumber,
    errorMessage,
    inputLabel,
    invalidInput,
  } = styles

  const error = get(formik.errors, name)
  const invalid = get(formik.touched, name, false) && !!error

  const className = invalid ? invalidInput : ''

  const onChange = (value) => {
    if (value) {
      const e = { target: { id: name, name, value } }
      formik.handleChange(e)
      if (customOnChange) customOnChange(value)
    }
  }

  return (
    <div className={customInputContainer}>
      {inputLabel && (
        <Typography.Paragraph className={inputLabel}>
          {label}
        </Typography.Paragraph>
      )}
      <InputNumber
        className={`${customInput} ${customInputNumber} ${className}`}
        style={{ width: '100%' }}
        name={name}
        placeholder={placeholder}
        type="number"
        value={get(formik.values, name)}
        onChange={onChange}
        onBlur={(e) => formik.handleBlur(e)}
        max={max}
        min={min}
        // formatter={formatter}
        // parser={parser}
        defaultValue={defaultValue}
      />
      {invalid && <div className={errorMessage}>{error}</div>}
    </div>
  )
}

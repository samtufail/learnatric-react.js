import { get } from 'lodash'
import { Typography } from 'antd'

import MaskedInput from 'antd-mask-input'
import styles from './styles.module.scss'

export function CustomInputPhone({ formik, label, name }) {
  const {
    customInputContainer,
    customInput,
    errorMessage,
    inputLabel,
    invalidInput,
  } = styles

  const error = get(formik.errors, name)
  const invalid = get(formik.touched, name, false) && !!error

  const className = invalid ? invalidInput : ''
  // const dispatch = useDispatch()
  const onChange = (e) => {
    formik.handleChange(e)
  }

  const onFocusChange = (e) => {
    formik.handleBlur(e)
  }

  return (
    <div className={customInputContainer}>
      <Typography.Paragraph className={inputLabel}>
        {label}
      </Typography.Paragraph>
      <MaskedInput
        className={`${customInput} ${className}`}
        mask="111-111-1111"
        name="phoneNumber"
        placeholder="123-456-7890"
        onChange={onChange}
      />
      {invalid && <div className={errorMessage}>{error}</div>}
    </div>
  )
}

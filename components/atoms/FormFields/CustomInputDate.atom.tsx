import { get } from 'lodash'
import { Typography, DatePicker } from 'antd'
import moment from 'moment'
import styles from './styles.module.scss'
import { ReactElement } from 'react'

export interface CustomInputDateProps {
  formik: any
  name: string
  disablePast?: boolean
  disableFuture?: boolean
  customError?: string
  showToday?: boolean
  showTime?: boolean
  label: string
  customDateChange?: any
  placeholder?: string
  className: any
}

export const CustomInputDate = ({
  formik,
  name,
  disablePast = false,
  disableFuture = true,
  customError = '',
  showToday = false,
  showTime = false,
  label = '',
  customDateChange,
  placeholder = 'MM-DD-YYYY',
}: CustomInputDateProps): ReactElement => {
  const {
    customInputContainer,
    customInput,
    errorMessage,
    inputLabel,
    invalidInput,
  } = styles
  const error = get(formik.errors, name, customError)
  const invalid = get(formik.touched, name, false) && !!error
  const className = invalid ? invalidInput : ''

  function onChange(momentObj) {
    const event = {
      target: { value: momentObj, name, id: name },
    }
    formik.handleChange(event)
    if (customDateChange) customDateChange(event)
  }

  return (
    <div className={customInputContainer}>
      <Typography.Paragraph className={inputLabel}>
        {label}
      </Typography.Paragraph>
      <DatePicker
        className={`${customInput} ${className}`}
        format="MM-DD-YYYY"
        name={name}
        placeholder={placeholder}
        showToday={showToday}
        showTime={showTime}
        value={get(formik.values, name)}
        onBlur={(e) => formik.handleBlur(e)}
        onChange={(momentObj) => {
          onChange(momentObj)
        }}
        disabledDate={(current) => {
          if (disablePast) {
            return current < moment().subtract(1, 'd').endOf('day')
          }
          if (disableFuture) {
            return current > moment().endOf('day')
          }
          return false
        }}
      />
      {invalid && <div className={errorMessage}>{error}</div>}
    </div>
  )
}

import dynamic from 'next/dynamic'
import { CustomInput as CustomInputImp } from './CustomInput.atom'
import { AvatarSelector as AvatarSelectorImp } from './AvatarSelector.atom'
import { CustomInputDate as CustomInputDateImp } from './CustomInputDate.atom'
import { CustomSelect as CustomSelectImp } from './CustomSelect.atom'
import { CustomInputNumber as CustomInputNumberImp } from './CustomInputNumber.atom'
import { PickChildToLoginModal as PickChildToLoginModalImp } from './PickChildToLoginModal.atom'
import { CustomInputPhone as CustomInputPhoneImp } from './CustomInputPhone.atom'
import { CustomPhoneSelect as CustomPhoneSelectImp } from './CustomPhoneSelect'

export const CustomInputPhone = dynamic(() =>
  import('./CustomInputPhone.atom').then(
    (module) => module.CustomInputPhone as any
  )
) as typeof CustomInputPhoneImp

export const CustomInput = dynamic(() =>
  import('./CustomInput.atom').then((module) => module.CustomInput as any)
) as typeof CustomInputImp

export const AvatarSelector = dynamic(
  () =>
    import('./AvatarSelector.atom').then(
      (module) => module.AvatarSelector
    ) as any
) as typeof AvatarSelectorImp

export const CustomInputNumber = dynamic(
  () =>
    import('./CustomInputNumber.atom').then(
      (module) => module.CustomInputNumber
    ) as any
) as typeof CustomInputNumberImp

export const CustomInputDate = dynamic(
  () =>
    import('./CustomInputDate.atom').then(
      (module) => module.CustomInputDate
    ) as any
) as typeof CustomInputDateImp

export const CustomSelect = dynamic(
  () =>
    import('./CustomSelect.atom').then((module) => module.CustomSelect) as any
) as typeof CustomSelectImp

export const PickChildToLoginModal = dynamic(
  () =>
    import('./PickChildToLoginModal.atom').then(
      (module) => module.PickChildToLoginModal
    ) as any
) as typeof PickChildToLoginModalImp

export const CustomPhoneSelect = dynamic(
  () =>
    import('./CustomPhoneSelect').then(
      (module) => module.CustomPhoneSelect
    ) as any
) as typeof CustomPhoneSelectImp

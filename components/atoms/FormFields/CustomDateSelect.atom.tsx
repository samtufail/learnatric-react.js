import { Typography } from 'antd'
import { ReactElement } from 'react'
import IntlTelInput from 'react-intl-tel-input'
import 'react-intl-tel-input/dist/main.css'
import styles from './styles.module.scss'
import { useFormik } from 'formik'

export const CustomDateSelect = ({ label }): ReactElement => {
  // const formik = useFormik({
  //   initialValues:{
  //     phoneNumber: ""
  //   },
  // })
  // const onChange = (e) => {
  //   formik.handleChange(e)
  // }
  // const onFocusChange = (e) => {
  //   formik.handleBlur(e)
  // }
  const { customInputContainer, inputLabel, customInput } = styles
  return (
    <div className={customInputContainer}>
      <Typography.Paragraph className={inputLabel}>
        {label}
      </Typography.Paragraph>
      <IntlTelInput
        fieldName="phoneNumber"
        preferredCountries={['us']}
        // onPhoneNumberChange={onChange}
        // onPhoneNumberBlur={onFocusChange}
      />
    </div>
  )
}

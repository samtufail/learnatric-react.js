import { Typography } from 'antd'
import { get } from 'lodash'
import { CheckCircleOutlined } from '@ant-design/icons'
import {
  avatar_selector,
  avatar_selector__heading,
  avatars_holder,
  avatar_container,
  avatar_container_active,
  avatar_check_icon,
  errorMessage,
} from './AvatarSelector.module.scss'
import { ReactElement } from 'react'

const avatars = [
  {
    id: 1,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249279/components/FormFields/Avatars/avatars-full/1_gyjaog.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249726/components/FormFields/Avatars/avatars-small/1_vevlay.png',
  },
  {
    id: 2,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249279/components/FormFields/Avatars/avatars-full/2_lhtmix.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249725/components/FormFields/Avatars/avatars-small/2_ywah8i.png',
  },
  {
    id: 3,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249279/components/FormFields/Avatars/avatars-full/3_hnvr33.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249724/components/FormFields/Avatars/avatars-small/3_gptflj.png',
  },
  {
    id: 4,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249280/components/FormFields/Avatars/avatars-full/4_byodw3.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249725/components/FormFields/Avatars/avatars-small/4_flnueq.png',
  },
  {
    id: 5,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249264/components/FormFields/Avatars/avatars-full/5_mklj43.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249713/components/FormFields/Avatars/avatars-small/5_k3ygfk.png',
  },
  {
    id: 6,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249265/components/FormFields/Avatars/avatars-full/6_mowyo4.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249729/components/FormFields/Avatars/avatars-small/6_kzlxi7.png',
  },
  {
    id: 7,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249265/components/FormFields/Avatars/avatars-full/7_arbnij.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249717/components/FormFields/Avatars/avatars-small/7_qb3jwq.png',
  },
  {
    id: 8,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249265/components/FormFields/Avatars/avatars-full/8_q0sqox.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249715/components/FormFields/Avatars/avatars-small/8_ttkf2j.png',
  },
  {
    id: 9,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249266/components/FormFields/Avatars/avatars-full/9_z9birv.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249719/components/FormFields/Avatars/avatars-small/9_drxtcb.png',
  },
  {
    id: 10,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249268/components/FormFields/Avatars/avatars-full/10_zs0gyq.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249716/components/FormFields/Avatars/avatars-small/10_t3gxk5.png',
  },
  {
    id: 11,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249268/components/FormFields/Avatars/avatars-full/11_i32dmf.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249715/components/FormFields/Avatars/avatars-small/11_kygb3z.png',
  },
  {
    id: 12,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249269/components/FormFields/Avatars/avatars-full/12_h4k7u8.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249719/components/FormFields/Avatars/avatars-small/12_kejnxu.png',
  },
  {
    id: 13,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249272/components/FormFields/Avatars/avatars-full/13_fll3gh.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249718/components/FormFields/Avatars/avatars-small/13_kcit7d.png',
  },
  {
    id: 14,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249272/components/FormFields/Avatars/avatars-full/14_y7wkju.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249721/components/FormFields/Avatars/avatars-small/14_iyylmt.png',
  },
  {
    id: 15,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249271/components/FormFields/Avatars/avatars-full/15_qg5v8i.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249721/components/FormFields/Avatars/avatars-small/15_ovfqnw.png',
  },
  {
    id: 16,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249275/components/FormFields/Avatars/avatars-full/16_txlkte.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249720/components/FormFields/Avatars/avatars-small/16_ejxgnx.png',
  },
  {
    id: 17,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249276/components/FormFields/Avatars/avatars-full/17_dq3j3y.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249725/components/FormFields/Avatars/avatars-small/17_j99gqg.png',
  },
  {
    id: 18,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249279/components/FormFields/Avatars/avatars-full/18_f8mcm0.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249721/components/FormFields/Avatars/avatars-small/18_u6tozf.png',
  },
  {
    id: 19,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249275/components/FormFields/Avatars/avatars-full/19_kvhdwt.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249722/components/FormFields/Avatars/avatars-small/19_godlw4.png',
  },
  {
    id: 20,
    full: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249279/components/FormFields/Avatars/avatars-full/20_bloynd.png',
    small:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632249725/components/FormFields/Avatars/avatars-small/20_xbiiae.png',
  },
]

export const AvatarSelector = ({ formik, customError = '' }): ReactElement => {
  const avatar = get(formik.values, 'avatarSmall')

  const error = get(formik.errors, 'avatarSmall', customError)

  const invalid = get(formik.touched, 'avatarSmall', false) && !!error

  return (
    <div className={avatar_selector}>
      <Typography.Title level={4} className={avatar_selector__heading}>
        Select Avatar *
      </Typography.Title>
      <div className={avatars_holder}>
        {avatars.map(({ small, full, id }) => {
          let active
          if (formik) {
            active = small === avatar
          }
          return (
            <button
              key={id}
              className={`${avatar_container} ${
                active ? avatar_container_active : ''
              }`}
              type="button"
              onClick={() => {
                formik.setFieldValue('avatarSmall', small)
                formik.setFieldValue('avatarLarge', full)
              }}
              style={{
                backgroundImage: `url(${small})`,
              }}
            >
              {active && (
                <div className={avatar_check_icon}>
                  <CheckCircleOutlined
                    style={{ fontSize: 24, color: 'white' }}
                  />
                </div>
              )}
            </button>
          )
        })}
      </div>
      {invalid && <div className={errorMessage}>{error}</div>}
    </div>
  )
}

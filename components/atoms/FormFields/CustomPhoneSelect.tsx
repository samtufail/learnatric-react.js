import { Typography } from 'antd'
import { get } from 'lodash'
import { ReactElement, useState } from 'react'
import IntlTelInput from 'react-intl-tel-input'
import 'react-intl-tel-input/dist/main.css'
import styles from './styles.module.scss'
import { CommonInputProps } from './types'

export interface CustomInputProps<T> extends CommonInputProps<T> {
  customOnChange?: (value: string) => void
  customOnBlur?: (value: string) => void
  customError?: string
  isEmail?: boolean
}

export function CustomPhoneSelect<T>({
  label,
  formik,
  name,
  customError = '',
}: CustomInputProps<T>){
  const [invalid, setInvalid] = useState(false)

  const { customInputContainer, inputLabel, errorMessage } = styles
  const error = get(formik.errors, name, customError)
  return (
    <div className={customInputContainer}>
      <Typography.Paragraph className={inputLabel}>
        {label}
      </Typography.Paragraph>
      <IntlTelInput
        fieldName="phoneNumber"
        preferredCountries={['us']}
        onPhoneNumberChange={(isValid, number, country) => {
          const fullPhone = `+${country.dialCode}${number}`
          if (number === '') {
            setInvalid(false)
          } else if (!isValid) {
            setInvalid(true)
          } else if (isValid && number !== '') {
            setInvalid(false)
            formik?.setFieldValue(name, fullPhone)
          }
        }}
      />
      {invalid && (
        <div className={errorMessage}>Please enter a valid Phone Number</div>
      )}
    </div>
  )
}

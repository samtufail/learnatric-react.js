import { get } from 'lodash'
import { ReactElement, ReactNode } from 'react'
import { Input, Typography } from 'antd'
import styles from './styles.module.scss'
import { CommonInputProps } from './types'

export interface CustomInputProps<T> extends CommonInputProps<T> {
  customOnChange?: (value: string) => void
  customOnBlur?: (value: string) => void
  prefix?: ReactElement | ReactNode
  customError?: string
  isEmail?: boolean
}

export function CustomInput<T>({
  formik,
  name,
  placeholder,
  prefix,
  customOnBlur,
  customOnChange,
  customError = '',
  type = 'text',
  label = '',
  isEmail,
}: CustomInputProps<T>) {
  const {
    customInputContainer,
    customInput,
    errorMessage,
    inputLabel,
    invalidInput,
  } = styles

  const error = get(formik.errors, name, customError)
  const invalid = get(formik.touched, name, false) && !!error

  const className = invalid ? invalidInput : ''
  // const dispatch = useDispatch()
  const onChange = (e) => {
    formik.handleChange(e)
    if (customOnChange) customOnChange(e.target.value)

    if (isEmail) {
      // dispatch(emailChange())
    }
  }

  const onFocusChange = (e) => {
    formik.handleBlur(e)
    if (customOnBlur) customOnBlur(e.target.value)
  }

  return (
    <div className={customInputContainer}>
      <Typography.Paragraph className={inputLabel}>
        {label}
      </Typography.Paragraph>
      <Input
        className={`${customInput} ${className}`}
        name={name}
        placeholder={placeholder}
        type={type}
        value={get(formik.values, name)}
        onChange={onChange}
        onBlur={onFocusChange}
        prefix={prefix}
      />
      {invalid && <div className={errorMessage}>{error}</div>}
    </div>
  )
}

import { ReactNode } from 'react'
import { FormikProps } from 'formik'

export interface CommonInputProps<T> {
  formik: FormikProps<T>
  name: string
  placeholder?: string
  label?: ReactNode
  narrow?: boolean
  className?: string | ''
  type?: 'number' | 'password' | 'text' | 'textarea' | 'email'
  disabled?: boolean
}

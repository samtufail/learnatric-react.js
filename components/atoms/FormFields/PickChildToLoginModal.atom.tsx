import { Modal, Typography } from 'antd'
import { ReactElement } from 'react'
import Router from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
// import { userToLogin } from 'store/actions'
import { avatar_selector__heading } from './AvatarSelector.module.scss'

export interface PickChildToLoginModalProps {
  visible: boolean
  setVisible: any
}

export const PickChildToLoginModal = ({
  visible,
  setVisible,
}: PickChildToLoginModalProps): ReactElement => {
  const dispatch = useDispatch()
  // const { childrens_profiles } = useSelector(({ user }: AppState) => user)

  const setChildNameForLogin = (un) => {
    // dispatch(userToLogin(un.firstName))
    Router.push('/login')
  }
  return (
    <Modal
      title="Pick Which User To Login"
      centered
      visible={visible}
      closable={false}
      onOk={() => setVisible(false)}
      onCancel={() => setVisible(false)}
      width={1000}
      footer={null}
      bodyStyle={{ height: '450px' }}
    >
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-evenly',
          marginLeft: 'auto',
          marginRight: 'auto',
        }}
      >
        {/* {childrens_profiles.map((profile) => ( */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            width: '156px',
            // ':hover': { cursor: 'pointer' },
          }}
          // onClick={() => setChildNameForLogin(profile)}
        >
          <Typography.Title
            level={4}
            className={avatar_selector__heading}
            style={{ alignSelf: 'center' }}
          >
            {/* {profile.username} */}
          </Typography.Title>
          {/* <img
              style={{ height: '300px', width: '150px' }}
              key={profile.id}
              src={profile.avatar_large}
              alt=""
            /> */}
        </div>
        {/* ))} */}
      </div>
    </Modal>
  )
}

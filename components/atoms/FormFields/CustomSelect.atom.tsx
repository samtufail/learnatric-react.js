import { get } from 'lodash'
import { Select, Typography } from 'antd'
import styles from './styles.module.scss'
import { ReactElement } from 'react'

const { Option } = Select

export interface CustomSelectProps {
  formik: any
  name: string
  customError?: string
  label: string
  options: any
  placeholder?: any
}

export function CustomSelect({
  formik,
  name,
  customError = '',
  label = '',
  options,
  placeholder,
}: CustomSelectProps): ReactElement {
  const {
    customInputContainer,
    inputLabel,
    errorMessage,
    invalidInput,
    customSelectInput,
  } = styles
  const error = get(formik.errors, name, customError)
  const invalid = get(formik.touched, name, false) && !!error

  const className = invalid ? invalidInput : ''

  const handleChange = (e: any): void => {
    const event = { target: { value: e, name } }
    formik.handleChange(event)
  }

  return (
    <div className={customInputContainer}>
      <Typography.Paragraph className={inputLabel}>
        {label}
      </Typography.Paragraph>
      <Select
        placeholder={placeholder}
        style={{
          display: 'flex',
          borderRadius: '8px',
          border: '1px solid',
          borderColor: '#999999',
          height: '42px',
        }}
        className={`${className} ${customSelectInput}`}
        onChange={handleChange}
        defaultValue={options[0].value}
        value={get(formik.values, name)}
        bordered={false}
        allowClear={true}
        dropdownClassName="customInput"
      >
        {options.map(({ value, title }) => (
          <Option key={value} value={value}>
            {title}
          </Option>
        ))}
      </Select>
      {invalid && <div className={errorMessage}>{error}</div>}
    </div>
  )
}

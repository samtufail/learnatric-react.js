import React from 'react'

import classes from './text.module.css'

// HEADINGS

export const textStyles = {
  heading1: {
    fontSize: 34,
  },
  heading2: {
    fontSize: 28,
  },
  heading3: {
    fontSize: 22,
  },
  heading4: {
    fontSize: 20,
  },
  heading5: {
    fontSize: 16,
  },
  heading6: {
    fontSize: 14,
  },
  body20: {
    fontSize: 20,
  },
  body20Bold: {
    fontSize: 20,
    lineHeight: 1,
  },
  body16: {
    fontSize: 16,
  },
  body16Bold: {
    fontSize: 16,
  },
  body17: {
    fontSize: 17,
    margin: 0,
  },
  body17Bold: {
    fontSize: 17,
  },
  body15: {
    fontSize: 15,
  },
  body15Bold: {
    fontSize: 15,
  },
  body14: {
    fontSize: 14,
  },
  body11: {
    fontSize: 11,
  },
  label50: {
    fontSize: 50,
  },
  label22: {
    fontSize: 22,
  },

  label19Bold: {
    fontSize: 19,
  },
  label17Bold: {
    fontSize: 17,
  },
  label15Bold: {
    fontSize: 15,
  },
  label15: {
    fontSize: 15,
  },
  label13Light: {
    fontSize: 13,
  },
  label12Light: {
    fontSize: 12,
  },
  label12: {
    fontSize: 12,
  },
  label11Light: {
    fontSize: 11,
  },
  label11: {
    fontSize: 11,
  },
  label10Light: {
    fontSize: 10,
  },
}

export function Heading1({ className, children }) {
  return <h1 className={[classes.heading1, className].join(' ')}>{children}</h1>
}

export function Heading2({ className, children, ...rest }) {
  return (
    <h2 {...rest} className={[classes.heading2, className].join(' ')}>
      {children}
    </h2>
  )
}

export function Heading3({ className, children, ...rest }) {
  return (
    <h3 {...rest} className={[classes.heading3, className].join(' ')}>
      {children}
    </h3>
  )
}

export function Heading4({ className, children, ...rest }) {
  return (
    <h4 {...rest} className={[classes.heading4, className].join(' ')}>
      {children}
    </h4>
  )
}

export function Heading5({ className, children, style, onClick }) {
  return (
    <h4
      onClick={onClick}
      style={style}
      className={[classes.heading5, className].join(' ')}
    >
      {children}
    </h4>
  )
}

export function Heading6({ className, ...rest }) {
  return <h6 {...rest} className={[classes.heading6, className]} />
}
// BODY

export function Body20({ style, ...rest }) {
  return <p {...rest} style={{ ...textStyles.body20, ...style }} />
}

export function Body20Bold({ style = {}, ...rest }) {
  return <p {...rest} style={{ ...textStyles.body20Bold, ...style }} />
}

export function Body17({ style, ...rest }) {
  return <p {...rest} style={{ ...textStyles.body17, ...style }} />
}

export function Body17Bold({ style, ...rest }) {
  return <p {...rest} style={[textStyles.body17Bold, style]} />
}
export function Body16({ className, children, ...rest }) {
  return (
    <p {...rest} className={[classes.body16, className].join(' ')}>
      {children}
    </p>
  )
}

export function Body16Bold({ style, ...rest }) {
  return <p {...rest} style={[textStyles.body16Bold, style]} />
}
export function Body15({ className, children }) {
  return <p className={[classes.body15, className].join(' ')}>{children}</p>
}

export function Body15Bold({ className, children, ...rest }) {
  return (
    <p {...rest} className={[classes.body15Bold, className].join(' ')}>
      {children}
    </p>
  )
}
export function Body14({ style, className, children }) {
  return (
    <p style={style} className={[classes.body14, className].join(' ')}>
      {children}
    </p>
  )
}
export function Body13({ style, className, children, ...rest }) {
  return (
    <p
      {...rest}
      style={style}
      className={[classes.body13, className].join(' ')}
    >
      {children}
    </p>
  )
}

export function Body11({ style, ...rest }) {
  return <p {...rest} style={[textStyles.body11, style]} />
}

// LABEL

export function Label50({ style, ...rest }) {
  return <span {...rest} style={[textStyles.label50, style]} />
}
export function Label22({ className, ...rest }) {
  return <span {...rest} className={[classes.label22, className]} />
}
export function Label19({ className, children, ...rest }) {
  return (
    <span {...rest} className={[classes.label19, className].join(' ')}>
      {children}
    </span>
  )
}
export function Label19Bold({ className, children, ...rest }) {
  return (
    <span {...rest} className={[classes.label19Bold, className].join(' ')}>
      {children}
    </span>
  )
}
export function Label17({ children, className, style }) {
  return (
    <span style={style} className={[classes.label17, className].join(' ')}>
      {children}
    </span>
  )
}
export function Label17Bold({ className, ...rest }) {
  return (
    <span {...rest} className={[classes.label17Bold, className].join(' ')} />
  )
}
export function Label15Bold({ className, ...rest }) {
  return (
    <span {...rest} className={[classes.label15Bold, className].join(' ')} />
  )
}
export function Label15({ className, children, ...rest }) {
  return (
    <span {...rest} className={[classes.label15, className].join(' ')}>
      {children}
    </span>
  )
}
export function Label15Light({ className, children }) {
  return (
    <span className={[classes.label15Light, className].join(' ')}>
      {children}
    </span>
  )
}
export function Label14({ style, className, children, onClick }) {
  return (
    <span
      style={style}
      onClick={onClick}
      className={[classes.label14, className].join(' ')}
    >
      {children}
    </span>
  )
}
export function Label14Bold({ style, className, children }) {
  return (
    <span className={[classes.label14Bold, className].join(' ')} style={style}>
      {children}
    </span>
  )
}
export function Label14Light({ className, children, style }) {
  return (
    <span style={style} className={[classes.label14Light, className].join(' ')}>
      {children}
    </span>
  )
}
export function Label13({ className, children }) {
  return (
    <span className={[classes.label13, className].join(' ')}>{children}</span>
  )
}
export function Label13Light({ style, ...rest }) {
  return <span {...rest} style={[textStyles.label13Light, style]} />
}
export function Label12({ style, ...rest }) {
  return <span {...rest} style={[textStyles.label12, style]} />
}
export function Label12Light({ style, ...rest }) {
  return <span {...rest} style={[textStyles.label12Light, style]} />
}
export function Label11({ style, ...rest }) {
  return <span {...rest} style={[textStyles.label11, style]} />
}
export function Label11Light({ style, ...rest }) {
  return <span {...rest} style={[textStyles.label11Light, style]} />
}
export function Label10({ className, children }) {
  return (
    <span className={[classes.label10, className].join(' ')}>{children}</span>
  )
}
export function Label10Light({ style, ...rest }) {
  return <span {...rest} style={[textStyles.label10Light, style]} />
}

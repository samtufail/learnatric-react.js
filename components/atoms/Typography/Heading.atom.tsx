import { Typography as AntdTypography } from 'antd'
import { ReactElement } from 'react'

export interface HeadingProps {
  children: any
  level?: number | any
  className?: string
}

export const Heading = ({
  children,
  level = 3,
  className = '',
}: HeadingProps): ReactElement => (
  <AntdTypography.Title level={level} className={className}>
    {children}
  </AntdTypography.Title>
)

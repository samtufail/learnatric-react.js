import dynamic from 'next/dynamic'
import { Heading as HeadingImport } from './Heading.atom'
import { Paragraph as ParagraphImport } from './Paragraph.atom'

export const Heading = dynamic(
  () => import('./Heading.atom').then((module) => module.Heading) as any
) as typeof HeadingImport
export const Paragraph = dynamic(
  () => import('./Paragraph.atom').then((module) => module.Paragraph) as any
) as typeof ParagraphImport

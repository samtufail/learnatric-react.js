import { Typography as AntdTypography } from 'antd'
import { ReactElement } from 'react'

export interface ParagraphProps {
  children: any
  className?: string
}

export const Paragraph = ({
  children,
  className = '',
}: ParagraphProps): ReactElement => (
  <AntdTypography.Paragraph className={className}>
    {children}
  </AntdTypography.Paragraph>
)

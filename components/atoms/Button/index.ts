import dynamic from 'next/dynamic'
import { Button as AppButton } from './Button.atom'

export const Button = dynamic(
  () => import('./Button.atom').then((module) => module.Button) as any
) as typeof AppButton

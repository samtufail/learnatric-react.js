import { ReactElement } from 'react'
import styles from './Button.module.scss'

export interface ButtonProps {
  children: any
  type: any
  className: string
  cameFromDash?: boolean
}
export const Button = ({
  children,
  type = 'button',
  className = '',
  cameFromDash = false,
}: ButtonProps): ReactElement => {
  const buttonClass = cameFromDash
    ? styles.childDashboard
    : styles.buttonContainer
  return (
    <div className={buttonClass}>
      <button type={type} className={className}>
        {children}
      </button>
    </div>
  )
}

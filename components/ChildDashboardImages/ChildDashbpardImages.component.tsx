import { ReactElement } from 'react'
import styles from './ChildDashboardImages.module.scss'

export const ChildDashboardImages = ({ toys }): ReactElement => {
  const {
    childDashboardBoyToySoldier,
    childDashboardBoyToySpy,
    childDashboardBoyToyBall,
    childDashboardBoyToyMonkey,
    childDashboardBoyToyBlocks,
    childDashboardBoyToyCounter,
    childDashboardBoyToyFootBall,
    childDashboardBoyToyTrain,
    childDashboardBoyToyTank,
    childDashboardBoyToyCar,
    childDashboardBoyToyGame,
    childDashboardBoyToySpaceShip,
    childDashboardBoyToyBoat,
    childDashboardBoyToyBucket,
    childDashboardBoyToyRocket,
    childDashboardBoyToyRobot,
    childDashboardBoyToyPuppy,
    childDashboardBoyToyPlane,
    childDashboardBoyToyHelicopter,
    childDashboardBoyToyApatosaurus,
    childDashboardGirlToyBlocks,
    childDashboardGirlToyBoat,
    childDashboardGirlToyBaloon,
    childDashboardGirlToyButterfly,
    childDashboardGirlToyCar,
    childDashboardGirlToyCounter,
    childDashboardGirlToyMonkey,
    childDashboardGirlToyBear,
    childDashboardGirlToyElephant,
    childDashboardGirlToyBucket,
    childDashboardGirlToyPig,
    childDashboardGirlToyBall,
    childDashboardGirlToyScooty,
    childDashboardGirlToySingleCar,
    childDashboardGirlToySpinosaurus,
    childDashboardGirlToySquirrel,
    childDashboardGirlToyDoll,
    childDashboardGirlToyGame,
    childDashboardGirlToyTrain,
    childDashboardGirlToyTruck,
  } = styles

  return (
    <div>
      {toys?.armySoldier ? (
        <div className={childDashboardBoyToySoldier}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948337/ChildDashboard_Toys/Boy_Toys/army-soldier_eynvsl.png"
            alt="army-soldier"
            width="50px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys?.armySpy ? (
        <div className={childDashboardBoyToySpy}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948416/ChildDashboard_Toys/Boy_Toys/army-spy_lk33el.png"
            alt="army-spy"
            width="50px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.baskitBall ? (
        <div className={childDashboardBoyToyBall}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948429/ChildDashboard_Toys/Boy_Toys/ball_b9ls8b.png"
            alt="ball"
            width="50px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.bannasMonkey ? (
        <div className={childDashboardBoyToyMonkey}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948437/ChildDashboard_Toys/Boy_Toys/bannas-monkey_tjigd4.png"
            alt="monkey"
            width="60px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.blocks ? (
        <div className={childDashboardBoyToyBlocks}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948442/ChildDashboard_Toys/Boy_Toys/blocks_h7al8o.png"
            alt="blocks"
            width="110px"
            height="100px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.counter ? (
        <div className={childDashboardBoyToyCounter}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948458/ChildDashboard_Toys/Boy_Toys/counter_jcpite.png"
            alt="counter"
            width="120px"
            height="110px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.footBall ? (
        <div className={childDashboardBoyToyFootBall}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948458/ChildDashboard_Toys/Boy_Toys/foot-ball_fdwyff.png"
            alt="foot ball"
            width="80px"
            height="80px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.toyTrain ? (
        <div className={childDashboardBoyToyTrain}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948557/ChildDashboard_Toys/Boy_Toys/toy-train_rgqmx5.png"
            alt="toy train"
            width="150px"
            height="120px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.tank ? (
        <div className={childDashboardBoyToyTank}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948557/ChildDashboard_Toys/Boy_Toys/tank_aw0cgb.png"
            alt="tank"
            width="100px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.sportsCar ? (
        <div className={childDashboardBoyToyCar}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948556/ChildDashboard_Toys/Boy_Toys/sports-car-purple_obs7em.png"
            alt="sports car"
            width="120px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.toyGame ? (
        <div className={childDashboardBoyToyGame}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948557/ChildDashboard_Toys/Boy_Toys/toy-game_pz1enk.png"
            alt="toy game"
            width="60px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.spaceShip ? (
        <div className={childDashboardBoyToySpaceShip}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948556/ChildDashboard_Toys/Boy_Toys/space-ship_xjk5dn.png"
            alt="space ship"
            width="80px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.boat ? (
        <div className={childDashboardBoyToyBoat}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948457/ChildDashboard_Toys/Boy_Toys/boat_z4tcm6.png"
            alt="boat"
            width="80px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.mudBucket ? (
        <div className={childDashboardBoyToyBucket}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948478/ChildDashboard_Toys/Boy_Toys/mud-bucket_gxo704.png"
            alt="mud bucket"
            width="80px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.scpaceRocket ? (
        <div className={childDashboardBoyToyRocket}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948480/ChildDashboard_Toys/Boy_Toys/space-rocket_ouzyud.png"
            alt="space rocket"
            width="50px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.robot ? (
        <div className={childDashboardBoyToyRobot}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948478/ChildDashboard_Toys/Boy_Toys/robot_y1xt08.png"
            alt="robot"
            width="70px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.puppy ? (
        <div className={childDashboardBoyToyPuppy}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948478/ChildDashboard_Toys/Boy_Toys/puppy_czsgkc.png"
            alt="puppy"
            width="70px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.plane ? (
        <div className={childDashboardBoyToyPlane}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948478/ChildDashboard_Toys/Boy_Toys/plane_i5l8cg.png"
            alt="plane"
            width="70px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.helicopter ? (
        <div className={childDashboardBoyToyHelicopter}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948458/ChildDashboard_Toys/Boy_Toys/helicopter_mpjs4h.png"
            alt="helicopter"
            width="80px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.apatosaurus ? (
        <div className={childDashboardBoyToyApatosaurus}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948250/ChildDashboard_Toys/Boy_Toys/apatosaurus_ukwdry.png"
            alt="Apatosaurus"
            width="80px"
            height="50px"
          />
        </div>
      ) : (
        <></>
      )}

      {toys.girlBlocks ? (
        <div className={childDashboardGirlToyBlocks}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948947/ChildDashboard_Toys/Girl_Toys/blocks_zbkqhw.png"
            alt="blocks"
            width="110px"
            height="100px"
          />
        </div>
      ) : (
        <></>
      )}

      {toys.girlBoat ? (
        <div className={childDashboardGirlToyBoat}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948947/ChildDashboard_Toys/Girl_Toys/boat_xa60fb.png"
            alt="boat"
            width="80px"
            height="80px"
          />
        </div>
      ) : (
        <></>
      )}

      {toys.baloon ? (
        <div className={childDashboardGirlToyBaloon}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948947/ChildDashboard_Toys/Girl_Toys/baloon_qamnw6.png"
            alt="baloon"
            width="80px"
            height="80px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.butterfly ? (
        <div className={childDashboardGirlToyButterfly}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949029/ChildDashboard_Toys/Girl_Toys/butterfly_ex5iwq.png"
            alt="butterfly"
            width="80px"
            height="80px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.car ? (
        <div className={childDashboardGirlToyCar}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949030/ChildDashboard_Toys/Girl_Toys/car_fia3fe.png"
            alt="Car"
            width="100px"
            height="80px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.girlcounter ? (
        <div className={childDashboardGirlToyCounter}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949030/ChildDashboard_Toys/Girl_Toys/counter_i3rwmd.png"
            alt="Counter"
            width="120px"
            height="110px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.monkey ? (
        <div className={childDashboardGirlToyMonkey}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/monkey_d3ibcl.png"
            alt="monkey"
            width="80px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.bear ? (
        <div className={childDashboardGirlToyBear}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948949/ChildDashboard_Toys/Girl_Toys/bear_up5cjf.png"
            alt="bear"
            width="80px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.elephant ? (
        <div className={childDashboardGirlToyElephant}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/elephant_ds46md.png"
            alt="elephant"
            width="80px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.bucket ? (
        <div className={childDashboardGirlToyBucket}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949029/ChildDashboard_Toys/Girl_Toys/bucket_drxei9.png"
            alt="mud bucket"
            width="80px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.pig ? (
        <div className={childDashboardGirlToyPig}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/pig_f17mys.png"
            alt="pig"
            width="90px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.ball ? (
        <div className={childDashboardGirlToyBall}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948947/ChildDashboard_Toys/Girl_Toys/ball_bl28gk.png"
            alt="ball"
            width="70px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.scooty ? (
        <div className={childDashboardGirlToyScooty}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/scooty_uucccd.png"
            alt="Scooty"
            width="100px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.singleSeatCar ? (
        <div className={childDashboardGirlToySingleCar}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949068/ChildDashboard_Toys/Girl_Toys/single-seat-car_dli5v4.png"
            alt="single seat car"
            width="80px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.spinosaurus ? (
        <div className={childDashboardGirlToySpinosaurus}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949068/ChildDashboard_Toys/Girl_Toys/spinosaurus_peismk.png"
            alt="Spinosaurus"
            width="80px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.squirrel ? (
        <div className={childDashboardGirlToySquirrel}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949070/ChildDashboard_Toys/Girl_Toys/squirrel_b3pyax.png"
            alt="squirrel"
            width="80px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.doll ? (
        <div className={childDashboardGirlToyDoll}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949029/ChildDashboard_Toys/Girl_Toys/doll_zlkcyk.png"
            alt="doll"
            width="80px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.game ? (
        <div className={childDashboardGirlToyGame}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/game_fnwium.png"
            alt="game"
            width="60px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.toyTrain ? (
        <div className={childDashboardGirlToyTrain}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949068/ChildDashboard_Toys/Girl_Toys/train_vahwss.png"
            alt="train"
            width="150px"
            height="120px"
          />
        </div>
      ) : (
        <></>
      )}
      {toys.truck ? (
        <div className={childDashboardGirlToyTruck}>
          <img
            src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949069/ChildDashboard_Toys/Girl_Toys/truck_npfatr.png"
            alt="truck"
            width="100px"
            height="60px"
          />
        </div>
      ) : (
        <></>
      )}
    </div>
  )
}

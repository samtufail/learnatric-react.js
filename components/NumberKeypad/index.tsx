import { ReactElement } from 'react'
import styles from './index.module.scss'
const NumberKeypad = ({ onSelectNumber, onRemove }): ReactElement => {
  const { keypad, keypadWrapper, keypadWrapperBody } = styles
  return (
    <div className={keypadWrapper}>
      <table id="keypad" className={keypad} cellPadding={10} cellSpacing={5}>
        <tbody className={keypadWrapperBody}>
          <tr>
            <td onClick={() => onSelectNumber(1)}>1</td>
            <td onClick={() => onSelectNumber(2)}>2</td>
            <td onClick={() => onSelectNumber(3)}>3</td>
            <td onClick={() => onSelectNumber(4)}>4</td>
            <td onClick={() => onSelectNumber(5)}>5</td>
            <td onClick={() => onSelectNumber(6)}>6</td>
          </tr>
          <tr>
            <td onClick={() => onSelectNumber('.')}>.</td>
            <td onClick={() => onSelectNumber(7)}>7</td>
            <td onClick={() => onSelectNumber(8)}>8</td>
            <td onClick={() => onSelectNumber(9)}>9</td>
            <td onClick={() => onSelectNumber(0)}>0</td>
            <td onClick={() => onRemove()}>X</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default NumberKeypad

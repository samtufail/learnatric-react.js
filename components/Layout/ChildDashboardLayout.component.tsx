import Head from 'next/head'
import { ChildNavbar } from 'components'
import { ReactElement, useEffect } from 'react'
import styles from './ChildDashboardLayout.module.scss'

export interface LayoutProps {
  title: string
  description?: string
  children: ReactElement | ReactElement[]
  backgrndimg?: string | any
}

export const ChildDashboardLayout = ({
  title = '',
  children,
  description = 'Learnatric is a learning platform.',
}: LayoutProps): ReactElement => {
  useEffect(() => {
    document.documentElement.lang = 'en-us'
  })
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
      </Head>
      <main
        style={{
          background: `url('/img/child-dashboard/background.png')`,
          minHeight: '100vh',
          backgroundRepeat: 'no-repeat',
          backgroundPositionY: 'center',
          backgroundSize: 'cover',
        }}
      >
        <ChildNavbar />
        {children}
      </main>
    </>
  )
}

import { ChildNavbarHome } from 'components/Navbar/ChildNavbarHome.component'
import { ReactElement } from 'react'
import Head from 'next/head'

export const ChildDashboardHomeLayout = ({
  title = '',
  children,
  description = 'Learnatric is a learning platform.',
}): ReactElement => {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
      </Head>
      <main>
        <ChildNavbarHome />
        {children}
      </main>
    </>
  )
}

import Head from 'next/head'
import { Navbar } from 'components'
import { ReactElement, useEffect } from 'react'

export interface LayoutProps {
  title: string
  description?: string
  children: ReactElement | ReactElement[]
}

export const Layout = ({
  title = '',
  children,
  description = 'Learnatric is a learning platform.',
}: LayoutProps): ReactElement => {
  useEffect(() => {
    document.documentElement.lang = 'en-us'
  })
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
      </Head>
      <main>
        <Navbar />
        {children}
      </main>
    </>
  )
}

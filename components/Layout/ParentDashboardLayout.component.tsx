import { Layout as AntdLayout, Menu } from 'antd'
import {
  SettingOutlined,
  KeyOutlined,
  CreditCardOutlined,
  CustomerServiceOutlined,
  UserOutlined,
} from '@ant-design/icons'
import { Layout } from 'components'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { ReactElement } from 'react'
import {
  parentDashboard,
  sideBar,
  siderOptions,
  siderOptionsActive,
  parentDashboardContent,
} from './ParentDashboardLayout.module.scss'
import { setPage } from 'store/actions'

const { Sider, Content } = AntdLayout
const { Item } = Menu

const parentDashboardLinks = [
  {
    key: '1',
    title: 'Parent Dashboard',
    href: '/parent-dashboard',
    icon: <UserOutlined />,
  },
  {
    key: '2',
    title: 'Account Settings',
    href: '/parent-dashboard/account-settings',
    icon: <SettingOutlined />,
  },
  {
    key: '3',
    title: 'Billing',
    href: '/parent-dashboard/billing',
    icon: <CreditCardOutlined />,
  },
  {
    key: '4',
    title: 'Help and Support',
    href: '/parent-dashboard/help-support',
    icon: <CustomerServiceOutlined />,
  },
]

export interface ParentDashboardLayoutProps {
  children: any
}
export const ParentDashboardLayout = ({
  children,
}: ParentDashboardLayoutProps): ReactElement => {
  const router = useRouter()
  const { page } = useSelector(({ auth }: AppState) => auth)
  const dispatch = useDispatch()
  const handleLinkClick = (page: string): any => {
    dispatch(setPage(page))
  }

  return (
    <Layout title="Parent Dashboard - Learnatric">
      <AntdLayout className={`${parentDashboard} parent-dashboard`}>
        {(page === 'Parent Dashboard' ||
          page === 'Account Settings' ||
          page === 'Billing' ||
          page === 'Help and Support') && (
          <Sider className={sideBar}>
            <Menu mode="inline">
              {parentDashboardLinks.map((link) => (
                <Item
                  key={link.key}
                  className={`${siderOptions} ${
                    router.asPath === `${link.href}` ? siderOptionsActive : ''
                  }`}
                  icon={link.icon}
                  onClick={() => handleLinkClick(link.title)}
                >
                  <Link href={link.href}>
                    <a>{link.title}</a>
                  </Link>
                </Item>
              ))}
            </Menu>
          </Sider>
        )}
        <AntdLayout>
          <Content className={parentDashboardContent}>{children}</Content>
        </AntdLayout>
      </AntdLayout>
    </Layout>
  )
}

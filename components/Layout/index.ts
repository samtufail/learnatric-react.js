import dynamic from 'next/dynamic'
import { Layout as LayoutImport } from './NormalLayout.component'
import { ParentDashboardLayout as ParentDashboardLayoutImport } from './ParentDashboardLayout.component'
import { ChildDashboardLayout as ChildDashboardLayoutImport } from './ChildDashboardLayout.component'
import { ChildDashboardHomeLayout as ChildDashboardHomeLayoutImport } from './ChildDashboardHomeLayout.component'

export const Layout = dynamic(
  () =>
    import('./NormalLayout.component').then((module) => module.Layout) as any
) as typeof LayoutImport

export const ParentDashboardLayout = dynamic(
  () =>
    import('./ParentDashboardLayout.component').then(
      (module) => module.ParentDashboardLayout
    ) as any
) as typeof ParentDashboardLayoutImport

export const ChildDashboardLayout = dynamic(
  () =>
    import('./ChildDashboardLayout.component').then(
      (module) => module.ChildDashboardLayout
    ) as any
) as typeof ChildDashboardLayoutImport

export const ChildDashboardHomeLayout = dynamic(
  () =>
    import('./ChildDashboardHomeLayout.component').then(
      (module) => module.ChildDashboardHomeLayout
    ) as any
) as typeof ChildDashboardHomeLayoutImport

import Lottie from 'react-lottie'
import animation from 'animations/star.json'
// import classes from './Loader.module.scss'
import { Typography } from 'antd'
import { FC } from 'react'

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animation,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
}

export const Star: FC = () => {
  return (
    <div>
      <Lottie options={defaultOptions} height={150} width={150} />
      <Typography.Title level={4}>
        <div
          style={{
            color: 'white',
            border: '1px solid black',
            borderRadius: '3px',
            backgroundColor: 'rgba(0, 0, 0, .5)',
            width: '300px',
            fontSize: '18px',
          }}
        >
          <div style={{ padding: 4 }}>Great Job Completing Your Lesson</div>
        </div>
      </Typography.Title>
    </div>
  )
}

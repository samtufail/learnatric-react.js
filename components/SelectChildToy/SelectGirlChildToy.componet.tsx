import { ReactElement } from 'react'
import { Modal } from 'antd'
import { useSelector, useDispatch } from 'react-redux'
import { setChildToys } from 'store/actions'

import styles from './SelectGirlChildToy.module.scss'

export const SelectGirlChildToy = ({
  show,
  setShow,
  toys,

  // setToys,
}): ReactElement => {
  const { loading, account } = useSelector(({ child }: AppState) => child)
  const dispatch = useDispatch()
  const setToys = (toy: string): void => {
    console.log('toys was selected: ', toy)
    let { toys } = account
    toys = JSON.parse(toys)
    // console.log('toys parsed: ', toys)
    toys[toy] = true
    // console.log('toys true: ', toys)
    const data = {
      child_id: account.id,
      toys,
    }
    dispatch(setChildToys(data))
  }
  const handleOk = () => {
    setShow(false)
  }

  const handleCancel = () => {
    setShow(false)
  }

  const { ImageModalContainer, toySelectContainer, alreadyHasToy } = styles

  return (
    <div>
      <Modal
        title="Please Select a Toy"
        visible={show}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[<></>]}
      >
        <div className={ImageModalContainer}>
          <div
            className={toys.girlBlocks ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('girlBlocks')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948947/ChildDashboard_Toys/Girl_Toys/blocks_zbkqhw.png"
              alt="blocks"
              width="110px"
              height="100px"
            />
          </div>

          <div
            className={toys.girlBoat ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('girlBoat')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948947/ChildDashboard_Toys/Girl_Toys/boat_xa60fb.png"
              alt="boat"
              width="80px"
              height="80px"
            />
          </div>

          <div
            className={toys.baloon ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('baloon')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948947/ChildDashboard_Toys/Girl_Toys/baloon_qamnw6.png"
              alt="baloon"
              width="80px"
              height="80px"
            />
          </div>
          <div
            className={toys.butterfly ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('butterfly')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949029/ChildDashboard_Toys/Girl_Toys/butterfly_ex5iwq.png"
              alt="butterfly"
              width="80px"
              height="80px"
            />
          </div>
          <div
            className={toys.car ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('car')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949030/ChildDashboard_Toys/Girl_Toys/car_fia3fe.png"
              alt="Car"
              width="100px"
              height="80px"
            />
          </div>
          <div
            className={toys.girlcounter ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('girlcounter')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949030/ChildDashboard_Toys/Girl_Toys/counter_i3rwmd.png"
              alt="Counter"
              width="120px"
              height="110px"
            />
          </div>
          <div
            className={toys.monkey ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('monkey')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/monkey_d3ibcl.png"
              alt="monkey"
              width="80px"
              height="60px"
            />
          </div>
          <div
            className={toys.bear ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('bear')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948949/ChildDashboard_Toys/Girl_Toys/bear_up5cjf.png"
              alt="bear"
              width="80px"
              height="60px"
            />
          </div>
          <div
            className={toys.elephant ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('elephant')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/elephant_ds46md.png"
              alt="elephant"
              width="80px"
              height="60px"
            />
          </div>
          <div
            className={toys.bucket ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('bucket')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949029/ChildDashboard_Toys/Girl_Toys/bucket_drxei9.png"
              alt="mud bucket"
              width="80px"
              height="60px"
            />
          </div>
          <div
            className={toys.pig ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('pig')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/pig_f17mys.png"
              alt="pig"
              width="90px"
              height="60px"
            />
          </div>
          <div
            className={toys.ball ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('ball')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948947/ChildDashboard_Toys/Girl_Toys/ball_bl28gk.png"
              alt="ball"
              width="70px"
              height="60px"
            />
          </div>
          <div
            className={toys.scooty ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('scooty')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/scooty_uucccd.png"
              alt="Scooty"
              width="100px"
              height="60px"
            />
          </div>
          <div
            className={toys.singleSeatCar ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('singleSeatCar')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949068/ChildDashboard_Toys/Girl_Toys/single-seat-car_dli5v4.png"
              alt="single seat car"
              width="80px"
              height="60px"
            />
          </div>
          <div
            className={toys.spinosaurus ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('spinosaurus')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949068/ChildDashboard_Toys/Girl_Toys/spinosaurus_peismk.png"
              alt="Spinosaurus"
              width="80px"
              height="60px"
            />
          </div>
          <div
            className={toys.squirrel ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('squirrel')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949070/ChildDashboard_Toys/Girl_Toys/squirrel_b3pyax.png"
              alt="squirrel"
              width="80px"
              height="60px"
            />
          </div>
          <div
            className={toys.doll ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('doll')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949029/ChildDashboard_Toys/Girl_Toys/doll_zlkcyk.png"
              alt="doll"
              width="80px"
              height="60px"
            />
          </div>
          <div
            className={toys.game ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('game')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949053/ChildDashboard_Toys/Girl_Toys/game_fnwium.png"
              alt="game"
              width="60px"
              height="60px"
            />
          </div>
          <div
            className={toys.toyTrain ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('toyTrain')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949068/ChildDashboard_Toys/Girl_Toys/train_vahwss.png"
              alt="train"
              width="150px"
              height="120px"
            />
          </div>
          <div
            className={toys.truck ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('truck')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639949069/ChildDashboard_Toys/Girl_Toys/truck_npfatr.png"
              alt="truck"
              width="100px"
              height="60px"
            />
          </div>
        </div>
      </Modal>
    </div>
  )
}

import { ReactElement } from 'react'
import { Modal } from 'antd'
import styles from './SelectChildToy.module.scss'
import { useSelector, useDispatch } from 'react-redux'
import { setChildToys } from 'store/actions'

export const SelectBoyChildToy = ({
  show,
  setShow,
  toys,
  // setToys,
}): ReactElement => {
  const { loading, account } = useSelector(({ child }: AppState) => child)
  const dispatch = useDispatch()

  const setToys = (toy: string): void => {
    console.log('toys was selected: ', toy)
    let { toys } = account
    toys = JSON.parse(toys)
    // console.log('toys parsed: ', toys)
    toys[toy] = true
    // console.log('toys true: ', toys)
    const data = {
      child_id: account.id,
      toys,
    }
    dispatch(setChildToys(data))
  }

  const handleOk = () => {
    setShow(false)
  }

  const handleCancel = () => {
    setShow(false)
  }
  const { ImageModalContainer, toySelectContainer, alreadyHasToy } = styles
  return (
    <div>
      {/* <Button type="primary" onClick={showModal}>
        Open Modal
      </Button> */}
      <Modal
        title="Please Select a Toy"
        visible={show}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[<></>]}
      >
        <div className={ImageModalContainer}>
          <div
            className={toys.armySoldier ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('armySoldier')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948337/ChildDashboard_Toys/Boy_Toys/army-soldier_eynvsl.png"
              alt="army-soldier"
              width="50px"
              height="50px"
            />
          </div>
          <div
            className={toys.armySpy ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setToys('armySpy')
              setShow(false)
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948416/ChildDashboard_Toys/Boy_Toys/army-spy_lk33el.png"
              alt="army-spy"
              width="50px"
              height="50px"
            />
          </div>
          <div
            className={toys.baskitBall ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('baskitBall')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948429/ChildDashboard_Toys/Boy_Toys/ball_b9ls8b.png"
              alt="ball"
              width="50px"
              height="50px"
            />
          </div>
          <div
            className={toys.bannasMonkey ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('bannasMonkey')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948437/ChildDashboard_Toys/Boy_Toys/bannas-monkey_tjigd4.png"
              alt="monkey"
              width="60px"
              height="60px"
            />
          </div>
          <div
            className={toys.blocks ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('blocks')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948442/ChildDashboard_Toys/Boy_Toys/blocks_h7al8o.png"
              alt="blocks"
              width="110px"
              height="100px"
            />
          </div>
          <div
            className={toys.counter ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('counter')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948458/ChildDashboard_Toys/Boy_Toys/counter_jcpite.png"
              alt="counter"
              width="120px"
              height="110px"
            />
          </div>
          <div
            className={toys.footBall ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('footBall')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948458/ChildDashboard_Toys/Boy_Toys/foot-ball_fdwyff.png"
              alt="foot ball"
              width="80px"
              height="80px"
            />
          </div>
          <div
            className={toys.toyTrain ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('toyTrain')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948557/ChildDashboard_Toys/Boy_Toys/toy-train_rgqmx5.png"
              alt="toy train"
              width="150px"
              height="120px"
            />
          </div>
          <div
            className={toys.tank ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('tank')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948557/ChildDashboard_Toys/Boy_Toys/tank_aw0cgb.png"
              alt="tank"
              width="100px"
              height="50px"
            />
          </div>
          <div
            className={toys.sportsCar ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('sportsCar')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948556/ChildDashboard_Toys/Boy_Toys/sports-car-purple_obs7em.png"
              alt="sports car"
              width="120px"
              height="50px"
            />
          </div>
          <div
            className={toys.toyGame ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('toyGame')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948557/ChildDashboard_Toys/Boy_Toys/toy-game_pz1enk.png"
              alt="toy game"
              width="60px"
              height="50px"
            />
          </div>
          <div
            className={toys.spaceShip ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('spaceShip')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948556/ChildDashboard_Toys/Boy_Toys/space-ship_xjk5dn.png"
              alt="space ship"
              width="80px"
              height="50px"
            />
          </div>
          <div
            className={toys.boat ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('boat')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948457/ChildDashboard_Toys/Boy_Toys/boat_z4tcm6.png"
              alt="boat"
              width="80px"
              height="50px"
            />
          </div>
          <div
            className={toys.mudBucket ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('mudBucket')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948478/ChildDashboard_Toys/Boy_Toys/mud-bucket_gxo704.png"
              alt="mud bucket"
              width="80px"
              height="50px"
            />
          </div>
          <div
            className={toys.scpaceRocket ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('scpaceRocket')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948480/ChildDashboard_Toys/Boy_Toys/space-rocket_ouzyud.png"
              alt="space rocket"
              width="50px"
              height="50px"
            />
          </div>
          <div
            className={toys.robot ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('robot')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948478/ChildDashboard_Toys/Boy_Toys/robot_y1xt08.png"
              alt="robot"
              width="70px"
              height="50px"
            />
          </div>
          <div
            className={toys.puppy ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('puppy')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948478/ChildDashboard_Toys/Boy_Toys/puppy_czsgkc.png"
              alt="puppy"
              width="70px"
              height="50px"
            />
          </div>
          <div
            className={toys.plane ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('plane')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948478/ChildDashboard_Toys/Boy_Toys/plane_i5l8cg.png"
              alt="plane"
              width="70px"
              height="50px"
            />
          </div>
          <div
            className={toys.helicopter ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('helicopter')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948458/ChildDashboard_Toys/Boy_Toys/helicopter_mpjs4h.png"
              alt="helicopter"
              width="80px"
              height="50px"
            />
          </div>
          <div
            className={toys.apatosaurus ? alreadyHasToy : toySelectContainer}
            onClick={() => {
              setShow(false)
              setToys('apatosaurus')
            }}
          >
            <img
              src="https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1639948250/ChildDashboard_Toys/Boy_Toys/apatosaurus_ukwdry.png"
              alt="Apatosaurus"
              width="80px"
              height="50px"
            />
          </div>
        </div>
      </Modal>
    </div>
  )
}

// This data will be deleted once actual apis get impletemted.

export const dashboardBackgroundUrls = [
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/Pre-K+Farm+1.jpg',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/K+Beach+Background.jpg',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/1st%2C+Space+4.jpg',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/2nd%2C+Desert+2.jpg',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/3rd%2C+Rainforest+1.jpg',
  },
]

export const tensFrameUrls = [
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+1.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+2.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+3.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+4.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+5.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+6.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+7.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+8.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+9.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+10.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+11.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+12.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+13.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+14.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+15.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+16.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+17.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+18.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+19.png',
  },
  {
    url: 'https://learnatric-problem-illustrations.s3.amazonaws.com/10s+Frame+20.png',
  },
]

export const lessonResponse = {
  id: 17,
  tag_id: 9,
  lesson_name: 'Understand the relationship between numbers and quantities 1',
  url: 'https://player.vimeo.com/video/577336483',
  we_dos: [
    {
      url: 'https://player.vimeo.com/video/577676095',
      we_do_question_text:
        'Choose the number on the screen that matches the picture. ',
      question_type: 'number pad',
      answer: '4',
      mc1: '',
      mc2: '',
      mc3: '',
      mc4: '',
      tag_id: 9,
    },
    {
      url: 'https://player.vimeo.com/video/577676380',
      we_do_question_text:
        'Choose the number on the screen that matches the picture. ',
      question_type: 'number pad',
      answer: '4',
      mc1: '',
      mc2: '',
      mc3: '',
      mc4: '',
      tag_id: 9,
    },
    {
      url: 'https://player.vimeo.com/video/577676743',
      we_do_question_text:
        'Choose the number on the screen that matches the picture. ',
      question_type: 'number pad',
      answer: '4',
      mc1: '',
      mc2: '',
      mc3: '',
      mc4: '',
      tag_id: 9,
    },
  ],
  questions: [
    {
      answer: '10',
      question_id: 1,
      lesson_id: 17,
      target: '0.7',
      question_text: 'Count up to 10 numbers.',
      p_tag: '9',
      s_tags: ['14', '', '', '', ''],
      question_type: 'tens frame',
      mc_1: '4',
      mc_2: '5',
      mc_3: '7',
      mc_4: '8',
    },
    {
      answer: '5',
      question_id: 1,
      lesson_id: 17,
      target: '0.7',
      question_text: 'How many hot dogs are there?',
      p_tag: '9',
      s_tags: ['14', '', '', '', ''],
      audio_url:
        'https://learnatric-audio.s3.amazonaws.com/how-many-clouds.mp3',
      illustration_url:
        'https://learnatric-problem-illustrations.s3.amazonaws.com/hot+dog+5.png',
      question_type: 'multiple choice',
      mc_1: '4',
      mc_2: '5',
      mc_3: '7',
      mc_4: '8',
    },
    {
      answer: '1',
      question_id: 2,
      lesson_id: 17,
      target: '0.7',
      question_text: 'How many baseballs?',
      p_tag: '9',
      s_tags: ['14', '', '', '', ''],
      audio_url:
        'https://learnatric-audio.s3.amazonaws.com/how-many-baseballs.mp3',
      illustration_url:
        'https://learnatric-problem-illustrations.s3.amazonaws.com/baseball+1.png',
      question_type: 'multiple choice',
      mc_1: '2',
      mc_2: '3',
      mc_3: '1',
      mc_4: '5',
    },
    {
      question_id: 3,
      answer: '10',
      lesson_id: 17,
      target: '0.7',
      question_text: 'How many party hats do you see?',
      p_tag: '9',
      s_tags: ['14', '', '', '', ''],
      audio_url:
        'https://learnatric-audio.s3.amazonaws.com/how-many-party-hats.mp3',
      illustration_url:
        'https://learnatric-problem-illustrations.s3.amazonaws.com/party+hat+10.png',
      question_type: 'multiple choice',
      mc_1: '5',
      mc_2: '6',
      mc_3: '9',
      mc_4: '10',
    },
    {
      answer: '3',
      question_id: 4,
      lesson_id: 17,
      target: '0.7',
      question_text: 'How many buckets are there?',
      p_tag: '9',
      s_tags: ['14', '', '', '', ''],
      audio_url:
        'https://learnatric-audio.s3.amazonaws.com/how-many-buckets.mp3',
      illustration_url:
        'https://learnatric-problem-illustrations.s3.amazonaws.com/buckets+3.png',
      question_type: 'multiple choice',
      mc_1: '2',
      mc_2: '3',
      mc_3: '5',
      mc_4: '6',
    },
    {
      answer: '7',
      question_id: 5,
      lesson_id: 17,
      target: '0.7',
      question_text: 'How many clouds in the sky?',
      p_tag: '9',
      s_tags: ['14', '', '', '', ''],
      audio_url:
        'https://learnatric-audio.s3.amazonaws.com/how-many-clouds.mp3',
      illustration_url:
        'https://learnatric-problem-illustrations.s3.amazonaws.com/clouds+7.png',
      question_type: 'multiple choice',
      mc_1: '1',
      mc_2: '2',
      mc_3: '7',
      mc_4: '8',
    },
  ],
}

export const lessonsPathWay = [
  { top: '40vh', left: '3vw' },
  { top: '46vh', left: '9vw' },
  { top: '57vh', left: '14vw' },
  { top: '59vh', left: '23vw' },
  { top: '50vh', left: '31vw' },
  { top: '43vh', left: '38vw' },
  { top: '45vh', left: '48vw' },
  { top: '55vh', left: '55vw' },
  { top: '56vh', left: '65vw' },
  { top: '45vh', left: '70vw' },
  { top: '40vh', left: '93vw' },
  { top: '46vh', left: '100vw' },
  { top: '57vh', left: '104vw' },
  { top: '59vh', left: '114vw' },
  { top: '50vh', left: '122vw' },
  { top: '43vh', left: '129vw' },
  { top: '45vh', left: '139vw' },
  { top: '55vh', left: '146vw' },
  { top: '56vh', left: '156vw' },
  { top: '45vh', left: '162vw' },
  { top: '40vh', left: '185vw' },
  { top: '46vh', left: '192vw' },
  { top: '57vh', left: '196vw' },
  { top: '59vh', left: '205vw' },
  { top: '50vh', left: '214vw' },
  { top: '43vh', left: '221vw' },
  { top: '45vh', left: '231vw' },
  { top: '55vh', left: '237vw' },
  { top: '56vh', left: '247vw' },
  { top: '45vh', left: '253vw' },
  { top: '40vh', left: '276vw' },
  { top: '46vh', left: '283vw' },
  { top: '57vh', left: '287vw' },
  { top: '59vh', left: '296vw' },
  { top: '50vh', left: '305vw' },
  { top: '43vh', left: '312vw' },
  { top: '45vh', left: '322vw' },
  { top: '55vh', left: '329vw' },
  { top: '56vh', left: '339vw' },
  { top: '45vh', left: '344vw' },
]

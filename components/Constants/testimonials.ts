export const testimonialData = [
  {
    testimonialText: `“My son was doing really well at Learnatric so that was motivating.  But a little bit of a challenge is good too, and we could see that it started to get harder.”`,
    name: `Sara, mother of a PreK student and first grader`,
  },
  {
    testimonialText: `“The instruction is good – it doesn’t just show a bunch of videos”`,
    name: `Kelley, mother of a PreK student`,
  },
  {
    testimonialText: `“I really like that the lessons come in small snippets – it’s like reading a chapter book and makes it easy to digest. I like that the lessons aren’t so long in the beginning that my daughter isn’t getting bored.”`,
    name: `Shari, mother of a kindergartener`,
  },
]

export const testimonials = [
  {
    testimonialText:
      'Repetition in educational technology may be useful, but it can be a waste of time if the student is ready to move on and the program forces you to spend a certain amount of time on a lesson.',
    name: 'Rudy, father of an 11-year old',
  },
  {
    testimonialText:
      'Offering a personalized learning cadence to each kid’s learning capacity is an absolutely great idea.',
    name: 'Vincent, father of a 2-year old',
  },
  {
    testimonialText:
      'The great thing about Learnatric is how advanced it is…the biggest problem with other programs is that they are not personalized.',
    name: 'John, father of 8, 11, and 12-year olds',
  },
]

export const testimonial2 = [
  {
    testimonialText: `“Offering personalized learning cadence to each kid’s learning capacity is absolutely a great idea.”`,
    name: `Vincent, father of 2-year old`,
  },
  {
    testimonialText: `“Repetition in educational technology may be useful, but it can be a waste of time if the student is ready to move on and the program forces you to spend a certain amount of time on a lesson.”`,
    name: `Rudy, father of 11-year old`,
  },
  {
    testimonialText: `“The great thing about Learnatric is how advanced it is … the biggest problem with other programs is they are not personalized”`,
    name: `John , father of 8, 11, and 12-year olds`,
  },
]

export const team = [
  {
    img: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244987/AboutUs/TeamSection/david_of3wv6.jpg',
    name: 'David Shpigler',
    designation: 'CEO',
    description:
      'David came up with the idea for “dynamic education” after realizing the limitations of legacy educational systems when working with his own children.',
    linkedIn: 'https://www.linkedin.com/in/david-shpigler-8583074a/',
  },
  {
    img: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244988/AboutUs/TeamSection/holly_pqvqo6.jpg',
    name: 'Dr. Holly Windram',
    designation: 'Chief Educational Officer',
    description:
      'Holly has a PhD in Educational Psychology and brings her vast experience to developing the pedagogical components of design to the company.',
    linkedIn: 'https://www.linkedin.com/in/holly-windram-phd-38636237/',
  },
  {
    img: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244986/AboutUs/TeamSection/anthony_mrwwel.jpg',
    name: 'Anthony DiCecco',
    designation: 'Data Scientist',
    description:
      'Anthony is a former teacher and data scientist with a master’s degree in applied statistics and analytics. He’s building a recommendation engine to customize students’ learning paths.',
    linkedIn: 'https://www.linkedin.com/in/anthony-dicecco/',
  },
  {
    img: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244986/AboutUs/TeamSection/ben_wmbxpa.jpg',
    name: 'Ben Taylor',
    designation: 'Product Owner',
    description:
      'Ben is a former teacher who switched to technology, especially data and the public cloud. He translates the characteristics of a tutor into our web app.',
    linkedIn: 'https://www.linkedin.com/in/ben-taylor-272ab9156/',
  },
  {
    img: 'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632244987/AboutUs/TeamSection/brian_vufrym.jpg',
    name: 'Brian Schaaf',
    designation: 'Full-Stack Developer',
    description:
      'Brian is a full-stack developer with experience in the cloud as well. He bikes in his spare time.',
    linkedIn: 'https://www.linkedin.com/in/brian-schaaf/',
  },
]

export const featuresContent = [
  {
    image:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246230/LearningPlatform/WorkSection/mlearn_img1_el0i30.png',
    heading: 'Lessons',
    description:
      'Lessons delivered to students based on their needs to make sure their time is well spent',
  },
  {
    image:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246230/LearningPlatform/WorkSection/mlearn_img2_xzo5xn.png',
    heading: 'Activities',
    description:
      'Activities that engage students through the process and help them learn in a fun way',
  },
  {
    image:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246230/LearningPlatform/WorkSection/mlearn_img3_eqb5rv.png',
    heading: 'Problem Sets',
    description:
      'Testing proficiency in all areas covered by lessons and activities to measure progress for every student',
  },
  {
    image:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246230/LearningPlatform/WorkSection/mlearn_img4_o4qr0x.png',
    heading: 'Dashboard',
    description:
      'Identifying strengths and weaknesses of each student to optimize the learning path',
  },
]
export const featuresContent2 = [
  {
    image:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246230/LearningPlatform/WorkSection/mlearn_img4_o4qr0x.png',
    heading: 'Parent Dashboard',
    description:
      'Parents can view their students’ progress in form of graphs and charts, personal rooms, and more',
  },
  {
    image:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246230/LearningPlatform/WorkSection/mlearn_img3_eqb5rv.png',
    heading: 'Instructions',
    description:
      'Robo gives direct instruction with videos that are curated based on each student’s needs',
  },
  {
    image:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246230/LearningPlatform/WorkSection/mlearn_img2_xzo5xn.png',
    heading: 'Activities',
    description:
      'Robo asks questions and responds based on how students perform, just as a tutor does',
  },
  {
    image:
      'https://res.cloudinary.com/learnatric/image/upload/q_auto,f_webp/v1632246230/LearningPlatform/WorkSection/mlearn_img1_el0i30.png',
    heading: 'Problem Sets',
    description:
      'Students independently practice curated problems to demonstrate mastery',
  },
]

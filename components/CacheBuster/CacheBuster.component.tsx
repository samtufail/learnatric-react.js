import { ReactElement, useEffect, useState} from 'react';
import packageJson from '../../package.json';
global.appVersion = packageJson.version;
interface CacheBusterProps {
  children: any
}

export const CacheBuster = ({  children }: CacheBusterProps) : ReactElement => {
  const [loading, setLoading] = useState<boolean>(true)
  const refreshCacheAndReload = async () => {
    console.log('Clearing cache and hard reloading...')
    if(caches) {
      const names = await caches.keys()
      names.forEach(name => {
        console.log('name: ', name)
        caches.delete(name)
      })
    }
    window.location.reload();
  }

  useEffect(() => {
    const version = localStorage.getItem('version')
    if (!version) {
      localStorage.setItem('version', packageJson.version)
      refreshCacheAndReload()
    } else {
      const latestVersion = version;
      const currentVersion = global.appVersion
      if (latestVersion !== currentVersion) {
        localStorage.removeItem('version')
        localStorage.setItem('version', currentVersion)
        refreshCacheAndReload()
        setLoading(false)
      } else {
        setLoading(false)
      }
    }
  }, [])

  return (
    <>
      {!loading &&
        children
      }
    </>
  )
}
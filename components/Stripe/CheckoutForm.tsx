import React, { ReactElement } from 'react'
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js'

export const CheckoutForm = (): ReactElement => {
  const stripe = useStripe()
  const elements = useElements()

  const handleSubmit = async (event) => {
    event.preventDefault()
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: elements.getElement(CardElement),
    })
  }

  return <CardElement />
}

import dynamic from 'next/dynamic'
import { StripeContainer as StripeImport } from './StripeContainer'
import { CardInput as CardInputImport } from './CardInput'

export const Stripe = dynamic(
  () =>
    import('./StripeContainer').then((module) => module.StripeContainer) as any
) as typeof StripeImport

export const CardInput = dynamic(
  () => import('./CardInput').then((module) => module.CardInput) as any
) as typeof CardInputImport

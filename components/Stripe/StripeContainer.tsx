import React, { useState, useEffect, ReactElement } from 'react'
import { loadStripe, Stripe } from '@stripe/stripe-js'
import { Elements } from '@stripe/react-stripe-js'
export interface StripeProps {
  children: ReactElement | ReactElement[]
}

// Production
// const STRIPE_PUBLISH_KEY =
//   'pk_live_51J2zzME260ZusP4JoCL41uIIg5FLHijJdMCiX62NpZ5LM3B5mxL5nEQsG4fE2JtJAqUe83MXwJn7kYYI4GPz79XJ00SsbE9Q6d'

// Development
const STRIPE_PUBLISH_KEY =
  'pk_test_51J2zzME260ZusP4JwfTcUB8mmbeIu09aAv1bixaOocy8VRK6zbagC0kTS94xnJsFRucW8r1ATpq5drjIp1l0vwfr00jUEGf07H'

export const StripeContainer = ({ children }: StripeProps): ReactElement => {
  const [stripePromise, setStripePromise] = useState<undefined | Stripe>()

  useEffect(() => {
    const getStripe = async () => {
      if (!stripePromise) {
        setStripePromise(await loadStripe(STRIPE_PUBLISH_KEY))
      }
    }
    getStripe()
  }, [])

  return !stripePromise ? null : (
    <Elements stripe={stripePromise}>{children}</Elements>
  )
}

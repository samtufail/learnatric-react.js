import { ReactElement } from 'react'
import { Heading, Paragraph } from '../atoms'
import styles from './FeaturesCard.module.scss'

export const FeaturesCard = ({ image, heading, description }): ReactElement => {
  const {
    featuresCard,
    featuresCardImage,
    featuresCardHeading,
    featuresCardParagraph,
  } = styles
  return (
    <div className={featuresCard}>
      <img
        className={featuresCardImage}
        src={image}
        alt="feautre"
        width="250"
        height="220"
      />
      <Heading level={1} className={featuresCardHeading}>
        {heading}
      </Heading>
      <Paragraph className={featuresCardParagraph}>{description}</Paragraph>
    </div>
  )
}

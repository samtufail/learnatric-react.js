import { ReactElement } from 'react'
import {
  ResponsiveContainer,
  BarChart as ReBarChart,
  Bar,
  XAxis,
  YAxis,
} from 'recharts'
import styles from './styles.module.scss'

export interface BarChartProps {
  data: {
    name?: string
    value?: number
  }[]
}

export const BarChart = ({ data }: BarChartProps): ReactElement => {
  const { customChart } = styles
  return (
    <ResponsiveContainer width="100%" height="100%">
      <ReBarChart data={data} style={{ width: '100%' }} className={customChart}>
        <Bar dataKey="value" barSize={50} fill="#5b9bd4" />
        <XAxis dataKey="name" />
        <YAxis />
      </ReBarChart>
    </ResponsiveContainer>
  )
}

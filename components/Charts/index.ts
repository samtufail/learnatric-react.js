import dynamic from 'next/dynamic'
import { BarChart as BarChartImport } from './BarChart.component'

export const BarChart = dynamic(
  () => import('./BarChart.component').then((module) => module.BarChart) as any
) as typeof BarChartImport

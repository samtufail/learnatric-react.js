import { AUTH_ACTION_TYPES } from 'store/actionTypes'

const INITIAL_STATE: AuthStore = {
  isLoggedIn: false,
  type: null,
  page: '',
  loading: false,
  account: null,
}

interface Action {
  payload: any
  type: string
}

const AuthReducer = (
  state: AuthStore = INITIAL_STATE,
  action: Action
): AuthStore => {
  switch (action.type) {
    case AUTH_ACTION_TYPES.PAGE: {
      return {...state, page: action.payload}
    }
    case AUTH_ACTION_TYPES.AUTH_LOADING_START: {
      return { ...state, loading: true }
    }
    case AUTH_ACTION_TYPES.AUTH_LOADING_STOP: {
      return { ...state, loading: false }
    }
    case AUTH_ACTION_TYPES.PARENT_SIGNUP_SUCCESS: {
      return { ...state, loading: false }
    }
    case AUTH_ACTION_TYPES.USER_LOGIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        isLoggedIn: true,
        type: action.payload.type,
        account: action.payload.account,
      }
    }
    case AUTH_ACTION_TYPES.LOGOUT: {
      return {
        ...state,
        isLoggedIn: false,
        type: '',
        loading: false,
      }
    }
    default: {
      return state
    }
  }
}

export default AuthReducer

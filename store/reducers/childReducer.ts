// import { updateChild } from './../actions/userActions'
import { CHILD_ACTION_TYPES } from 'store/actionTypes'

const INITIAL_STATE: ChildStore = {
  loading: false,
  account: null,
  currentLesson: null,
  userLesson: null,
  currentLessonQuestions: null,
  cameFromLesson: false,
  playInitialVideo: false,
  initialVideoDisplay: 'none',
  question_id: null,
  wobble: 0,
  trainNumber: 1,
}

interface Action {
  payload: any
  type: string
}

const ChildReducer = (
  state: ChildStore = INITIAL_STATE,
  action: Action
): ChildStore => {
  switch (action.type) {
    case CHILD_ACTION_TYPES.WOBBLE: {
      return {
        ...state,
        wobble: action.payload,
      }
    }
    case CHILD_ACTION_TYPES.TRAIN_NUMBER: {
      return {
        ...state,
        trainNumber: action.payload,
      }
    }
    case CHILD_ACTION_TYPES.REMOVE_Q_ID: {
      return {
        ...state,
        question_id: null,
      }
    }
    case CHILD_ACTION_TYPES.QUESTION_FETCH: {
      return { ...state, currentLessonQuestions: action.payload }
    }
    case CHILD_ACTION_TYPES.QUESTION_CURATION: {
      return {
        ...state,
        currentLessonQuestions: action.payload.question,
        question_id: action.payload.question.question_id,
      }
    }
    case CHILD_ACTION_TYPES.CHANGE_VIDEO_DISPLAY: {
      return { ...state, initialVideoDisplay: action.payload }
    }
    case CHILD_ACTION_TYPES.PLAY_INITIAL_VIDEO: {
      return { ...state, playInitialVideo: action.payload }
    }
    case CHILD_ACTION_TYPES.FINISHED_LESSON: {
      return { ...state, cameFromLesson: action.payload.bool }
    }
    case CHILD_ACTION_TYPES.CHILD_LOADING_START: {
      return { ...state, loading: true }
    }
    case CHILD_ACTION_TYPES.CHILD_LOADING_STOP: {
      return { ...state, loading: false }
    }
    case CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS: {
      return {
        ...state,
        account: action.payload,
      }
    }
    case CHILD_ACTION_TYPES.GET_CHILD_LESSON: {
      return {
        ...state,
        currentLesson: action.payload.lesson,
        // currentLessonQuestions: action.payload.questions,
        // loading: false,
      }
    }

    case CHILD_ACTION_TYPES.ADD_AND_GET_USER_LESSON: {
      return {
        ...state,
        userLesson: action.payload,
        loading: false,
      }
    }

    case CHILD_ACTION_TYPES.UPDATE_USER_LESSON: {
      if (action.payload === 'start-over') {
        return {
          ...state,
          userLesson: {
            ...state.userLesson,
            instruction_completed: false,
          },
        }
      } else {
        return {
          ...state,
          userLesson: {
            ...state.userLesson,
            [action.payload]: true,
          },
          loading: false,
        }
      }
    }

    case CHILD_ACTION_TYPES.ADD_ANSWER_RESULT: {
      const { lastQuestion } = action.payload.data
      return {
        ...state,
        userLesson: {
          ...state.userLesson,
          problem_questions_attempted: lastQuestion
            ? 0
            : state.userLesson.problem_questions_attempted + 1,
        },
        account: lastQuestion
          ? {
              ...state.account,
            }
          : state.account,
        loading: false,
      }
    }
    default: {
      return state
    }
  }
}

export default ChildReducer

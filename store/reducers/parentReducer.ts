import { PARENT_ACTION_TYPES } from 'store/actionTypes'

const INITIAL_STATE: ParentStore = {
  loading: false,
  account: null,
  childPerformance: null,
  billingDetails: null
}

interface Action {
  payload: any
  type: string
}

const ParentReducer = (
  state: ParentStore = INITIAL_STATE,
  action: Action
): ParentStore => {
  switch (action.type) {
    case PARENT_ACTION_TYPES.SAVE_BILLING_DETAILS: {
      return { ...state, billingDetails: action.payload }
    }
    case PARENT_ACTION_TYPES.GET_CHILD_PERFORMANCE: {
      return { ...state, childPerformance: action.payload }
    }
    case PARENT_ACTION_TYPES.PARENT_LOADING_START: {
      return { ...state, loading: true }
    }
    case PARENT_ACTION_TYPES.PARENT_LOADING_STOP: {
      return { ...state, loading: false }
    }
    case PARENT_ACTION_TYPES.SAVE_PARENT_ACCOUNT_DETAILS: {
      return {
        ...state,
        account: action.payload,
      }
    }
    case PARENT_ACTION_TYPES.ADD_CHILD_SUCCESS: {
      return {
        ...state,
        account: {
          ...state.account,
          noOfChildsProfileCompleted:
            state.account.noOfChildsProfileCompleted + 1,
        },
        loading: false,
      }
    }

    case PARENT_ACTION_TYPES.GET_PARENT_CHILDS_SUCCESS: {
      return {
        ...state,
        account: {
          ...state.account,
          childrens: action.payload,
        },
        loading: false,
      }
    }

    case PARENT_ACTION_TYPES.UPDATE_CHILD_SUCCESS: {
      return {
        ...state,
        account: {
          ...state.account,
          noOfChildsCredsCompleted: state.account.noOfChildsCredsCompleted + 1,
        },
        loading: false,
      }
    }

    case PARENT_ACTION_TYPES.UPDATE_EMAIL: {
      return { ...state, account: { ...state.account, email: action.payload } }
    }
    case PARENT_ACTION_TYPES.UPDATE_PHONE: {
      return {
        ...state,
        account: { ...state.account, phoneNumber: action.payload },
      }
    }
    case PARENT_ACTION_TYPES.UPDATE_CHILD_NAME: {
      return {
        ...state,
        account: { ...state.account, firstName: action.payload },
      }
    }
    default: {
      return state
    }
  }
}

export default ParentReducer

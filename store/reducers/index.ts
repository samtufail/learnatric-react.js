import { combineReducers } from 'redux'
import storage from 'redux-persist/lib/storage'
import { persistReducer } from 'redux-persist'

import childReducer from './childReducer'
import parentReducer from './parentReducer'
import authReducer from './authReducer'
import assessmentReducer from './assessmentReducer'
// import hardSet from 'redux-persist/es/stateReconciler/hardSet'
const innerPersistConfig = {
  key: 'root',
  storage,
  // blacklist: ['assessment'],
  // stateReconciler: hardSet,
}

const innerReducer = combineReducers({
  child: childReducer,
  parent: parentReducer,
  auth: authReducer,
  assessment: assessmentReducer,
})

const rootReducer = persistReducer(innerPersistConfig, innerReducer)

export default rootReducer

import { ASSESSMENT_ACTION_TYPES } from 'store/actionTypes'

const INITIAL_STATE: AssessmentStore = {
  index: 0,
  question: null,
  answers: [],
}

interface Action {
  payload: any
  type: string
}

const AssessmentReducer = (
  state: AssessmentStore = INITIAL_STATE,
  action: Action
): AssessmentStore => {
  switch (action.type) {
    case ASSESSMENT_ACTION_TYPES.SET_ASSESSMENT_QUESTIONS: {
      return {
        ...state,
        question: action.payload,
      }
    }
    case ASSESSMENT_ACTION_TYPES.SET_IDX: {
      return {
        ...state,
        index: action.payload,
      }
    }
    case ASSESSMENT_ACTION_TYPES.SET_ASSESSMENT_ANSWERS: {
      console.log('data payload: ', action.payload)
      return {
        ...state,
        answers: action.payload,
      }
    }
    default:
      return state
  }
}

export default AssessmentReducer

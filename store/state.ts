import { Child, Lesson, Parent, Question } from 'models'
import { UserLesson } from 'models/UserLesson'
import { compose } from 'redux'
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose
  }

  interface AssessmentStore {
    index: number
    question: any
    answers: any
  }
  interface ChildStore {
    account: Child
    loading: Boolean
    currentLesson: Lesson
    currentLessonQuestions: Question
    userLesson: UserLesson
    cameFromLesson: boolean
    playInitialVideo: boolean
    initialVideoDisplay: string
    question_id: number
    wobble: number
    trainNumber: number
  }
  interface ParentStore {
    loading: Boolean
    account: Parent
    childPerformance: any
    billingDetails: any
  }
  interface AppState {
    auth: AuthStore
    child: ChildStore
    parent: ParentStore
    assessment: AssessmentStore
  }
  interface AuthStore {
    isLoggedIn: boolean
    page: string
    type: string
    loading: boolean
    account: Child | Parent
  }
}

import { toast } from 'react-toastify'
import { Dispatch } from 'redux'
import axios from 'api/axios'
import {
  PARENT_ACTION_TYPES,
  CHILD_ACTION_TYPES,
  AUTH_ACTION_TYPES,
} from '../actionTypes'
import { store } from '../index'
import { gaEvent } from 'lib/ga'

export const addUserLesson =
  (lessonId: number) => async (dispatch: Dispatch) => {
    dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_START })
    // dispatch({
    //   type: CHILD_ACTION_TYPES.FINISHED_LESSON,
    //   payload: { bool: false },
    // })
    const userLesssonId = store.getState().child.account?.user_lesson_id
    const data = {
      child_id: localStorage.getItem('CHILD_ID'),
      lesson_id: lessonId,
      user_lesson_id: userLesssonId ? userLesssonId : null,
    }
    axios
      .post('/userLesson', data)
      .then((res) => {
        // console.log('res after clickijng into lesson: ', res)
        if (res.status === 200) {
          const { result } = res.data
          // the only cases where this wont run is when the user haddent finished the lesson they were on before loggin out or leaving the computer
          if (res.data.result.updatedChild) {
            // console.log('was no user_lessonId')
            dispatch({
              type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
              payload: res.data.result.updatedChild[1],
            })
            dispatch({
              type: CHILD_ACTION_TYPES.ADD_AND_GET_USER_LESSON,
              payload: res.data.result.updateUserLesson,
            })
          } else {
            // console.log('was user_lessonId')
            dispatch({
              type: CHILD_ACTION_TYPES.ADD_AND_GET_USER_LESSON,
              payload: res.data.result,
            })
          }
          // dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
        } else {
          console.log('Response', res)
          toast.error('Something Went Wrong')
          dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
        }
      })
      .catch((error) => {
        toast.error(error.response.data.message)
        dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
      })
  }

export const updateUserLesson =
  (
    userLessonID: number,
    type: string,
    child_id: string,
    is_we_do: boolean = false
  ) =>
  async (dispatch: Dispatch) => {
    dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_START })
    // console.log('type: ', type)
    const data = {
      field: type,
      child_id: child_id,
    }
    axios
      .put(`/userLesson/${userLessonID}`, data)
      .then((res) => {
        // console.log('res: ', res)
        if (res.status === 200) {
          if (type !== 'problem_set') {
            // console.log('not problem set res: ', res)
            dispatch({
              type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
              payload: res.data.result.update[1],
            })
          } else if (
            type === 'problem_set' &&
            !store.getState().child.account?.is_test_account
          ) {
            // console.log('prb set')
            gaEvent({
              action: 'complete-lesson',
              event_category: 'complete-lesson',
              event_label: 'complete-lesson',
              value: userLessonID,
            })
          }
          dispatch({
            type: CHILD_ACTION_TYPES.UPDATE_USER_LESSON,
            payload: type,
          })
          if (!is_we_do) {
            dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
          }
        } else {
          console.log('Response', res)
          toast.error('Something Went Wrong')
          dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
        }
      })
      .catch((error) => {
        toast.error(error.response.data.message)
        dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
      })
  }

export const updateTrainWobble =
  (child_id: number, wobble: number | null, trainNumber: number | null) =>
  async (dispatch: Dispatch) => {
    console.log('wobble: ', wobble, ' trainNumber: ', trainNumber)
    axios
      .put('/trainwobble', {
        child_id,
        wobble,
        trainNumber,
      })
      .then((response) => {
        console.log('resonse: ', response)
        dispatch({
          type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
          payload: response.data.result[1],
        })
      })
  }
function getState() {
  throw new Error('Function not implemented.')
}

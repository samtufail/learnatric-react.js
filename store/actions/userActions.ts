import { toast } from 'react-toastify'
import { AnyAction, Dispatch } from 'redux'
import Router, { useRouter } from 'next/router'
import axios from 'api/axios'
import LogRocket from 'logrocket'
import { gaEvent } from 'lib/ga'
import { store } from '../index'
import {
  AUTH_ACTION_TYPES,
  CHILD_ACTION_TYPES,
  PARENT_ACTION_TYPES,
} from '../actionTypes'
import moment from 'moment'

export const startInitialVideo = (bool: boolean) => (dispatch: Dispatch) => {
  dispatch({ type: CHILD_ACTION_TYPES.PLAY_INITIAL_VIDEO, payload: bool })
}
export const changeVideoDisplay = (display: string) => (dispatch: Dispatch) => {
  dispatch({ type: CHILD_ACTION_TYPES.CHANGE_VIDEO_DISPLAY, payload: display })
}

export const getBilling = () => async (dispatch: Dispatch) => {
  try {
    dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START })
    const res = await axios.get(
      `/parent/billing?subId=${
        store.getState().parent.account.stripeSubsID
      }&custId=${store.getState().parent.account.stripeId}`
    )
    const { status } = res.data.result.status
    const actStatus =
      status === 'trialing'
        ? 'Trial'
        : res.data.result.status.charAt(0).toUpperCase() +
          res.data.result.status.slice(1)
    res.data.result = {
      ...res.data.result,
      status: actStatus,
    }
    // setBillingInfo(res.data.result)
    // setInterval(res.data.result.plan.interval.charAt(0).toUpperCase() + res.data.result.plan.interval.slice(1))
    // setBillingAmt(calcBillingAmt(res.data.result.plan.interval))
    dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
  } catch (e) {
    console.log('error finding billing info')
    dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
  }
}
export const setPage = (page: string) => async (dispatch: Dispatch) => {
  dispatch({ type: AUTH_ACTION_TYPES.PAGE, payload: page })
  if (page === 'Billing') {
    try {
      const { stripeSubsID, stripeId } = store.getState().parent.account
      // console.log(stripeSubsID, stripeId)
      if (stripeSubsID) {
        dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START })
        const res = await axios.get(
          `/parent/billing?subId=${stripeSubsID}&custId=${stripeId}`
        )
        const { status } = res.data.result.status
        let actStatus =
          status === 'trialing'
            ? 'Trial'
            : res.data.result.status.charAt(0).toUpperCase() +
              res.data.result.status.slice(1)
        actStatus = actStatus === 'Trialing' ? 'Trial' : actStatus
        const details = {
          ...res.data.result,
          actStatus: actStatus,
        }
        dispatch({
          type: PARENT_ACTION_TYPES.SAVE_BILLING_DETAILS,
          payload: details,
        })
        dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
      } else {
        const details = {
          actStatus: 'Free Three Month Sign up',
          trail_ends: moment(store.getState().parent.account.createdAt)
            .add(90, 'days')
            .calendar(),
        }
        dispatch({
          type: PARENT_ACTION_TYPES.SAVE_BILLING_DETAILS,
          payload: details,
        })
      }
    } catch (e) {
      console.log('error finding billing info', e)
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
    }
  }
}

export const parentSignup = (data: any) => async (dispatch: Dispatch) => {
  const { values, paymentMethod, plan_selected, is_free_signup } = data
  dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START, data })
  axios
    .post('/parent/signup', {
      ...values,
      plan_selected,
      paymentMethod,
      is_free_signup,
    })
    .then((res) => {
      if (res.status === 201) {
        const { message } = res.data
        dispatch({ type: AUTH_ACTION_TYPES.PARENT_SIGNUP_SUCCESS })
        toast.success(message)
        // Router.push('/login')
        gaEvent({
          action: is_free_signup ? 'free_signup' : 'purchase',
          event_category: is_free_signup ? 'free_signup' : 'purchase',
          event_label: `firstName: ${res.data.result.firstName} lastName: ${res.data.result.lastName} email: ${res.data.result.email}`,
          value: res.data.result.id,
        })
        axios.post('/email-signup', values)
        Router.push('/congratulations')
        // console.log('created parent res: ', res)
      } else {
        console.log('Response', res)
        toast.error('Something Went Wrong')
        dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
      }
    })
    .catch((error) => {
      toast.error(error.response.data.message)
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
    })
}

export const getChildPerformance = async (): Promise<any> => {
  // dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
  return axios.get(
    `childrenTagData?parent_id=${localStorage.getItem('PARENT_ID')}`
  )
  //     .then((res) => {
  //       console.log('ours', res)

  //     //   if (res.status === 200) {
  //     //     const { result } = res.data
  //     //     dispatch({
  //     //       type: PARENT_ACTION_TYPES.GET_CHILD_PERFORMANCE,
  //     //       payload: result,
  //     //     })
  //     //     dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
  //     //   } else {
  //     //     console.log('Response', res)
  //     //     dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
  //     //     toast.error('Something Went Wrong')
  //     //   }
  //     //   dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
  //     // })
}

export const userLogin = (data: any) => (dispatch: Dispatch) => {
  dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START, data })
  axios
    .post('/user/login', data)
    .then(async (res) => {
      // console.log('login res: ', res)
      if (res.status === 200) {
        const { message, result } = res.data
        const { type, token, parent, child } = result
        const id = type === 'PARENT' ? parent.id : child.id
        localStorage.setItem('TYPE', type)
        localStorage.setItem('TOKEN', token)
        if (type === 'PARENT') {
          const { step } = parent
          gaEvent({
            action: 'parent-login',
            event_category: 'PARENT',
            event_label: 'parent-login',
            value: parent.id,
          })
          localStorage.setItem('PARENT_ID', parent.id)
          LogRocket.identify(id, {
            name: `${parent.firstName} ${parent.lastName}`,
            accoutType: 'PARENT',
            accountID: `${id}`,
          })
          switch (step) {
            case '1':
              dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
              Router.push('/add-children')
              dispatch({
                type: AUTH_ACTION_TYPES.USER_LOGIN_SUCCESS,
                payload: { account: parent, type },
              })
              dispatch({
                type: PARENT_ACTION_TYPES.SAVE_PARENT_ACCOUNT_DETAILS,
                payload: parent,
              })
              break
            case '2':
              Router.push('/create-children-profile')
              dispatch({
                type: AUTH_ACTION_TYPES.USER_LOGIN_SUCCESS,
                payload: { account: parent, type },
              })
              dispatch({
                type: PARENT_ACTION_TYPES.SAVE_PARENT_ACCOUNT_DETAILS,
                payload: parent,
              })
              break
            case '3':
              getChildPerformance().then((resp) => {
                // Router.push('/parent-dashboard')
                if (resp.status === 200) {
                  const { result } = resp.data
                  dispatch({
                    type: PARENT_ACTION_TYPES.GET_CHILD_PERFORMANCE,
                    payload: result,
                  })
                  dispatch({
                    type: AUTH_ACTION_TYPES.USER_LOGIN_SUCCESS,
                    payload: { account: parent, type },
                  })
                  dispatch({
                    type: PARENT_ACTION_TYPES.SAVE_PARENT_ACCOUNT_DETAILS,
                    payload: parent,
                  })
                  Router.push('/parent-dashboard')
                  // dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP }
                } else {
                  console.log('Response', res)
                  dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
                  toast.error('Something Went Wrong')
                }
                // dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
              })
              break
            default:
              break
          }
          // dispatch({
          //   type: AUTH_ACTION_TYPES.USER_LOGIN_SUCCESS,
          //   payload: { account: parent, type },
          // })
          // dispatch({
          //   type: PARENT_ACTION_TYPES.SAVE_PARENT_ACCOUNT_DETAILS,
          //   payload: parent,
          // })
        } else {
          localStorage.setItem('CHILD_ID', child.id)
          gaEvent({
            action: 'child-login',
            event_category: 'CHILD',
            event_label: 'child-login',
            value: child.id,
          })
          LogRocket.identify(id, {
            name: `${child.firstName} ${child.lastName}`,
            accoutType: 'CHILD',
            parentId: `${child.parentId}`,
            grade: `${child.grade}`,
            username: `${child.username}`,
          })
          Router.push('/child-dashboard')
          dispatch({
            type: AUTH_ACTION_TYPES.USER_LOGIN_SUCCESS,
            payload: { account: child, type },
          })
          dispatch({
            type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
            payload: child,
          })
        }
        toast.success(message)
      } else {
        console.log('Response', res)
        toast.error('Something Went Wrong')
        dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
      }
    })
    .catch((error) => {
      toast.error(error?.response?.data?.message)
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
    })
}

export const switchToChild = (childId: number) => (dispatch: Dispatch) => {
  dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START, childId })
  localStorage.removeItem('TOKEN')
  localStorage.removeItem('PARENT_ID')
  localStorage.removeItem('CHILD_ID')
  localStorage.removeItem('TYPE')
  axios
    .get(`/switch-to-child?childId=${childId}`)
    .then((res) => {
      const { message, result } = res.data
      const { type, token, child } = result
      const { id } = child
      localStorage.setItem('TYPE', type)
      localStorage.setItem('TOKEN', token)
      localStorage.setItem('CHILD_ID', child.id)
      gaEvent({
        action: 'child-login',
        event_category: 'CHILD',
        event_label: 'child-login',
        value: child.id,
      })
      LogRocket.identify(id, {
        name: `${child.firstName} ${child.lastName}`,
        accoutType: 'CHILD',
        parentId: `${child.parentId}`,
        grade: `${child.grade}`,
        username: `${child.username}`,
      })
      Router.push('/child-dashboard')
      dispatch({
        type: AUTH_ACTION_TYPES.USER_LOGIN_SUCCESS,
        payload: { account: child, type },
      })
      dispatch({
        type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
        payload: child,
      })
      toast.success(message)
    })
    .catch((error) => {
      toast.error(error?.response?.data?.message)
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
    })
}

export const switchToParent = (childId: number) => (dispatch: Dispatch) => {
  dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_START })
  localStorage.removeItem('TOKEN')
  localStorage.removeItem('PARENT_ID')
  localStorage.removeItem('CHILD_ID')
  localStorage.removeItem('TYPE')
  axios
    .get(`/switch-to-parent?childId=${childId}`)
    .then((res) => {
      console.log('parent res', res)
      const { message, result } = res.data
      const { type, token, parent } = result
      const { id } = parent
      localStorage.setItem('TYPE', type)
      localStorage.setItem('TOKEN', token)
      localStorage.setItem('PARENT_ID', parent.id)
      // gaEvent({
      //   action: 'parent-login',
      //   event_category: 'PARENT',
      //   event_label: 'parent-login',
      //   value: parent.id,
      // })
      LogRocket.identify(id, {
        name: `${parent.firstName} ${parent.lastName}`,
        accoutType: 'PARENT',
        accountID: `${id}`,
      })
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
      Router.push('/parent-dashboard')
      dispatch({
        type: AUTH_ACTION_TYPES.USER_LOGIN_SUCCESS,
        payload: { account: parent, type },
      })
      dispatch({
        type: PARENT_ACTION_TYPES.SAVE_PARENT_ACCOUNT_DETAILS,
        payload: parent,
      })
      Router.push('/parent-dashboard')
      toast.success(message)
    })
    .catch((error) => {
      toast.error(error?.response?.data?.message)
      dispatch({ type: AUTH_ACTION_TYPES.AUTH_LOADING_STOP })
    })
}

export const addChild =
  (data: any, resetForm: () => void, next: () => void, last: () => void) =>
  async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .post('/child', data)
      .then((res) => {
        if (res.status === 201) {
          const { message } = res.data
          dispatch({ type: PARENT_ACTION_TYPES.ADD_CHILD_SUCCESS })
          resetForm()
          next()
          last()
          toast.success(message)
        } else {
          console.log('Resonse', res)
          toast.error('Something Went Wrong')
          dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
        }
      })
      .catch((error) => {
        toast.error(error.response.data.message)
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
  }

export const getChildsOfParent = () => async (dispatch: Dispatch) => {
  dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
  axios
    .get(`/childs?Id=${localStorage.getItem('PARENT_ID')}`)
    .then((res) => {
      if (res.status === 200) {
        console.log('get childs j:', res.data)
        const { result } = res.data
        dispatch({
          type: PARENT_ACTION_TYPES.GET_PARENT_CHILDS_SUCCESS,
          payload: result,
        })
      } else {
        console.log('Response', res)
        toast.error('Something Went Wrong')
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      }
    })
    .catch((error) => {
      toast.error(error.response.data.message)
      dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
    })
}

export const updateChild =
  (data: any, resetForm: () => void, next: () => void, last: () => void) =>
  async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .put('/child', data)
      .then((res) => {
        if (res.status === 200) {
          const { message } = res.data
          resetForm()
          next()
          last()
          dispatch({
            type: PARENT_ACTION_TYPES.UPDATE_CHILD_SUCCESS,
          })
          toast.success(message)
        } else {
          console.log('Response', res)
          toast.error('Something Went Wrong')
          dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
        }
      })
      .catch((error) => {
        toast.error(error.response.data.message)
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
  }

export const getLessonForChild = () => async (dispatch: Dispatch) => {
  dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_START })
  axios
    .get(`/lesson?childId=${localStorage.getItem('CHILD_ID')}`)
    .then(async (res) => {
      if (res.status === 200) {
        if (store.getState().child.question_id) {
          const fetchQuestion = await axios.get(
            `fetch-question?question_id=${store.getState().child.question_id}`
          )
          console.log('question resp: ', fetchQuestion)
          const question = fetchQuestion.data.result
          dispatch({
            type: CHILD_ACTION_TYPES.QUESTION_FETCH,
            payload: question,
          })
        }
        const { result } = res.data
        dispatch({
          type: CHILD_ACTION_TYPES.GET_CHILD_LESSON,
          payload: result,
        })
        dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
      } else {
        console.log('Response', res)
        toast.error('Something Went Wrong')
        dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
      }
    })
    .catch((error) => {
      toast.error(error.response.data.message)
      dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
    })
}

export const logoutUser = (router: any) => async (dispatch: Dispatch) => {
  localStorage.removeItem('TOKEN')
  localStorage.removeItem('PARENT_ID')
  localStorage.removeItem('CHILD_ID')
  localStorage.removeItem('TYPE')
  router.push('/')
  dispatch({ type: AUTH_ACTION_TYPES.LOGOUT })
}

export const updateEmail =
  (values: { id: number; email: string }) => async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .put('/account/update-email', values)
      .then((res) => {
        if (res.data.code === 202) {
          dispatch({
            type: PARENT_ACTION_TYPES.UPDATE_EMAIL,
            payload: res.data.result.email,
          })
          toast.success(res.data.message)
        } else if (res.data.code === 409) {
          toast.error(res.data.message)
        }
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
      .catch((err) => {
        toast.error(err.message)
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
  }

export const updatePhone =
  (values: { id: number; phoneNumber: string }) =>
  async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .put('/account/update-phone', values)
      .then((res) => {
        if (res.data.code === 202) {
          dispatch({
            type: PARENT_ACTION_TYPES.UPDATE_PHONE,
            payload: res.data.result.phoneNumber,
          })
          toast.success(res.data.message)
        }
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
      .catch((err) => {
        toast.error(err.message)
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
  }

export const updateParentPassword =
  (values: { id: number; current_pass: string; new_pass: string }) =>
  async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .put('/account/update-password-parent', values)
      .then((res) => {
        if (res.data.code === 202) {
          toast.success(res.data.message)
        } else if (res.data.code === 401) {
          toast.error(res.data.message)
        }
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
      .catch((err) => {
        toast.error(err.message)
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
  }

export const updateChildUsername =
  (values: { id: number; username: string }) => async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .put('/account/update-child-username', values)
      .then((res) => {
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
        toast.success(res.data.message)
      })
      .catch((err) => {
        toast.error(err.data.message)
      })
  }

export const updateChildPassword =
  (values: { id: number; current_pass: string; new_pass: string }) =>
  async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .put('/account/update-passwod-child', values)
      .then((res) => {
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
      .catch((err) => {
        toast.error(err.data.message)
      })
  }

export const forgetParentPassword =
  (values: { email: string }, router) => async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .put('account/forgot-password', values)
      .then((res) => {
        // console.log({ res })
        if (res.data.code === 202) {
          // toast.success(res.data.message)
          router.push('/email-sent')
        } else if (res.data.code === 400) {
          toast.error(res.data.message)
        }
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
      .catch((err) => {
        toast.error(err.message)
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
  }

export const forgetChildPassword =
  (values: { username: string }, router) => async (dispatch: Dispatch) => {
    dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_START })
    axios
      .put('account/forgot-password', values)
      .then((res) => {
        if (res.data.code === 202) {
          // toast.success(res.data.message)
          router.push('/email-sent?child=1')
        } else if (res.data.code === 400) {
          toast.error(res.data.message)
        }
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
      .catch((err) => {
        toast.error(err.message)
        dispatch({ type: PARENT_ACTION_TYPES.PARENT_LOADING_STOP })
      })
  }

export const setChildToys = (data: any) => async (dispatch: Dispatch) => {
  dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_START })

  try {
    const child = await axios.post('/child/set-toys', data)
    console.log('child after toys: ', child.data.result)
    dispatch({
      type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
      payload: child.data.result,
    })
    dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
  } catch (err) {
    toast.error(err.message)
    dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
  }
}

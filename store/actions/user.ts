// import { Dispatch } from 'redux'
// import Router, { NextRouter } from 'next/router'
// import axios from 'api/axios'
// import { UserActionTypes } from '../actionTypes'
// import { lessonResponse } from 'components/Constants'
// import { Lesson, SignUp } from 'models'
// import LogRocket from 'logrocket'
// import { ConsoleSqlOutlined } from '@ant-design/icons'

// export const userToLogin = (payload: UserState) => ({
//   type: UserActionTypes.USER_TO_LOGIN,
//   payload,
// })
// export const updateLessonQuestions = (payload: UserState) => ({
//   type: UserActionTypes.COMPLETE_LESSON,
//   payload,
// })

// export const updateCameFromActivity = (payload: boolean) => ({
//   type: UserActionTypes.CAME_FROM_ACTIVITY,
//   payload,
// })



// export const loginUser =
//   ({ username, password }: LoginData) =>
//   async (dispatch: Dispatch) => {
//     dispatch({
//       type: UserActionTypes.LOGIN_START,
//     })

//     try {
//       localStorage.removeItem('stepInLesson')
//       localStorage.removeItem('questionIndex')
//       const res = await axios.post('/login', {
//         username,
//         password,
//       })

//       if (res.data.status === 200) {
//         const { account, lessonsAndQuestions, message, type } = res.data.data
//         localStorage.setItem('access_token', account.token)
//         localStorage.setItem('userId', account.id)
//         localStorage.setItem('type', type)

//         if (account?.id) {
//           LogRocket.identify(account?.id, {
//             name: `${account?.firstName} ${account?.lastName}`,
//             accoutType: `${res.data.data.type}`,
//             parentId:
//               res.data.data.type === 'children'
//                 ? `${account.parentId}`
//                 : 'not a child',
//             grade: `${account.grade}`,
//           })
//         }

//         if (type === 'parent') {
//           dispatch(
//             loginUserSuccess({
//               parentId: account.id,
//               isParent: true,
//               isLoggedIn: true,
//               child_count: account.child_count,
//               loginError: false,
//               errorMessage: '',
//               cameFromActivity: false,
//             })
//           )
//           Router.push('/parentdashboard')
//         } else if (type === 'children') {
//           dispatch(
//             loginUserSuccess({
//               account,
//               lessonsAndQuestions,
//               child_id: account.id,
//               isLoggedIn: true,
//               myLesson: lessonResponse,
//               loginError: false,
//               errorMessage: '',
//               cameFromActivity: false,
//             })
//           )
//           Router.push('/child-dashboard')
//         }
//       } else {
//         dispatch(loginUserFail(res.data.message))
//       }
//     } catch (err) {
//       console.log('err', err)
//       dispatch(loginUserFail('Something went wrong'))
//     }
//   }

// const loginUserFail = (payload) => ({
//   type: UserActionTypes.LOGIN_FAIL,
//   payload,
// })

// const loginUserSuccess = (payload: UserState) => ({
//   type: UserActionTypes.LOGIN_SUCCESS,
//   payload,
// })

// export const logoutUser = (Router: NextRouter | string[]) => {
//   return (dispatch: Dispatch) => {
//     localStorage.removeItem('access_token')
//     localStorage.removeItem('userId')
//     localStorage.removeItem('stepInLesson')
//     localStorage.removeItem('questionIndex')
//     dispatch({
//       type: UserActionTypes.LOGOUT_USER,
//     })

//     Router.push('/')
//   }
// }

// export const subscribeUser =
//   (data, values, router) => async (dispatch: Dispatch) => {
//     dispatch({
//       type: UserActionTypes.LOADING_START,
//       payload: 'Confirming Subscription',
//     })
//     try {
//       const res = await axios.post('/parent/subscription', data)
//       if (
//         res.status === 200 &&
//         res.data.stripe_customer_id &&
//         res.data.stripe_sub_id
//       ) {
//         dispatch({
//           type: UserActionTypes.LOADING_STOP,
//         })
//         const valuesObj = {
//           ...values,
//           plan: data.plan_selected,
//           stripe_customer_id: res.data.stripe_customer_id,
//           stripe_sub_id: res.data.stripe_sub_id,
//         }
//         // @ts-ignore
//         dispatch(signUp(valuesObj, router))
//       } else {
//         dispatch({
//           type: UserActionTypes.LOADING_STOP,
//         })
//       }
//     } catch (err) {
//       console.log('err', err)
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//     }
//   }

// export const signUp = (data, router) => async (dispatch: Dispatch) => {
//   dispatch({
//     type: UserActionTypes.LOADING_START,
//     payload: 'Signing Up',
//   })
//   try {
//     const res = await axios.post('/parent/new-parent', data)
//     if (typeof res.data.data === 'string') {
//       dispatch({
//         type: UserActionTypes.SIGN_UP,
//         payload: {
//           message: res.data.data,
//         },
//       })
//     }

//     if (typeof res.data.data.id === 'number') {
//       dispatch({
//         type: UserActionTypes.SIGN_UP,
//         payload: {
//           parentId: res.data.data.id,
//           child_count: res.data.data.child_count,
//         },
//       })
//       LogRocket.identify(res.data.data.id, {
//         name: `${res.data.data.firstName} ${res.data.data.lastName}`,
//       })
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//       // setLoading({ state: false, message: '' })
//       router.push('/congratulations')
//     } else {
//       // todo handle displaying the error message "User Account witht that email already exists"
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//     }
//   } catch (err) {
//     console.log('err', err)
//     dispatch({
//       type: UserActionTypes.LOADING_STOP,
//     })
//   }
// }
// // export const childSignUp = (payload: UserState) => ({
// //   type: UserActionTypes.SIGN_UP_CHILDREN,
// //   payload,
// // })

// export const loadMyLesson = (payload: Lesson) => ({
//   type: UserActionTypes.LOAD_MY_LESSON,
//   payload,
// })

// export const completeMyLesson =
//   (payload: Lesson) => async (dispatch: Dispatch) => {
//     dispatch({
//       type: UserActionTypes.COMPLETE_LESSON_START,
//     })
//     try {
//       const res = await axios.post('/children/pass-my-lesson', {
//         id: localStorage.getItem('userId'),
//       })
//       if (res.status === 200) {
//         dispatch({
//           type: UserActionTypes.COMPLETE_LESSON_SUCCESS,
//         })
//         // @ts-ignore
//         dispatch(getChildDetail())
//         Router.push('/child-dashboard')
//       } else {
//         dispatch({
//           type: UserActionTypes.COMPLETE_LESSON_FAIL,
//         })
//       }
//     } catch (err) {
//       console.log('err', err)
//     }
//   }

// export const getChildDetail = () => async (dispatch: Dispatch) => {
//   try {
//     dispatch({
//       type: UserActionTypes.GET_CHILD_DETAIL_START,
//     })

//     const res = await axios.get(
//       `/children?id=${localStorage.getItem('userId')}`
//     )
//     if (res.status === 200) {
//       dispatch({
//         type: UserActionTypes.GET_CHILD_DETAIL_SUCCESS,
//         payload: res.data,
//       })
//     } else {
//       dispatch({
//         type: UserActionTypes.GET_CHILD_DETAIL_FAIL,
//       })
//     }
//   } catch (err) {
//     console.log('err', err)
//   }
// }

// export const signupChild =
//   (data, resetForm, next, last) => async (dispatch: Dispatch) => {
//     dispatch({
//       type: UserActionTypes.LOADING_START,
//     })
//     try {
//       const res = await axios.post('/children/newchild', data)
//       resetForm()
//       next()
//       last()
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//       // Router.push('/create-children-profile')
//     } catch (err) {
//       console.log('err', err)
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//     }
//   }

// export const fetchChildren =
//   (parentId, router) => async (dispatch: Dispatch) => {
//     dispatch({
//       type: UserActionTypes.LOADING_START,
//     })
//     try {
//       const res = await axios.get(
//         `/children/fetch-children?parentId=${parentId}`
//       )
//       const childrenFromDb = []
//       res.data.data.forEach((child) => {
//         childrenFromDb.push({
//           firstName: child.firstName,
//           id: child.id,
//           parentId: child.parentId,
//         })
//       })
//       dispatch({
//         type: UserActionTypes.SIGN_UP_CHILDREN,
//         payload: childrenFromDb,
//       })
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//       router.push('/create-children-profile')
//     } catch (err) {
//       console.log('err', err)
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//     }
//   }

// export const createChildrenProfile =
//   (data, setVisibleState, child_count, router) =>
//   async (dispatch: Dispatch) => {
//     // dispatch({
//     //   type: UserActionTypes.LOADING_START,
//     // })
//     try {
//       const res = await axios.post('/children/create-child-profile', data)
//       const savedChildrenProfile = []

//       res.data.data.forEach((child) => {
//         if (child.status === 'fulfilled') {
//           savedChildrenProfile.push(child.value[1])
//         }
//       })
//       dispatch({
//         type: UserActionTypes.CREATE_CHILDREN_PROFILE,
//         payload: savedChildrenProfile,
//       })
//       if (child_count === 1) {
//         dispatch(userToLogin(savedChildrenProfile[0].firstName))
//         router.push('/login')
//       } else {
//         setVisibleState(true)
//       }
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//     } catch (err) {
//       console.log('err', err)
//       dispatch({
//         type: UserActionTypes.LOADING_STOP,
//       })
//     }
//   }

// export const emailChange = () => async (dispatch: Dispatch) => {
//   dispatch({
//     type: UserActionTypes.SIGN_UP,
//     payload: {
//       messageEmail: '',
//     },
//   })
// }
export {}

import { toast } from 'react-toastify'
import { Dispatch } from 'redux'
import axios from 'api/axios'
import {
  AUTH_ACTION_TYPES,
  CHILD_ACTION_TYPES,
  ASSESSMENT_ACTION_TYPES,
} from '../actionTypes'
import { store } from '../index'
import Router from 'next/router'

export const getAssessmentQuestions =
  (data: any) => async (dispatch: Dispatch) => {
    console.log('data: ', data)
    dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_START })
    try {
      const res = await axios.get(
        `/assessment-questions?child_id=${data.id}&grade=${data.grade}`
      )
      console.log('res: ', res)
      if (res.status === 200) {
        dispatch({
          type: ASSESSMENT_ACTION_TYPES.SET_ASSESSMENT_QUESTIONS,
          payload: res.data.result,
        })
        dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
      }
    } catch (err) {
      console.log('error fetching assessment questions: ', err)
      dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
    }
  }

export const handleAssessmentAnswers =
  (data: any) => async (dispatch: Dispatch) => {
    // console.log('data assessment answer: ', data)
    const { child_id } = data
    const { grade } = store.getState().child.account
    // console.log('grade: ', grade)
    dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_START })
    try {
      const tagAnswerRes = await axios.post('/tags-answers', data)
      if (!tagAnswerRes.data.result.assessment_complete) {
        dispatch({
          type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
          payload: tagAnswerRes.data.result,
        })
        const questionsRes = await axios.get(
          `/assessment-questions?child_id=${child_id}&grade=${grade}`
        )
        console.log('question res: ', questionsRes)
        dispatch({
          type: ASSESSMENT_ACTION_TYPES.SET_ASSESSMENT_QUESTIONS,
          payload: questionsRes.data.result,
        })
        dispatch({
          type: ASSESSMENT_ACTION_TYPES.SET_IDX,
          payload: 0,
        })
        dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
      } else {
        dispatch({
          type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
          payload: tagAnswerRes.data.result,
        })
        Router.push('/child-dashboard')
        dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
        toast.success('Great Job Finishing Assessment')
      }
    } catch (err) {
      console.log('error updateing tag ratings and answer_results', err)
      dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
    }

    dispatch({
      type: ASSESSMENT_ACTION_TYPES.SET_IDX,
      payload: 0,
    })
  }

export const handleAssessmentIdx =
  (data: any) => async (dispatch: Dispatch) => {
    dispatch({
      type: ASSESSMENT_ACTION_TYPES.SET_IDX,
      payload: data,
    })
  }

import { toast } from 'react-toastify'
import { Dispatch } from 'redux'
import axios from 'api/axios'
import { AUTH_ACTION_TYPES, CHILD_ACTION_TYPES } from '../actionTypes'
import Router from 'next/router'

export const addAnswerResult =
  (data: any, is_we_do: boolean = false) =>
  async (dispatch: Dispatch) => {
    // console.log('data: ', data)
    dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_START })
    axios
      .post('/answerResult', data)
      .then((res) => {
        if (res.status === 200) {
          if (!data.answerResult.is_we_do) {
            dispatch({
              type: CHILD_ACTION_TYPES.SAVE_CHILD_ACCOUNT_DETAILS,
              payload: res.data.result[1],
            })
          }
          if (!is_we_do) {
            dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
          }
          if (data.lastQuestion) {
            dispatch({
              type: CHILD_ACTION_TYPES.FINISHED_LESSON,
              payload: { bool: true },
            })
            dispatch({ type: CHILD_ACTION_TYPES.REMOVE_Q_ID })
            Router.push('/child-dashboard')
          }
        } else {
          console.log('Response', res)
          toast.error('Something Went Wrong')
          dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
        }
      })
      .catch((error) => {
        toast.error(error.response.data.message)
        dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
      })
  }

export const curateQuestion =
  (tag: number, lesson_id: number) => async (dispatch: Dispatch) => {
    dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_START })
    try {
      const res = await axios.get(
        `/curate-question?tag_id=${tag}&child_id=${localStorage.getItem(
          'CHILD_ID'
        )}&lesson_id=${lesson_id}`
      )
      // console.log('res question curation: ', res)
      dispatch({
        type: CHILD_ACTION_TYPES.QUESTION_CURATION,
        payload: res.data.result,
      })
      dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
    } catch (e) {
      console.log('errror question curation', e)
      dispatch({ type: CHILD_ACTION_TYPES.CHILD_LOADING_STOP })
    }
  }

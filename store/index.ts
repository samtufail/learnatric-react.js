import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import LogRocket from 'logrocket'
import { Persistor, persistStore } from 'redux-persist'
import reducers from 'store/reducers'

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? // @ts-ignore
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose

export const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(thunk, LogRocket.reduxMiddleware()))
)

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>

export const persistor = persistStore(store as any)

export * from './hooks'
